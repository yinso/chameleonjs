#!/usr/bin/env coffee

logger = require '../shared/log'

logger.setLogStream './chameleon-debug.log'
logger.log "----------------------------------------------------------------------"
logger.log "CHAMELEON SESSION", new Date().toString()
logger.log "----------------------------------------------------------------------"

require '../shared/function'
fs = require '../shared/fs'
path = require 'path'
repl = require 'repl'
parser = require './chamelang'
BuiltIn = require './builtin'
VM = require './vm'
ExpressionEntry = require './entrylog'
require './builtin'
AppEnvironment = require './appenvironment'
Environment = require './environment'

# the first thing we should do is to load up the package.json and do the appropriate processing.
# do I want to extract part of the runtime and push it to environment?
# seems like that's something that I'll want to explicitly manage...
# hmm...
# some commands... diff...
class Runtime
  @make: (cb) ->
    interp = new Runtime()
    logger.debug process.cwd()
    # we'll look for a project file to load a bunch of context...
    # let's say that's a cson right now.
    interp.initialize (err) ->
      if err
        logger.debug 'Error_loading_config', err
      else
        myEval = (cmd, context, filename, cb) ->
          # this context is what's available in the base of the system...
          interp.eval cmd, context, filename, cb
        cb null, myEval
  @makeScriptable: (cb) ->
    interp = new Runtime()
    interp.initialize (err) ->
      if err
        logger.debug 'Error_loading_config', err
      else
        # I'll want to return the object for which I can use for evaluating
        myEval = (cmd, cb) ->
          context = uuid: uuid.v4
          interp.eval cmd, context, 'global', cb
        cb null, myEval
  @start: () ->
    Runtime.make (err, myEval) ->
      if err
        logger.debug 'Error', err
      else
        rep = repl.start
          prompt: 'chameleon> '
          input: process.stdin
          output: process.stdout
          eval: myEval
        @entryLog = new ExpressionEntry rep
  constructor: () ->
    # this is going to be used to write to 
    @vm = new VM()
    @appEnv = new AppEnvironment(@)
    process.on 'SIGINT', () =>
      @evalQuit()
  initialize: (cb) ->
    # we'll need to ensure that the load works across the board... hmmm...
    @vm.eval 'load("code/base.chml")', cb
  eval: (cmd, context, @filename, cb) ->
    cmd = cmd.replace /^\((.*)\n+\)$/, '$1'
    if cmd == ''
      cb null, null
    else
      @evalInner cmd, @callback(cmd, cb)
  callback: (cmd, cb) ->
    (err, res) =>
      if err
        logger.debug 'Error: ', err
      else
        cb null, res
  evalInner: (cmd, cb) ->
    try
      if cmd == 'quit'
        @evalQuit cb
      else
        @vm.eval cmd, cb
    catch e
      cb e
  evalQuit: (cb) ->
    @appEnv.disconnect (err) =>
      process.exit()

module.exports = Runtime


