
# let's do a validator framework... am I ready for this again?
# 1 - the goal is to have a set of validators that can be composed together.
# each validator ought to be

# something like this...
# it also needs to have a good reporting framework...

assert = require 'assert'
_ = require 'underscore'
Exception = require '../shared/exception'
async = require '../shared/async'
Default = require './default'
Schema = require './schema'
logger = require '../shared/log'

# we'll create a simpler version of the validator.
# no reason this needs to be schema specific

# let's make sure we have simple validator... it's a list of functions + combinators.
# 3 combinators.
# and - this is the default.
# or
# not
# every validator should throw the same error message - so we keep the localization of msg
# to be a separate thing.

# let's see - 
# the simplest way of writing these things is going to be quite tough it seems...
# forget about 

class Validator
  @__process: (val) =>
    if val instanceof Function
      val
    else
      throw new Exception error: 'unsupported_validator_type', value: val
  @register: (key, val) ->
    @schema.registerValidator key, val
  @has: (key) ->
    @schema.hasValidator key
  @registerSchema: (schema) ->
    assert.ok schema instanceof Schema, "invalid_schema: #{schema}"
    @schema = schema
    @schema.setClass
      class: 'validators'
      validate: @__process
  constructor: (@spec) ->
    @initialize @spec
  initialize: (spec) -> # by default it's AND
    @inners = []
    for key, val of spec
      if Validator.has(key) and val
        @inners.push Validator.has(key) val
  validate: (val, cb) ->
    helper = (inner, next) ->
      inner val, next
    async.forEach @inners, helper, (err, re) ->
      if err
        cb err
      else
        cb null, val
  describe: () ->
    @spec
  error: (val) ->
    new Exception error: "validation_failed", value: val, spec: @spec

Validator.registerSchema Schema.current

Validator.register 'min', (min) ->
  assert.ok typeof(min) == 'number', "min_expect_a_number_but_passed: #{min}"
  (val, cb) ->
    if min <= val
      cb null, val
    else
      cb new Exception error: "min_failed", value: val, spec: min

Validator.register 'max', (max) ->
  assert.ok typeof(max) == 'number', "max_expect_a_number_but_passed: #{max}"
  (val, cb) ->
    if max >= val
      cb null, val
    else
      cb new Exception error: "max_failed", value: val, spec: max

Validator.register 'minLength', (min) ->
  assert.ok typeof(min) == 'number', "minLength_expect_a_number_but_passed: #{min}"
  (val, cb) ->
    if min <= val.length
      cb null, val
    else
      cb new Exception error: "minLength_failed", value: val, spec: min

Validator.register 'maxLength', (max) ->
  assert.ok typeof(max) == 'number', "maxLength_expect_a_number_but_passed: #{max}"
  (val, cb) ->
    if max >= val.length
      cb null, val
    else
      cb new Exception error: "maxLength_failed", value: val, spec: max

Validator.register 'regex', (regex) ->
  if typeof(regex) == 'string'
    res = regex.match /^\/(.*)\/([igm]*)$/
    if res
      regex = new RegExp res[1], res[2]
    else
      throw new Exception error: 'invalid_regex_expression', value: regex
  logger.debug 'Validator.regex', regex.toString()
  assert.ok regex instanceof RegExp, "regex_expect_a_RegExp_but_passed: #{regex}"
  (val, cb) ->
    logger.debug 'regex.validate', regex.exec val
    if regex.exec val
      cb null, val
    else
      cb new Exception error: "regex_failed", value: val, spec: regex

module.exports = Validator # also a registered framework...