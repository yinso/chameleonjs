logger = '../shared/log'

# an environment can be chained, like a stack
# ought this be combined with registry?
# they are quite similar but a bit different.
class Environment
  constructor: (@inner = {}, @prev = null) ->
  get: (key) ->
    if @inner.hasOwnProperty(key)
      @inner[key]
    else if @prev
      @prev.get key
    else
      undefined
  getLexical: (key) ->
    if @inner.hasOwnProperty(key)
      @inner[key]
    else
      undefined
  isTopLevel: () ->
    not @prev
  isFreeVariable: (key) ->
    #logger.debug 'Environment.isFreeVariable', key, @inner, not @inner.hasOwnProperty(key) and @prev.get key
    not @inner.hasOwnProperty(key) and @prev.get key
  isTopLevelVariable: (key) ->
    if @isTopLevel()
      if @inner.hasOwnProperty(key)
        true
      else
        false
    else
      if @inner.hasOwnProperty(key)
        false
      else
        @prev.isTopLevelVariable(key)
  isClosureVariable: (key) ->
    @isFreeVariable(key) and not @isTopLevelVariable()
  isLexicalVariable: (key) ->
    @inner.hasOwnProperty(key) # this is lexical to the current level... 
  set: (key, val) ->
    @inner[key] = val # this allows for overwriting values in environment...
  del: (key) ->
    val = @inner[key]
    delete @inner[key]
    val
  clone: () ->
    new Environment @_clone(), @_clonePrev(@prev)
  _clone: (source = @inner) ->
    clone = {}
    for k, v of @inner
      clone[k] = v
    clone
  _clonePrev: () ->
    if @prev instanceof Environment
      @prev.clone()
    else if @prev instanceof Object
      @_clone @prev
    else
      @prev

module.exports = Environment
