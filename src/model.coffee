# a model is a constructor that can be used to create a particular object.
#
{EventEmitter} = require 'events'
Exception = require '../shared/exception'
assert = require 'assert'
async = require '../shared/async'
uuid = require '../shared/uuid'
crypto = require '../shared/crypto'
_ = require 'underscore'
obj = require './object'
Registry = require './registry'
logger = require '../shared/log'

# it's okay for the Type, TypeConstraints, and Defaults to be shared by all available
# schemas - after all - they are not instantiated as the rest of the schema...
# the current design is a bit... too much

# the next step is to fix the type system here... we want the following
# 1 - easy to use type system that can be extended quickly
# 

# these are type constraints... probably separate out a type definition...
class Constraint
  validateAsync: (val, cb) -> cb null, val

class MinConstraint extends Constraint
  @type = 'min'
  constructor: (@min) ->
  validateAsync: (val, cb) -> # why is this not available?
    if val >= @min
      cb null, val
    else
      cb {error: 'min_contraint_failed', min: @min, value: val}, val

class MaxConstraint extends Constraint
  @type = 'max'
  constructor: (@max) ->
  validateAsync: (val, cb) -> 
    if val <= @max
      cb null, val
    else
      cb {error: 'max_contraint_failed', max: @max, value: val}, val

class BetweenConstraint extends Constraint
  @type = 'between'
  constructor: ([@min, @max]) ->
  validateAsync: (val, cb) -> 
    if val <= @max and val >= @min
      cb null, val
    else
      cb {error: 'max_contraint_failed', max: @max, value: val}, val

class MinLengthConstraint extends Constraint
  @type = 'minLength'
  constructor: (@minLength) ->
  validateAsync: (val, cb) ->
    if val >= @minLength
      cb null, val
    else
      cb {error: 'minLength_contraint_failed', minLength: @minLength, value: val}, val

class MaxLengthConstraint extends Constraint
  @type = 'maxLength'
  @params = ['maxLength']
  constructor: (@maxLength = Number.POSITIVE_INFINITY) ->
  validateAsync: (val, cb) -> 
    if val <= @maxLength
      cb null, val
    else
      cb {error: 'maxLength_contraint_failed', maxLength: @maxLength, value: val}, val

class Type extends Constraint
  @regexToFunction = (regex) ->
    (v, failVal = undefined) ->
      if regex.exec v
        v
      else
        failVal
  constructor: ({@name, @test, @testAsync, @maker, @makerAsync, @schema}) ->
    #assert.ok @constructor.isValidator @test
    if @test instanceof RegExp
      @regex = @test
      @test = @constructor.regexToFunction @regex
    if not @testAsync
      @testAsync = @makeTestAsync()
    if not @maker
      @maker = @makeMaker()
    if not @makerAsync 
      @makerAsync = @makeMakerAsync()
  makeMaker: () ->
    (v) =>
      res = @validate v, true # will throw error.
      v
  makeTestAsync: () ->
    (v, cb) =>
      try
        res = @validate v, true
        cb null, v
      catch e
        cb e, v
  makeMakerAsync: () ->
    (v, cb) =>
      try
        res = @make v, true
        cb null, res
      catch e
        cb e, v
  validate: (v, toError = false) ->
    res = @test v
    if res
      res
    else if toError
      throw new Exception error: 'fail_validation', value: v
    else
      res
  validateAsync: (v, cb) ->
    @testAsync v, (err, res) ->
      # don't know what to do here specifically....
      if err # in this case it makes no sense to deal with the error object...!!!
        cb err, res
      else
        cb err, res
  # type can be used for transformation as well... this would require there to be a constructor.
  make: (v) ->
    if @maker
      @maker v
    else
      throw new Exception error: 'Type_has_no_maker', type: @
  makeAsync: (v, cb) ->
    @makerAsync v, cb
  describe: () ->
    type: @name
    regex: if @regex then @regex else undefined
    test: @test # this means every type is not serializable... okay - we'll deal with this later...
  decl: () ->
    @name
  equiv: (type) ->
    # the type and another type must be *equivalent*... for now this means the same thing, until we implement base type.
    @name == type.name
  diff: (type) ->
    # a type different from another type if their names are different...
    if @equiv type
      null
    else
      {left: @name, right: @name}

# this is a special type that is made of another type.
# how do we specify the generic type constraint?
# it MUST take a TYPE and return a TYPE.
class ListOf extends Type # if this returns a type we'll have to deal with maker as well.
  @type = 'listOf'
  @params = ['listOf']
  constructor: (@schema, arg) ->
    if arg instanceof Object
      @initialize arg
    else
      @initialize type: arg
  initialize: ({type, min, max}) ->
    @type = @schema.makeType type
    @min = min or 0
    @max = max or Number.POSITIVE_INFINITY
  validate: (obj, toError) ->
    if obj instanceof Array
      # ensure the number is within the size count
      try
        for item, i in obj
          @type.validate item, true
      catch e
        if toError
          throw e
        else
          return undefined
      if obj.length >= @min and obj.length <= @max
        obj
      else
        if toError
          throw new Exception error: 'object_not_in_range', value: obj, min: @min, max: @max
        else
          undefined     
    else
      if toError
        throw new Exception error: 'object_not_array', value: obj
      else
        undefined
  validateAsync: (obj, cb) ->
    helper = (item, next) =>
      @type.validateAsync item, next
    if obj instanceof Array
      async.forEach obj, helper, (err, res) =>
        if err
          cb err, obj
        else if obj.length >= @min and obj.length <= @max
          cb null, obj
        else
          cb {error: 'object_not_in_range', value: obj, min: @min, max: @max}, obj
    else
      cb {error: 'object_not_array', value: obj}, obj
  make: (obj = []) -> # if there is something passed in...
    if not obj instanceof Array
      throw new Exception error: 'param_not_array', value: obj
    list = [].concat obj
    for i in [0...Math.max(@min, list.length)] by 1
      list.push @type.make(list[i]) # the maker will have to be able to deal with default nothing passed in... that's one place it's different from validator
    list
  makeAsync: (obj = [], cb) ->
    helper = (item, next) =>
      @type.makeAsync item, next
    if not obj instanceof Array
      cb error: 'param_not_array', value: obj, null
    else
      list = [].concat obj
      list.length = Math.max @min, obj.length
      async.map list, helper, cb
  describe: () ->
    type: @type.describe()
    min: @min
    max: @max
  describe: () ->
    type: @type.decl()
    min: @min
    max: @max
  equiv: (type) ->
    type instanceof ListOf and @type.equiv(type.type) and @min == type.min and @max == @type.max
  diff: (type) ->
    if not @equiv(type)
      {left: @describe(), right: type.describe()}
    else
      null

class Default
  constructor: (@schema, @spec) ->
    if spec instanceof Object # we can return object? for now, no.
      if spec.proc and @schema.defaults.has spec.proc
        @inner = @schema.defaults.has spec.proc
        @args = spec.args or []
      else if spec.object # allow for passing in of an object... what do we do with this?
        @val = spec.object # allow for returning of objects...
      else
        throw new Exception error: 'invalid_default_spec', spec: spec
    else
      @val = spec
  run: () ->
    if @inner
      @inner.apply @, @args
    else
      return @val
  describe: () ->
    @spec
  equiv: (def) ->
    obj.deepEqual @describe(), def.describe()
  diff: (def) ->
    if not @equiv def
      {left: @describe(), right: def.describe()}
    else
      null

class ConstraintRegistry extends Registry
  constructor: (schema) ->
    super()
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
  validate: (constraint) ->
    assert.ok (constraint?.prototype?.validateAsync or constraint?.validateAsync) instanceof Function
    if @has(constraint.type)
      throw new Exception error: 'redefine_constraint_type', type: constraint.type
    return constraint
  make: (params) ->
    cons = null
    for key, options of params 
      if key == 'type'
        cons = @schema.types.make type: options
        if cons
          return cons
      else if @has(key)
        type = @has(key)
        cons = new type options
        return cons
    throw new Exception error: 'invalid_constraint', value: params

class TypeRegistry extends Registry
  constructor: (schema) ->
    super()
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
  validate: ({name, test, testAsync, maker, makerAsync}) ->
    if @isValidator(testAsync or test)
      new Type # is this right?
        name: name
        test: test
        testAsync: testAsync
        maker: maker
        makerAsync: makerAsync
        schema: @schema
    else if @isObjectValidator(testAsync or test)
      test.schema = @schema
      test
    else
      throw new Exception error: 'invalid_validator', value: test
  isValidator: (obj) ->
    obj instanceof RegExp or obj instanceof Function
  isObjectValidator: (obj) ->
    (obj instanceof Object and obj.validate instanceof Function)
  make: ({type}) ->
    if typeof(type) == 'string'
      @has type
    else
      @constraint.make type

class DefaultRegistry extends Registry
  constructor: (schema) ->
    super() 
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
  validate: ({name, proc}) ->
    if @has name
      throw new Exception error: 'redefine_default', name: name
    else
      assert.ok proc instanceof Function
      proc
  make: (spec) ->
    new Default @schema, spec

class ModelRegistry extends Registry
  constructor: (schema) ->
    super()
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}

class IndexRegistry extends Registry
  constructor: (schema) ->
    super()
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}

# provide the ability to define everything in a single JSON blob
class Schema
  constructor: (@name) ->
    @types = new TypeRegistry @
    @constraints = new ConstraintRegistry @
    @defaults = new DefaultRegistry @
    @models = new ModelRegistry @
    @indexes = new IndexRegistry @
    # below are the default settings for every schema    
    @registerConstraint MinConstraint
    @registerConstraint MaxConstraint
    @registerConstraint BetweenConstraint
    @registerConstraint MinLengthConstraint
    @registerConstraint MaxLengthConstraint
    @registerConstraint Type # what does this mean?
    @registerType
      name: 'integer'
      test: (n) -> parseInt(n) != NaN and Math.floor(n) == n
      maker: (n = 0) ->
        x = parseInt(n)
        if not isNaN(x)
          x
        else
          throw new Exception error: 'not_an_integer', value: n
    @registerType
      name: 'float'
      test: ((n) -> typeof(n) == 'number')
      maker: (n = 0) ->
        x = parseFloat(n)
        if not isNaN(x)
          x
        else
          throw new Exception error: 'not_a_float', value: n
    @registerType
      name: 'string'
      test: (s) -> typeof(s) == 'string'
      maker: (s = '') -> s.toString()
    @registerType
      name: 'ssn'
      test: /\d\d\d\-?\d\d\-?\d\d\d\d/
    @registerConstraint ListOf
    @registerDefault
      name: 'timestamp'
      proc: () ->
        (new Date()).getTime()
    @registerDefault
      name: 'make_uuid'
      proc: () -> uuid.v4() # this should be built-in...
    @registerDefault
      name: 'make_crypto_salt'
      proc: crypto.makeSalt
  registerType: (typeSpec) ->
    @types.register typeSpec.name, typeSpec
  registerConstraint: (spec) ->
    @constraints.register spec.type, spec
  registerDefault: (defaultSpec) ->
    @defaults.register defaultSpec.name, defaultSpec
  registerModel: (modelSpec) ->
    @types.register modelSpec.name, modelSpec
    @model.register modelSpec.name, modelSpec
  makeConstraint: (constraintSpec) ->
    @constraints.make constraintSpec
  makeDefault: (defaultSpec) ->
    @defaults.make defaultSpec
  makeType: (typeSpec) ->
    # the type need to exist... and we'll need to pass ourselves into the type object.
    logger.debug 'Schema.makeType', typeSpec
    type = 
      if typeof(typeSpec) == 'string' and @types.has typeSpec
        @types.has typeSpec
      else if typeSpec instanceof Object and typeSpec?.listof # we'll need to make the type.
        new ListOf @, typeSpec.listof
      else # unknown type maker. need to think what this means...
        err = error: 'invalid_type_declaration', value: typeSpec
        logger.debug 'Schema.makeType_error', err
        throw new Exception err
    type
  makeModel: (modelSpec) ->
    try
      # this is the time where things are not as dan
      model = new Model @, modelSpec
      @models.register modelSpec.name, model
      @registerType name: modelSpec.name, test: model
      model
    catch e
      logger.debug 'Schema.makeModel_error', e
      throw e
  alterModel: ({name, alter}) ->
    # first of all - the model needs to exist.
    model = @models.has name
    if model
      # we'll make change for each of the alter...
      model.alter alter
    else
      throw new Exception error: 'unknown_model', name: name
  createIndex: (spec) ->
    # first of all - the index MUST exist as a specific name...
    # we should create index through the table...
    table = @models.has spec.table
    if not table
      throw new Exception error: 'unknown_model', name: spec.table
    else
      @indexes.register spec.name, table.createIndex(spec)
    # we'll need to track what's pushed and what's not pushed... for each connection...
  dropIndex: (spec) ->
    index = @indexes.has spec.name
    if index
      @indexes.delete spec.name
      table = index.table
      table.dropIndex spec
    else
      throw new Exception error: 'unknown_index', name: spec.name
  showTables: () -> # everything has a schema attached to them... is it necessary?
    @models.keys()
  showTypes: () ->
    @types.keys()
  showIndexes: () ->
    @indexes.keys()
  describeTable: (name) -> # this might need to be moved to the table itself... let's do that.
    table = @models.has name
    if not table
      throw new Exception error: 'unknown_table', name: name
    else
      table.describe()
  describeType: (name) ->
    type = @types.has name
    if not type
      throw new Exception error: 'unknown_type', name: name
    else
      type.describe()
  describeIndex: (name) ->
    index = @indexes.has name
    if not index
      throw new Exception error: 'unknown_index', name: name
    else
      index.describe()
  describe: () ->
    types: [] # not yet
    tables: @describeTables()
    indexes: @describeIndexes()
  describeTypes: () ->
    for type of @types.values()
      if type != @ and not type.builtin
        # we'll serialize this type...
        type.describe()
  describeTables: () ->
    models = []
    for model in @models.values()
      if model instanceof Model
        models.push model.describe()
    models
  describeIndexes: () ->
    indexes = []
    for index in @indexes.values()
      if index instanceof Index
        indexes.push index.describe()
    indexes
  clear: () ->
    # just go ahead and clear the 
    @models = new ModelRegistry @
    @indexes = new IndexRegistry @
  loadSchema: ({types, tables, indexes}) ->
    # there will be more in the future - let's go ahead and deal with tables.
    @loadTables tables
    @loadIndexes indexes
  loadTables: (tables) ->
    for table in tables
      @loadTable table
  loadTable: (table) ->
    # given the tablespec - what do we do? go ahead and make it I think...
    @makeModel table
  loadIndexes: (indexes) ->
    for index in indexes
      @loadIndex index
  loadIndex: (index) ->
    @createIndex index
  equal: (schema) ->
    obj.deepEqual @describe(), schema.describe()
  diff: (schema) ->
    # comparing getting the list of the tables... so we'll have to our own comparisons.
    # this one would be hard to test since we don't have another schema loaded up ready to
    # go in the system.
    # how do we test this?
    modelsDiff = obj.diff @models, schema.models, false
    # need to deal with the schema
    # we should also deal with the indexes...
    indexesDiff = obj.diff @indexes, schema.indexes, false
    if not modelsDiff and not indexesDiff
      modelsDiff
    else
      models: @innerDiff(modelsDiff)
      indexes: @innerDiff(indexesDiff)
  innerDiff: (stuffDiff) ->
    {left, right, both} = stuffDiff
    leftDiff = {}
    for key, val of left
      leftDiff[key] = val.describe()
    rightDiff = {}
    for key, val of right
      rightDiff[key] = val.describe()
    bothDiff = {}
    for key, val of both
      result = val.left.diff(val.right)
      if result
        bothDiff[key] = result
    left: leftDiff
    right: rightDiff
    both: bothDiff
  diffTable: ({table1, table2}) ->
    t1 = @models.has table1
    t2 = @models.has table2
    if not t1
      throw new Exception error: 'unknown_table', name: table1
    if not t2
      throw new Exception error: 'unknown_table', name: table2
    t1.diff t2
  clone: (newName) -> # this might not turn out to be useful... hmm....
    newSchema = new Schema newName
    # we'll push the model for the 
    newSchema.loadSchema @describe()
    newSchema
  # static object - at the end to ensure all schema prototype functions have been created.
  @base = new Schema('base')
  # this is the global schema that's available for everyone to use.



# it's all in property...
# what if we attach schema to every type?
class Property 
  constructor: (schema, spec) ->
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
    @name = spec.name
    logger.debug 'Property.ctor', spec
    assert.ok typeof(@name) == 'string' and @name != '', "invalid_name: #{@name}"
    @type = @schema.makeType spec.type
    @required = if spec.hasOwnProperty('optional') then not spec.optional else true
    if spec.hasOwnProperty('default')
      @default = @schema.makeDefault spec.default
      @required = false
    # besides name - everything else specified here should be considered constraint...
    # hmm...
    # let's see how we are going to do that...
    @initializeOtherConstraints(spec)
  
  initializeOtherConstraints: (spec) ->
    @constraints = []
    #for key, val of spec
    #  if key != 'name' and key != 'type' and key != 'optional' and key != 'default'
    #    args = {}
    #    args[key] = val
    #    @constraints.push @schema.makeConstraint args
  noValue: (v) ->
    v == null or v == undefined # 0, [], '' are all valid values
  validate: (val, toError) ->
    # required = true and value => validate
    # required = false and value => validate
    # required = true and not value => error
    # required = false and not vale => okay
    # default should be applied when there are no values being supplied.
    if @required and @noValue(val) 
      if toError
        throw new Exception error: 'value_required', column: @name, value: val
      else
        undefined
    else if not @required and @noValue(val)
      val
    else
      @type.validate val, toError
  validateAsync: (val, cb) ->
    helper = (constraint, next) =>
      constraint.validateAsync val, next
    if @required and @noValue(val)
      cb {error: 'required', column: @name, value: val}, val
    else if not @required and @noValue(val)
      cb null, val
    else
      @type.validateAsync val, (err, res) =>
        if err
          cb err, val
        else
          async.forEach @constraints, helper, (err, res) ->
            cb err, val
  make: (val) ->
    if @noValue(val) and @default
      return @default.run()
    else if @required and @noValue(val)
      throw new Exception error: 'required', column: @name, value: val
    else if not @required and @noValue(val)
      val
    else # ensure the value is validated.
      @type.make val
  makeAsync: (val, cb) ->
    if @noValue(val) and @default
      try
        defaultVal = @default.run()
        cb null, defaultVal
      catch e
        cb e
    else if @required and @noValue(val)
      cb {error: 'required', column: @name, value: val}, val
    else if not @required and @noValue(val)
      cb null, val
    else
      @type.makeAsync val, cb
  alter: (spec) ->
    {type, optional} = spec
    defaultSpec = spec.default
    # how do we know whether or not type is the same as before?
    # it might not be easily tested but we can certain recreate it easily.
    if type
      @type = @schema.makeType spec.type
    if spec.hasOwnProperty('optional')
      @required = if spec.hasOwnProperty('optional') then not spec.optional else true
    if spec.hasOwnProperty('default')
      @default = @schema.makeDefault spec.default
      @required = false
  describe: () ->
    output =
      name: @name
      type: @type.decl()
      optional: not @required
    if @default
      output['default'] = @default.describe()
    output
  equiv: (prop) ->
    @type.equiv prop.type
  fullEquiv: (prop) ->
    result = true
    if @default and prop.default
      result = @default.equiv(prop.default)
    else if @default or prop.default 
      result = false
    result and @equiv(prop) and @required == prop.required
  diff: (prop) ->
    if @fullEquiv(prop)
      null
    else
      {left: @describe(), right: prop.describe()}

# time to serialize the full system...
# what do we serialize out? we use what we have described to serialize out the information.
# 1 - each custom defined type MUST be serializable... that means we'll need to describe the defaults
# too...

class Composite extends Type
  @assertValidColumns = (props) ->
    uniques = {}
    for {name}, i in props
      assert.ok not uniques.hasOwnProperty(name)
      uniques[name] = name
  constructor: (schema, {@name, columns}) ->
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
    assert.ok typeof(@name) == 'string' and @name != ''
    @constructor.assertValidColumns columns
    @columns = {}
    @orderedColumns = []
    for spec in columns
      {name} = spec
      column = new Property @schema, spec
      @columns[name] = column
      @orderedColumns.push column
  validate: (obj, toError) ->
    if obj instanceof Object
      # validate each column one at a time.
      try
        for column, i in @orderedColumns
          column.validate obj[column.name], true
        obj
      catch e
        if toError
          throw e
        else
          undefined
    else
      if toError
        throw new Exception error: 'invalid_model', name: @name, value: obj
      else
        undefined
  validateAsync: (obj, cb) ->
    helper = (column, next) ->
      column.validateAsync obj[column.name], next
    if not obj instanceof Object
      cb {error: 'invalid_object', name: @name, value: obj}, obj
    else # we'll need to validate each column one at a time.
      async.forEach @orderedColumns, helper, (err, res) ->
        cb err, obj
  make: (obj = {}) ->
    copy = _.extend {}, obj
    for column, i in @orderedColumns
      copy[column.name] = column.make copy[column.name]
    copy
  makeAsync: (obj = {}, cb) ->
    copy = _.extend {}, obj
    helper = (column, next) ->
      column.makeAsync copy[column.name], next
    if not obj instanceof Object
      cb {error: 'invalid_object', name: @name, value: obj}, null
    else
      async.forEach @orderedColumns, helper, (err, res) ->
        cb err, copy

# a model is made up of columns...
# this differs from a composite as Model are *active* - they have other properties
# attached to them.
# a model should create not just an object - but also a class... this can be hard...
# the question is... what should the object be blessed with?
class Model extends Type
  constructor: (schema, options) ->
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
    try
      @name = options.name
      @makerAsync = options.makerAsync or ((obj, cb) -> cb null, obj)
      assert.ok typeof(@name) == 'string' and @name != '', 'missing_name'
      assert.ok @makerAsync instanceof Function, 'makerAsync_not_function'
      @columns = {}
      @orderedColumns = []
      @indexes = {}
      for spec in options.columns
        @addColumn spec
      @schema.models.register @name, @
      # process the constraints at this point... the 
      for spec in options.columns
        @addColumnIndex spec
      for spec in options.constraints or []
        @addConstraint spec 
      for key, val of options
        if key != 'name' and key != 'columns' and key != 'makeAsync' and key != 'constraints'
          @[key] = val
    catch e
      # we'll need to rollback on the changes that we have caused... dang... for now just delete
      # from schema.models & schema.indexes...
      @schema.models.delete @name
      for spec in options.columns
        @dropColumnIndex spec
      throw e
  validate: (obj, toError) ->
    if obj instanceof Object
      # validate each column one at a time.
      try
        for column, i in @orderedColumns
          column.validate obj[column.name], true
        obj
      catch e
        if toError
          throw e
        else
          undefined
    else
      if toError
        throw new Exception error: 'invalid_model', name: @name, value: obj
      else
        undefined
  validateAsync: (obj, cb) ->
    helper = (column, next) ->
      column.validateAsync obj[column.name], next
    if not obj instanceof Object
      cb {error: 'invalid_object', name: @name, value: obj}, obj
    else # we'll need to validate each column one at a time.
      async.forEach @orderedColumns, helper, (err, res) ->
        cb err, obj
  make: (obj = {}) ->
    copy = _.extend {}, obj
    for column, i in @orderedColumns
      copy[column.name] = column.make copy[column.name]
    copy
  makeAsync: (obj = {}, cb) ->
    copy = _.extend {}, obj
    helper = (column, next) ->
      column.makeAsync copy[column.name], (err, res) ->
        if err
          next err
        else
          copy[column.name] = res
          next null, res
    if not obj instanceof Object
      cb {error: 'invalid_object', name: @name, value: obj}, null
    else
      async.forEach @orderedColumns, helper, (err, res) =>
        if err
          cb err, res
        else
          @makerAsync copy, cb
  alter: (changes) ->
    for change in changes
      logger.debug 'Model.alter', changes, change.alter
      if change.alter == 'addColumn' # exp must be good at add column
        @addColumn change.exp
      else if change.alter == 'dropColumn'
        @dropColumn change.exp
      else if change.alter == 'alterColumn'
        @alterColumn change.exp
      else if change.alter == 'addConstraint'
        @addConstraint change.exp
      else if change.alter == 'dropConstraint'
        @dropConstraint change.exp
      else
        throw new Exception error: 'unsupported_alter_statement', changes
  alterColumn: (spec) ->
    {name, type, optional} = spec
    defaultDecl = spec.default
    if not @columns.hasOwnProperty(name)
      throw new Exception error: 'non_existant_column', value: spec
    else
      column = @columns[name]
      column.alter spec 
  dropColumn: (name) ->
    if @columns.hasOwnProperty(name)
      delete @columns[name]
      @orderedColumns = _.filter @orderedColumns, (column) -> column.name != name
      # we'll also delete the 
  addColumn: (spec) ->
    {name} = spec
    if @columns.hasOwnProperty(name)
      throw new Exception error: 'column_duplicated', value: spec
    column = new Property @schema, spec
    @columns[name] = column # why is this an array?
    @orderedColumns.push column
  defaultConstraintName: (spec) ->
    # take the first column in the columns... to create a name...
    prefix
    if spec.primary
      prefix = 'p'
    else if spec.unique
      prefix = 'u'
    else if spec.foreign
      prefix = 'f'
    else
      prefix = 'c'
    spec.name or "#{prefix}_#{@name}_#{spec.columns?[0]}"
  dropConstraint: (name) ->
    logger.debug 'Model.dropConstraint', name
    @schema.dropIndex name: name
  addConstraint: (spec) -> # need to consolidate with addColumnIndex...
    logger.debug 'Model.addConstraint', spec
    name = @defaultConstraintName(spec)
    if spec.primary
      @schema.createIndex
        name: name
        table: @name # our table has not yet exist at this point... we'll have to do so by forcing adding ourselves in?
        columns: spec.columns
        primary: true
    else if spec.unique
      @schema.createIndex
        name: name
        table: @name
        columns: spec.columns
        unique: true
    if spec.foreign
      @schema.createIndex
        name: name
        table: @name
        columns: spec.columns
        foreign: spec.foreign
    logger.debug "Schema.index[#{name}] = ", @schema.indexes.has(name)
  addColumnIndex: (spec) ->
    logger.debug 'Model.addColumnIndex', spec
    if spec.primary
      @schema.createIndex
        name: "p_#{@name}_#{spec.name}"
        table: @name # our table has not yet exist at this point... we'll have to do so by forcing adding ourselves in?
        columns: [ spec.name ]
        primary: true
    else if spec.unique
      @schema.createIndex
        name: "u_#{@name}_#{spec.name}"
        table: @name
        columns: [ spec.name ]
        unique: true
    if spec.foreign
      @schema.createIndex
        name: "f_#{@name}_#{spec.name}"
        table: @name
        columns: [ spec.name ]
        foreign: spec.foreign
  dropColumnIndex: (spec) ->
    if spec.primary
      @schema.indexes.delete "p_#{@name}_#{spec.name}"
    else if spec.unique
      @schema.indexes.delete "u_#{@name}_#{spec.name}"
    if spec.foreign
      @schema.indexes.delete "f_#{@name}_#{spec.name}"
  describe: () ->
    indexes = []
    for key, index of @indexes
      indexes.push index.describe()
    name: @name
    columns: (column.describe() for column in @orderedColumns)
#    indexes: indexes
  createIndex: (spec) ->
    @verifyIndexSpec spec
    @indexes[spec.name] = new Index @schema, spec
    logger.debug 'createIndex', spec.name, '=>', @indexes[spec.name]
    @indexes[spec.name]
  dropIndex: (spec) ->
    if @indexes.hasOwnProperty spec.name
      delete @indexes[spec.name]
  verifyIndexSpec: (spec) ->
    for key, index of @indexes
      if index.equiv spec # we should throw error.
        throw new Exception error: 'equivalent_index_exist', value: index
  diff: (table) ->
    columnsDiff = obj.diff @orderedColumns, table.orderedColumns, false
    logger.debug 'Model.diff', columnsDiff
    if columnsDiff
      {left, right, both} = columnsDiff
      leftDiff = {}
      for key, col of left
        leftDiff[key] = col.describe()
      rightDiff = {}
      for key, col of right
        rightDiff[key] = col.describe()
      bothDiff = {} # things that are changed...
      for key, col of both # the col is the number.
        result = @orderedColumns[key].diff(table.orderedColumns[key])
        if result
          bothDiff[key] = result
      columns:
        left: leftDiff
        right: rightDiff
        both: bothDiff
    else
      null

# if we are to alter table column we'll have to deal with the indexes...!!!
# do I just want to convert this into foreign key constraints?
class Index
  constructor: (schema, {name, table, columns, unique, foreign, primary}) ->
    Object.defineProperty @, 'schema', {value: schema, writable: false, enumerable: false, configurable: false}
    # check to see if the table exists...
    @name = name
    @table = @schema.models.has table
    if not @table
      throw new Exception error: 'unknown_table', name: table
    else
      # we now have a table... we should also verify that the columns exist..
      @columns = @verifyColumnsExist @table, columns
    @unique = unique or false
    @assertSinglePrimaryKey primary
    # deal with foreign key references.
    if foreign
      @foreign = @schema.models.has foreign.table
      if not @foreign
        throw new Exception error: 'unknown_foreign_table', name: foreign.table
      else
        @references = @verifyForeignKeyMatch @foreign, foreign.columns
  assertSinglePrimaryKey: (primary = false) ->
    if primary
      for key, index of @table.indexes
        if index.primary # already has a primary key -> fail
          throw new Exception error: 'multiple_primary_key_declared', table: @table.name
    @primary = primary
  verifyForeignKeyMatch: (foreign, references) ->
    columns = @verifyColumnsExist foreign, references
    for [col, ref] in _.zip(@columns, columns)
      logger.debug 'Index.verifyForeignKeyMatch', col, ref
      if not col.equiv ref
        throw new Exception error: 'foreign_key_column_must_be_equivalent', column: col, reference: ref
    columns        
  verifyColumnsExist: (table, columns) ->
    output = 
      for column in columns
        if not table.columns.hasOwnProperty(column)
          throw new Exception error: 'unknown_column', name: "#{@table.name}.#{column}"
        table.columns[column]
    output
  equiv: (spec) ->
    @sameColumns(spec.columns) and @sameType(spec)
  sameType: (spec) ->
    (@primary and spec.primary) or (@unique and spec.unique) or (@foregin and spec.foreign)
  sameColumns: (columns) ->
    colMap = {}
    for column in columns
      colMap[column] = column
    for col in @columns
      if not colMap.hasOwnProperty(col.name)
        return false
    return true
  describe: () ->
    output = 
      name: @name
      table: @table.name
      columns: (column.name for column in @columns)
      unique: @unique or @primary
      primary: @primary
    if @foreign
      output.foreign =
        table: @foreign.name
        columns: (column.name for column in @references)
    output
  #equiv: (index) ->
  #  obj.deepEqual @describe(), index.describe()
  diff: (index) ->
    if obj.deepEqual @describe(), index.describe()
      null
    else
      {left: @describe(), right: index.describe()}

module.exports =
  Type: Type
  Property: Property
  Composite: Composite
  Model: Model
  ListOf: ListOf
  Constraint: Constraint
  Default: Default
  Schema: Schema
  Registry: Registry