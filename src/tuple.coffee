async = require '../shared/async'
Exception = require '../shared/exception'
RefHolder = require './refholder'
BaseProperty = require './baseproperty'
Property = require './property'
Environment = require './environment'
Type = require './type2'
BuiltIn = require './builtin'
OPCodes = require './opcodes'
logger = require '../shared/log'

# every object can have a type tag.
# __type__
class Tuple extends Type
  @compile: (vm, exp, env) ->
    {tuple, props} = exp
    argsEnv = {}
    for {name} in props
      if argsEnv.hasOwnProperty(name)
        throw new Exception error: 'duplicate_tuple_property_name', value: props
      argsEnv[name] = 1
    propList = # we should allow for 
      for prop in props
        item = Property.make vm, prop, env, argsEnv
    for prop in props
      propList[prop.name] = prop
    [[OPCodes.push, propList], [OPCodes.makeTuple, tuple]]
  @makeNewEnv: (args, env) ->
    argsEnv = {}
    for {name} in args
      argsEnv[name] = new RefHolder(name)
    new Environment(argsEnv, env)
  makeIsaPropHelper: (vm, tuple) ->
    (prop, next) ->
      code = [[OPCodes.push, tuple], [OPCodes.push, prop.isa], [OPCodes.tupleCall, 2]]
      vm.pushCallStack code
      vm.runAsync (err, res) ->
        next null, [prop.name, err, res]
  makeCtorPropHelper: (vm, tuple) ->
    (prop, next) ->
      code = [[OPCodes.push, tuple], [OPCodes.push, prop.ctor], [OPCodes.tupleCall, 2]]
      vm.pushCallStack code
      vm.runAsync (err, res) ->
        next null, [prop.name, err, res]
  makeFinalHelper: (tuple, resultProc, cb) ->
    (err, res) ->
      if err
        cb err
      else
        errObj = null
        for [name, e, r] in res
          if e != null
            if errObj == null
              errObj = {}
            else
              errObj[name] = e
        if errObj != null
          cb errObj
        else
          cb null, resultProc(tuple, res)
  isaResultProc: (tuple, res) ->
    result = true
    for [name, e, r] in res
      if not r
        return false
    result
  ctorResultProc: (tuple, res) ->
    result = {}
    for [name, e, r] in res
      result[name] = r
    result
  @makeIsa: (type, vm, env) ->
    BuiltIn.make 'isa', ['tuple'], (tuple, cb) ->
      if not tuple instanceof Object
        cb new Exception error: 'not_a_tuple', value: tuple
      else
        helper = type.makeIsaPropHelper vm.clone(), tuple
        async.map type.props, helper, type.makeFinalHelper(tuple, type.isaResultProc, cb) 
  @makeCtor: (type, vm, env) ->
    BuiltIn.make 'isa', ['tuple'], (tuple, cb) ->
      if not tuple instanceof Object
        cb new Exception error: 'not_a_tuple', value: tuple
      else
        helper = type.makeCtorPropHelper vm.clone(), tuple
        async.map type.props, helper, type.makeFinalHelper(tuple, type.ctorResultProc, cb) 
  @make: (vm, {name, props}, env) ->
    type = new Tuple name, props
    type.isa = @makeIsa type, vm, env
    type.ctor = @makeCtor type, vm, env
    type
  constructor: (@name, @props) ->
    # let's think through about the list of the props...
    # is this where the things are being made?
  join: (tuple) ->
    # we'll need to combine the tuple's name... we'll need to ensure that there are no duplicate
    # names...
    # we'll need to deal with the aliases, and see how that works...
  isa: (obj) ->
    if obj.__type == @
      true
    else
      # we'll need to go through verifying the object's fields and how they
      # are all correct...
      false
  ctor: (obj) ->
    
module.exports = Tuple

