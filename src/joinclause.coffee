Relation = require './relation'

# 
# let's see if we can get joins to work correctly...
#
class JoinClause extends Relation
  
  # 
  # 4 things makes up a join clause.
  # 1 - the lhs
  # 2 - the rhs
  # 3 - the exp on how the join occurs
  # 4 - the type of join (inner, left, right, and cross)
  # 
  @compile: (vm, {lhs, rhs, exp, join}, env ) ->
    # 
    # to compile for this... the question is... how do we do test prior to
    # that? it means our ability to resolve the tables will be called into question...
    # the inner object needs to be compiled first before it comes to prepare for the
    # outer object?
    # by separating compilation phase from the making phase - we might end up doing double
    # work... something to think about.
    # 
    # the key thing to think about is
    #
    code = [[OPCodes.push, join]]
    lhsRel = SelectQuery.resolveRelation vm, lhs, env
    code.push [OPCodes.push, lhsRel]
    rhsRel = SelectQuery.resolveRelation vm, rhs, env
    code.push [OPCodes.push, rhsRel]
    code.push [OPCodes.push, exp]
    code.push [OPCodes.makeJoinClause, 4]
    code

  @make: (vm, {lhs, rhs, join, exp}, env) ->
    # 
    # we'll have to compile the exp into what it actually will be...
    # we have two different ways of going about this...
    # 
    # 1 - let lhs & rhs's alias being the variable that's bound.
    #
    # 2 - it seems that there might need to be a different way of handling the types?
    # ??
    # when it's test.<column> -> I don't really know whether or not the column exists...
    # let's do the simplest thing for now?
    # 
    code = @compileExp vm, lhs, rhs, exp, env
    # we also need to deal with compiling of the type...
    # basically we'll need a new Tuple...
    new JoinClause lhs, rhs, join, code
  @compileExp: (vm, lhs, rhs, exp, env) ->
    newEnv = {}
    newEnv[lhs.alias or lhs.id] = lhs.type.props
    newEnv[rhs.alias or rhs.id] = rhs.type.props
    vm.compile exp, new Environment(newEnv, env)
     
  constructor: (@lhs, @rhs, @joinType, @joinExp) ->
    # the type ought to be two of them join'd together...??
    # we'll have to decide what that means...!!!

module.exports = JoinClause
