assert = require 'assert'
_ = require 'underscore'
Exception = require '../shared/exception'
async = require '../shared/async'
Registry = require './registry'
Schema = require './schema'
Default = require './default' # will there be a circular binding potential? don't know yet
Type = require './type'
{Field, FieldList} = require './field'
obj = require './object'
Constraint = require './constraint'
logger = require '../shared/log'

# Index is a type of constraint. for now the only type I guess...
# there shouldn't be new types of indexes created, so they can be instance objects
# rather than class methods that are being registered...
# (i.e. they are like fields rather than types or tables)

class BaseTable extends Type.has('object')
  # do we want to call them fields rather than columns? hmm...
  @constraints: []
  @process: (newClass, {fields, constraints} = newClass) ->
    # we'll need to process the fields for indexes
    # plus also processing the constraints
    newFields =
      for spec in fields or []
        new Field newClass, spec # field does not exist... this can be done via keeping a @base
    newClass.fields = @fields.concat newFields
    #logger.debug 'Table.process', newClass.name, fields, newClass.fields
    newClass.constraints = []
    newConstraints =
      for spec in constraints or []
        Constraint.create _.extend({table: newClass}, spec)
    for spec in fields or []
      if spec.primary
        newConstraints.push Constraint.create type: 'index', table: newClass, columns: [spec.name], primary: true
      if spec.unique
        newConstraints.push Constraint.create type: 'index', table: newClass, columns: [spec.name], unique: true
      if spec.foreign
        newConstraints.push Constraint.create type: 'index', table: newClass, columns: [spec.name], unique: spec.foreign        
    # we actually do not care what these guys are in order...
    # also - constraints makes a bit less sense to do so with
    # for now this is okay I guess... we'll come back to more constraints as needed.
    newClass.constraints = @constraints.concat newConstraints
    newClass
  @diff: (table) -> # should only be called if the table have the same name.
    @fields.diff table.fields
  @join: (objectType) ->
    newType = class x extends BaseTable # by default it's just an object that has basic operations
    # i.e. it'll lose many of its values?
    newType.fields = @fields.join objectType.fields
    newType

# a Table is a Type... we ought to ensure that we have the things attached appropriately.
# Table should inherit from object
# or should table extends Type? Then any other tables
class Table
  @BaseTable: BaseTable
  @registerSchema: (schema) ->
    assert.ok schema instanceof Schema, "invalid_schema: #{schema}"
    @schema = schema
    @schema.setClass
      class: 'tables'
      validate: @validateTable # this returns the table... one thing to keep in mind is that
    logger.debug 'Table.registerSchema', @schema.tables.validate
  @declare: (name) ->
    logger.debug 'Table.declare', name
    if not typeof(name) == 'string'
      throw new Exception error: 'unsupported_table_declare_type', value: name
    if name == 'base' or name == 'BaseTable'
      BaseTable
    else
      Schema.current.hasTable name
  @register: (spec) ->
    # how do I ensure that I register the table...
    # given that 
    @schema.registerTable spec
  @validateTable: (spec) => # why did it not run through this function anymore? is this not loaded?
    logger.debug 'Table.ctor', spec, spec.base
    if spec instanceof Function # we are done.
      @processTable spec
    else # this is something that we'll have to create and extend.
      # anonymous table without a base is fine... by default it would just be an empty table
      # this is where it's different from type... for now this works.
      {@name, base, fields} = spec
      baseType =
        if base
          @declare base
        else
          BaseTable
      spec = baseType.extend spec
      spec.baseType = baseType
      spec
  @processTable: (table, spec = table) ->
    baseType =
      if table.base
        @declare table.base
      else
        BaseTable
    table.baseMixin baseType
    baseType.process table, spec
    table
  
  @has: (name) ->
    @schema.hasTable name

Table.registerSchema Schema.current

module.exports = Table
