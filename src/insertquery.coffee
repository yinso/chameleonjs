Relation = require './relation'

Type = require './type2' # we might be able to inherit from Table... but do we need to?
async = require '../shared/async'
_ = require 'underscore'
logger = require '../shared/log'
Exception = require '../shared/exception'
OPCodes = require './opcodes'
Tuple = require './tuple'
BuiltIn = require './builtin'
Environment = require './environment'
SelectQuery = require './selectquery'

# is insert query a relation?
# not really
# a select query IS NOT a RELATION... I think that's the revelation!!!!
# the return of a select query can result in a *view* for sure!!!!

# 
# insert query is extremely *simple*...
# but this drives how everything will be done going forward.
# i.e. it deals with the internal representation of the data...
# most of them are likely to return an object rather than return an array.
# so we have to manage on the object that's being returned.
# the goal is to do as little change as possible to the actual 
#
# okay - so we'll work with 
class InsertQuery
  @compile: (vm, exp, env) ->
    {insert, values, select} = exp
    logger.debug 'InsertQuery.compile', insert, values, select
    code = [] # first - we'll push the values down...
    if values instanceof Array
      try
        code = code.concat.apply code, (vm.compile(value, env) for value in values)
        code.push [OPCodes.makeArray, values.length]
      catch e
        logger.error "InsertQuery.compileValuesError", e.message
        throw e
    else if select
      code = code.concat SelectQuery.compile(vm, select, env, false)
    else
      throw new Exception error: 'invalid_insert_exp', value: exp
    # we'll need to resolve this to find it being a relation that can be inserted...
    table = @resolveRelation vm, insert, env
    if not (table instanceof Relation)
      throw new Exception error: 'not_a_relation', value: table
    else
      code = code.concat vm.compile(insert, env)
      code.push [OPCodes.makeInsertQuery], [OPCodes.insert]
      code
  @resolveRelation: (vm, exp, env) ->
    vm.evalExpSync exp, env
  @make: (vm, {insert, values}, env) ->
    new InsertQuery insert, values
  constructor: (@insert, @values) ->
  exec: (vm, cb) ->
    if @values instanceof SelectQuery
      @values.exec vm, (err, res) =>
        if err
          cb err
        else
          @insert.insertView vm, res, cb
    else
      @insert.insert vm, @values, cb

module.exports = InsertQuery