assert = require 'assert'
Exception = require '../shared/exception'
obj = require './object'
uuid = require '../shared/uuid'
crypto = require '../shared/crypto'
Registry = require './registry'
Schema = require './schema'

# this has its own registry of functions...
# might want to bring in a generalized version of a Function concept... i.e. this is pretty
# close to running of a Function object anyways...
# will have to determine how to tie them back here...


# this is extremely different... hmm... this is arguably not part of the chain.
class Default
  @registerSchema: (schema) ->
    assert.ok schema instanceof Schema, "invalid_schema: #{schema}"
    @schema = schema
    @schema.setClass class: 'defaults', type: @, validate: ({name, proc}) -> proc
  
  @register = ({name, proc}) ->
    @schema.registerDefault name: name, proc: proc
  @has = (name) ->
    @schema.hasDefault name
  constructor: (@spec) ->
    if spec instanceof Object # we can return object? for now, no.
      if spec.proc and @constructor.has spec.proc
        @inner = @constructor.has spec.proc
        @args = spec.args or []
      else if spec.value
        @val = spec.value
      else if spec.object # allow for passing in of an object... what do we do with this?
        @val = spec.object # allow for returning of objects...
      else
        throw new Exception error: 'invalid_default_spec', spec: spec
    else
      @val = spec
  run: () ->
    if @inner
      @inner.apply @, @args
    else
      return @val
  describe: () ->
    @spec
  equiv: (def) ->
    obj.deepEqual @describe(), def.describe()
  diff: (def) ->
    if not @equiv def
      {left: @describe(), right: def.describe()}
    else
      null

Default.registerSchema Schema.current

Default.register
  name: 'timestamp'
  proc: () ->
    (new Date()).getTime()

Default.register
  name: 'make_uuid'
  proc: () -> uuid.v4() # this should be built-in...

Default.register
  name: 'make_crypto_salt'
  proc: crypto.makeSalt


module.exports = Default
