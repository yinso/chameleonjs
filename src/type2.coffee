BuiltIn = require './builtin'
Registry = require './registry'
BaseProperty = require './baseproperty'

# are we going to store type in a specific array?
# that's possible...
# we now can handle type expression with a where clause.
# are we going to deal with pattern matching?
# that's quite possible...
# the only pattern matching these days are going to occur on the left hand side...
# that means this will require a parsing...
# it's a type of def clause...
# we'll ensure that it's lead by a def clause or something else... 
class Type extends BaseProperty
  @_reg = new Registry noDupe: true
  @register: (name, isa, ctor) ->
    @_reg.register new Type(name, isa, ctor)
  @registerSync: ({name, isa, isaArgs, ctor, ctorArgs}) ->
    @register name, BuiltIn.makeSync('isa', isaArgs, isa), BuiltIn.makeSync('ctor', ctorArgs, ctor)
  @declare: (type) ->
    @_reg.has type
  @compile: (vm, exp, env) ->
  constructor: (@name, @isa, @ctor) -> # is this correct? by default this makes sense.
    #if not @isa instanceof BuiltIn or not @isa instanceof FunctionObj
    #  throw new Exception error: 'invalid_validator', value: @isa
    #if not @maker instanceof BuiltIn or not @maker instanceof FunctionObj
    #  throw new Exception error: 'invalid_validator', value: @maker

Type.registerSync
  name: 'integer'
  isaArgs: ['v']
  isa: (v) ->
    typeof(v) == 'number' and (v % 1) == 0
  # we'll need to have an ability to convert the data from one type to another...
  ctorArgs: ['v']
  ctor: (v) ->
    if typeof(v) == 'number' and (v % 1) == 0
      v
    else if typeof(v) == 'string'
      val = parseInt(v)
      if val.toString() != v
        throw new Exception error: 'cannot_be_converted_to_integer', value: v
      else
        val
    else
      throw new Exception error: 'cannot_be_converted_to_integer', value: v

Type.registerSync
  name: 'float'
  isaArgs: ['v']
  isa: (v) ->
    typeof(v) == 'number' 
  # we'll need to have an ability to convert the data from one type to another...
  ctorArgs: ['v']
  ctor: (v) ->
    if typeof(v) == 'number'
      v
    else if typeof(v) == 'string'
      val = parseFloat(v)
      # okay - how do we want to deal with this problem?
      if val.toString() != v
        throw new Exception error: 'cannot_be_converted_to_float', value: v
      else
        val
    else
      throw new Exception error: 'cannot_be_converted_to_float', value: v

Type.registerSync
  name: 'string'
  isaArgs: ['v']
  isa: (v) ->
    typeof(v) == 'string'
  ctorArgs: ['v']
  ctor: (v) ->
    v.toString()

#Type.registerSync
#  name: 'function'
#  isaArgs: ['v']
#  isa: (v) ->
#    v instanceof BuiltIn
#  ctorArgs: ['v'] # one cannot create via a ctor!
#  ctor: (v) -> # 

# typeof ought to be a knowledge that exists at this level...
# should 


# in order to deal with object we'll have to know the actual fields...
# so this is probably just something that we'll *register* the type into base environment...

module.exports = Type
