###

opcodes

push - push a value to the data stack
newEnv - create a new environment and daisychain
set name - pop the latest, set the value to the name in the env
call <number> - call a procedure with the top being the function.
ret - return the result of the env; this will pop the latest env & push the result
tailcall - tailcall a procedure

###

OPCodes =
  #
  # environment-specific opcodes
  # 
  newEnv: 'newEnv' # creates a brand new empty environment
  pushEnv: 'pushEnv' # pop an object from data stack and push onto the environment stack
  popEnv: 'popEnv' # pop the environment stack
  updateEnv: 'updateEnv' # pop an object from the data stack and update the top of the environment stack
  updateRef: 'updateRef'
  setEnv: 'setEnv' # arg: <key> - pop the top of the data stack and set <key> against the environment
  defEnv: 'defEnv' # arg: <key> - take the top of the stack and def <key> against the environment
  refEnv: 'refEnv' # arg: <key> - maybe call this refEnv?

  #
  # object-specific opcodes
  # 
  member: 'member' # arg: <key>

  #
  # debugging
  # 
  show: 'show'
  describe: 'describe'

  #
  # data stack specific opcodes
  # 
  push: 'push' # arg: <val> - push data onto the data stack
  pop: 'pop' # pop data from the data stack
  dupe: 'dupe' # create the top item and push it again.
  
  #
  # closure-specific opcodes
  # 
  refClosure: 'refClosure'
  resolveClosure: 'resolveClosure'

  #
  # function call specific opcodes
  # 
  call: 'call'
  return: 'return' 
  tailCall: 'tailCall'
  # we can just make it so that way we cannot use tupleCall with BuiltIn...
  tupleCall: 'tupleCall' # when the arg is a tuple call.
  tupleTailCall: 'tupleTailCall' # tuple tail call...
  # or maybe we have a builtIn that'll handle the appropriate tupleCall.
  # just call it apply.
  # apply is in a way special for sure...
  # the question is... how special...
  # we should have the apply as a function...
  # but if we have that then we'll need to have pass in VM... let's see how that looks.

  #
  # branch upcodes
  # 
  ifNotJump: 'ifNotJump' # this will be based on the condition currently on top of the stack...
  jump: 'jump'
  label: 'label' # we'll need to deal with codeblock now... it seems... 
  goto: 'goto'

  #
  # exception opcodes
  # 
  throw: 'throw' # throw what's on top of stack... how to deal with stack trace?
  # let's revisit the style of erlang exceptions... hmm...
  #makeError: 'makeError' # the same as makeObject.

  #
  # type-specific opcodes
  # 
  makeTuple: 'makeTuple'
  makeRelation: 'makeRelation'
  
  #
  # arg validation opcodes
  # 
  validateRequired: 'validateRequired'
  
  #
  # constructor opcodes
  # 
  makeArray: 'makeArray'
  makeObject: 'makeObject'
  makeObjectViaKeys: 'makeObjectViaKeys'
  makeFunction: 'makeFunction'
  makeArg: 'makeArg'
  
  #
  # vm-specific opcodes
  # 
  trace: 'trace'
  untrace: 'untrace'
  
  # 
  # relation-specific opcodes
  # NOTE that this is an async proposition...
  # pretty much all relation-based statements are async!!!
  # this also appears to be a problem though... needs to think through on how
  # much of these can be converted over to sync positions
  #
  insert: 'insert'
  insertView: 'insertView'
  select: 'select'
  makeSelectQuery: 'makeSelectQuery'
  makeJoinClause: 'makeJoinClause'
  makeInsertQuery: 'makeInsertQuery'
  makeAlias: 'makeAlias'

  # relation specific - deal with it in cursor situation.
  cursorRef: 'cursorRef'
  pushCursor: 'pushCursor'
  popCursor: 'popCursor' # this can be done as part of stack pop anyways... but have it for symmetry

module.exports = OPCodes
