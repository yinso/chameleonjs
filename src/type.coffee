Exception = require '../shared/exception'
_ = require 'underscore'
Default = require './default'
async = require '../shared/async'
Registry = require './registry'
Class = require '../shared/class'
assert = require 'assert'
Schema = require './schema'
Validator = require './validator'
{Field, FieldList} = require './field'
obj = require './object'
logger = require '../shared/log'

Function::mixin = (parents...) ->
  # if a prop exists in multiple parents - the first one wins
  for parent in parents
    for key, prop of parent
      if parent.hasOwnProperty(key) and not @hasOwnProperty(key)
        @[key] = prop
  @

Function::baseMixin = (parent) ->
  for key, prop of parent
    if parent.hasOwnProperty(key) and not @hasOwnProperty(key)
      @[key] = prop
  @__base__ = parent
  @

Function::clone = (name = undefined) ->
  cloneObj = @__cloned or @
  temp =
    if name
      eval "function #{name}() { return cloneObj.apply(this, arguments); }; #{name}"
    else
      () ->
        cloneObj.apply @, arguments
  for key, val of @
    if @hasOwnProperty(key) 
      temp[key] = val
  temp.__cloned = cloneObj
  temp

Function::setBaseClass = (parent) ->
  @mixin parent
  ctor = ->
  ctor.prototype = parent.prototype
  @prototype = new ctor()
  @prototype.constructor = @
  @__base__ = parent
  @__super__ = parent.prototype
  logger.debug 'Function::setBaseClass', @__base__
  @

# every type 
class Type
  @validate: (name, val, cb) ->
    if @has(name)
      @has(name).validate val, cb
    else
      cb error: 'unknown_type', name: name
  @make = (name, val, cb) ->
    @validate name, val, (err, res) =>
      if err
        cb err
      else
        @has(name).make val, cb
  @declare: (option) ->
    if option == undefined or option == null
      throw new Exception error: 'empty_type_spec'
    if typeof option == 'string'
      @has option
    else
      # what happens when we declare with this? hmm...
      @validateType option
  @validateType: (spec) => # are we not getting in here?
    logger.debug 'Type.ctor', spec, spec.base
    if spec instanceof Function # we are done.
      if spec.base
        logger.debug 'spec.base', spec.base
        baseType = @declare spec.base
        spec.baseMixin baseType
        baseType.process spec
#      else if spec.__super__ # this is the object that's the original constructor.
#        spec.baseMixin spec.__super__.constructor
      spec
    else # this is something that we'll have to create and extend.
      {@name, base} = spec
      if not base
        throw new Exception error: 'base_cannot_be_empty', value: spec
      baseType = @declare base
      baseType.extend spec
  @register = (type) ->
    @schema.registerType type
  @has = (name) ->
    @schema.hasType name
  @registerSchema: (schema) -> 
    assert.ok schema instanceof Schema, "invalid_schema: #{schema}"
    @schema = schema
    @schema.setClass
      class: 'types'
      validate: @validateType

Type.registerSchema Schema.current

Field.Type = Type

class any
  @validator = new Validator {}
  @validatorKeys = []
  @makeValidator: (spec) ->
    output = {}
    for key in @validatorKeys
      if spec.hasOwnProperty(key)
        output[key] = spec[key]
    new Validator output
  @validate: (val, cb) ->
    #logger.debug 'integer.parse', val
    if @validateSync(val) != false
      @validator.validate val, cb
    else
      cb new Exception error: 'failed_validation', value: val
  @validateSync: (val) ->
    val == val
  @make: (val, cb) ->
    @validate val, cb
  @extend: (spec) -> # so how the hell does this extend work?
#    if not spec instanceof Function
    newClass = @clone spec.name
#    if spec.name # how do I fix this part?
#      # baseType is a function... we should take the ??? what should we do here?
#      newClass = @clone(spec.name)
#      #newClass = eval "function #{spec.name} (val) { }; #{spec.name}"
#    else
#      #newClass = (val) -> 
#      newClass = @clone()
    for key, val of spec
      newClass[key] = val
    @process newClass, spec
    newClass.setBaseClass @
  @process: (newClass, spec = newClass) ->
    newClass.validator = @makeValidator spec
  @describe: () ->
    output = {}
    if @name
      output.name = @name
    if @__base__ and @__base__ != any
      output.base = @__base__.name
    _.extend output, @validator.describe()
    output
  @nameOrDescribe: () ->
    if @name and Type.has @name
      @name
    else
      @describe()
  @diff: (type) ->
    obj.diff @describe(), type.describe()

Type.register any

class string extends String
  @baseMixin any
  @validatorKeys = ['minLength', 'maxLength', 'regex'] # these are the additional properties.
  @validateSync: (val) ->
    typeof val == 'string'

Type.register string

class integer extends Number
  @baseMixin any
  @validatorKeys = ['min', 'max']
  @validateSync: (val) ->
    x = parseInt(val)
    if not isNaN(x)
      x
    else
      false

Type.register integer

class float extends Number
  @baseMixin any
  @validatorKeys = ['min', 'max']
  @validateSync: (val) ->
    x = parseFloat(val)
    if not isNaN(x)
      x
    else
      false

Type.register float

class object extends Object
  @baseMixin any
  @validate: (val, cb) ->
    if @validateSync val
      @validateFields val, cb
    else
      cb new Exception error: 'not_object', value: val
  @validateSync: (val) ->
    val instanceof Object
  @validateFields: (val, cb) ->
    helper = (field, next) ->
      field.validate val[field.name], next
    async.forEach @fields or [], helper, (err, res) ->
      if err
        cb err
      else
        cb null, val
  @process: (newClass, {fields} = newClass) ->
    newFields =
      for spec in fields
        new Field newClass, spec
    newClass.fields = @fields.concat(newFields)
  @make: (val, cb) ->
    @_makeFields val, cb
  @_makeFields: (val, cb) ->
    result = _.extend {}, val
    logger.debug 'type.make', @, @ instanceof Function
    helper = (field, next) =>
      field.make result[field.name], (err, res) ->
        if err
          next err
        else
          result[field.name] = res
          next null
    async.forEach @fields, helper, (err, res) =>
      if err
        cb err
      else
        @_makeObject result, cb
  @_makeObject: (result, cb) ->
    obj = new @ result
    cb null, obj
  @describe: () ->
    output = {}
    if @name
      output.name = @name
    if @__base__ != any
      output.base = @__base__.name
    output.fields = @fields.describe()
    output
  @fields = new FieldList()
  # this is where things are a bit confusing...
  # 
  @join: (objectType) ->
    newType = class joined extends object
      constructor: (@lhs, @rhs) -> # we are not validating here because this is not a public method. i.e. this ought to be called within Relation.join, rather than directly.
      @itemJoin: (lhs, rhs) ->
        new @ lhs, rhs
      isJoined: true
      get: (key) -> # this didn't get copied... hmm... did it get overwritten?
        [object, name] = key.split '.'
        name = if not name then object else name
        if object == name
          object = 0 # the first one
        else
          object = parseInt(object)
        if object == 0
          @lhsGet name
        else
          @rhsGet object, name
      lhsGet: (key) ->
        if @lhs.hasOwnProperty(key)
          @lhs[key]
        else
          undefined
      rhsGet: (obj, key) ->
        if @rhs.isJoined
          @rhs.get("#{obj - 1}.#{key}")
        else if @rhs.hasOwnProperty(key)
          @rhs[key]
        else
          undefined

      # by default it's just an object that has basic operations
    # i.e. it'll lose many of its values?
    logger.debug 'Type.join', newType, newType.itemJoin
    newType.fields = @fields.join objectType.fields
    newType
  order: (fields) ->
    # return a new relation that's ordered by the column...
    # this is going to become even more complex...
    # we'll deal with this later...
    # for now - the next step is to ensure that this is integrated with the select expression.
  constructor: (val) ->
    _.extend @, val
  toString: () ->
    Object.toString @
  get: (key) ->
    if @hasOwnProperty(key)
      @[key]
    else
      undefined
  set: (key, val, cb) ->
    # if we want to set the value here - we ought to automatically check against the validator...
    # that makes sense!
    if @hasOwnProperty(key)
      @fields[key].validate val, (err, res) =>
        if err
          cb err
        else
          @[key] = val
          cb null, val
    else # throw error.
      cb new Exception error: 'invalid_key', key: key, value: val
  valueOf: () ->
    @

Type.register object

Array2 = Class(Array, {}); 

class list extends Array2
  @baseMixin any
  @validatorKeys = ['minLength', 'maxLength']
  @type: Type.declare 'any'
  @validate: (val, cb) ->
    if val instanceof Array
      @validator.validate val, (err, res) =>
        if err
          cb err
        else
          @itemsValidate val, cb
    else
      cb new Exception 'not_an_array', value: val
  @itemsValidate: (val, cb) ->
    helper = (item, next) =>
      @type.validate item, next
    async.forEach val, helper, (err, res) ->
      if err
        cb err
      else
        cb null, val
  @make: (val, cb) -> # making of the object ...
    @validate val, (err, res) ->
      if err
        cb err
      else
        cb err, val
  @process: (newClass, spec = newClass) ->
    logger.debug 'list_process', newClass.__base__
    newClass.type = Type.declare spec.type
    newClass.validator = @makeValidator spec
  @describe: () ->
    output = {}
    if @__base__ != any
      output.base = @__base__.name
    output.type = @type.describe()
    if @name
      output.name = @name
    output.type = @type.describe()
    _.extend output, @validator.describe()

Type.register list # you can just declare list like this...

RegExp::toJSON = () ->
  @toString()

ssn = Type.register
  name: 'ssn'
  base: 'string'
  regex: /^\d\d\d-?\d\d-?\d\d\d\d$/

logger.debug 'ssn.describe() =====================================================>'
logger.debug ssn.describe()

ssn1 = Type.register class ssn1 extends string
  @base: 'string'
  @regex: /^\d\d\d-?\d\d-?\d\d\d\d$/

logger.debug 'ssn1.describe() =====================================================>'
logger.debug ssn1.describe()

listOfInt = Type.declare
  base: 'list'
  type: 'integer'
  minLength: 2
  maxLength: 100

logger.debug 'listOfInt.describe() ==================================================>'
logger.debug listOfInt.describe()


uuid = Type.register
  name: 'uuid'
  base: 'string'
  regex: /^[a-fA-F0-9]{8}-?[a-fA-F0-9]{4}-?4[a-fA-F0-9]{3}-?(:?8|9|A|B)[a-fA-F0-9]{3}-?[a-fA-F0-9]{12}$/



module.exports = Type


