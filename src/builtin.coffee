# we should assume that the built-in will receive regular Exp objects
# rather than assuming that this will be stripped out appropriately.
# let's handle the stripping of the values here...
# (i.e. literal will self eval).

Environment = require './environment'
Registry = require './registry'
Exception = require '../shared/exception'
fs = require '../shared/fs'
logger = require '../shared/log'
uuid = require '../shared/uuid'
crypto = require '../shared/crypto'

# we'll need to deal with the procedure arity & its requirement as well...
# time to revisit how that's done with with FunctionObj
# procedure arity is the biggest deal
# we'll need to consider whether to allow for varargs (print will use it coming handily).
# 
#
# how do I deal with procedure arity?
# it seems that I should allow for vararg as well... I don't see any reason why not for now
# it just needs to be always defined as a *array*...

# let's do it simple first.
# BuiltIn ought to have simple procedure arity rules.
# simplest
# fixed
# varargs
# fixed + varargs
# let's support these only for now...
#

callWithArgs = (proc) ->
  (vm, args) ->
    proc.apply vm, args

callWithArgsAsync = (proc) ->
  (vm, args, cb) ->
    proc.apply vm, args.concat(cb)

# we'll need to deal with arguments. This is for tuple call.
# ['a', 'b'] -> first param is a, and second param is b
# ['a', 'b', '...'] -> first param is a, and second param is b, which is a vararg.

class BuiltInArgument
  constructor: (@args) ->
    @validateArgs()
  validateArgs: () ->
    unique = {}
    hasVarArg = false
    for arg, i in @args
      if unique.hasOwnProperty(arg)
        throw new Exception error: 'builtin_duplicate_argument_names', value: @args
      else if arg == '...'
        if i < @args.length - 1
          throw new Exception error: 'builtin_vararg_before_end', value: @args
        else
          hasVarArg = true
      else 
        unique[arg] = 1
    if hasVarArg
      @args.pop() # remove the last
      @vararg = @args.pop() # remove the vararg
  tupleToArgList: (tuple) ->
    argList = []
    for arg in @args
      argList.push tuple[arg]
    if @vararg
      if not tuple[@vararg] instanceof Array
        throw new Exception error: 'builtin_vararg_not_passed_as_array', name: @vararg, value: tuple
      else
        argList = argList.concat(tuple[@vararg])
    argList

class BuiltIn
  @_reg = new Registry noDupe: true
  @has: (key) ->
    @_reg.has key
  @register: (key, val) ->
    @_reg.register key, val
  @makeSync: (name, args, proc) ->
    new @(name, new BuiltInArgument(args), callWithArgs(proc), true)
  @make: (name, args, proc) ->
    new @(name, new BuiltInArgument(args), callWithArgsAsync(proc))
  @define: (name, args, proc) ->
    @register name, @make(name, args, proc)
  @defineSync: (name, args, proc) ->
    @register name, @makeSync(name, args, proc, true)
  constructor: (@name, @args, @proc, @sync = false) ->
  runSync: (vm, args) ->
    if @sync
      @proc vm, args
    else
      throw new Exception error: 'not_sync_function', value: @
  run: (vm, args, cb) ->
    if @sync
      try
        cb null, @runSync vm, args
      catch e
        cb e
    else
      @proc vm, args, cb
  tupleRunSync: (vm, tuple) ->
    @runSync vm, @args.tupleToArgList(tuple)
  tupleRun: (vm, tuple, cb) ->
    @run vm, @args.tupleToArgList(tuple), cb
  eval: (env, cb) ->
    cb null, @

BuiltIn.defineSync '+', ['args', '...'], (args...) ->
  val = args[0]
  for i in [1...args.length]
    val = val + args[i]
  val

BuiltIn.defineSync '-', ['args', '...'], (args...) ->
  val = args[0]
  for i in [1...args.length]
    val = val - args[i]
  val

BuiltIn.defineSync '*', ['args', '...'], (args...) ->
  val = args[0]
  for i in [1...args.length]
    val = val * args[i]
  val

BuiltIn.defineSync '/', ['args', '...'], (args...) ->
  val = args[0]
  for i in [1...args.length]
    val = val / args[i]
  val

BuiltIn.defineSync '%', ['args', '...'], (args...) ->
  val = args[0]
  for i in [1...args.length]
    val = val % args[i]
  val

BuiltIn.defineSync '&&', ['args', '...'], (args...) ->
  for arg in args
    if arg
      continue
    else
      return false
  if args.length > 1 then args[args.length - 1] else false

BuiltIn.defineSync '<', ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs < rhs
      continue
    else
      return false
  true

BuiltIn.defineSync '<=', ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs <= rhs
      continue
    else
      return false
  true

BuiltIn.defineSync '>',  ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs > rhs
      continue
    else
      return false
  true

BuiltIn.defineSync '>=', ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs >= rhs
      continue
    else
      return false
  true

# does this require unwrap args? uncertain... this is likely to change again.
BuiltIn.defineSync '==', ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs == rhs
      continue
    else
      return false
  true

# we'll need to create a list of the args for this as well...
# is this *easier*?
# hmm... need to think through at this level...
# for example, what is the args... map to for tuple??? I don't really know...
# this is one problem with this particular design...
# or does it mean that we no longer allow for variable args?
# that it might mean that...
# the question is... what is the args transform function for built-in?
# and it seems to be adding additional work that doesn't really do anything here.
# so if that's the case, another way is to do it within the call munging for args
# hmm...
# so - we might still have functions within to wrap things up appropriately...
# hmm....
BuiltIn.defineSync '!=', ['args', '...'], (args...) ->
  for i in [1...args.length]
    lhs = args[i - 1]
    rhs = args[i]
    if lhs != rhs
      continue
    else
      return false
  true

BuiltIn.defineSync '||', ['args', '...'], (args...) ->
  for arg in args
    if arg
      return arg
    else
      continue
  false

BuiltIn.defineSync '!', ['args', '...'], (arg) ->
  not arg

formatOne = (arg) ->
  arg

BuiltIn.defineSync 'formatOne', ['arg'], formatOne

BuiltIn.defineSync 'format', ['args', '...'], (args...) ->
  (formatOne(arg) for arg in args)

BuiltIn.defineSync 'print', ['args', '...'], (args...) ->
  logger.debug.apply @, (formatOne(arg) for arg in args)
  args

# let's deal with read first...
# let's also deal with the separation of the stream objects from the reading operations.
# let's assume we have a stream object...
# let's use nodeJS's stream object?
# if we want this to be

BuiltIn.define 'readFile', ['filePath'], (filePath, cb) ->
  fs.readFile filePath, 'utf8', (err, data) ->
    if err
      cb err
    else
      cb null, data.toString()

BuiltIn.define 'writeFile', ['filePath', 'data'], (filePath, data, cb) ->
  fs.writeFile filePath, data, 'utf8', cb

BuiltIn.define 'raiseAsync', ['error'], (error, cb) ->
  cb error

# if I want to have a direct read interface that's only differ by the stream object that
# they represent... we'll have to define the stream object...
# hmm...
# at this moment it isn't clear what that looks like... we'll have to take it a bit slowly.
# read(http://blah)
# write(http://blah) # this might not fly...

# 
# REGEXP
# match/replace - need to figure out if this should be built-in like perl...
# don't like magic variables such as $1, $2, etc.
# 
BuiltIn.defineSync 'match', ['regex', 'data'], (regex, data) ->
  regex.exec(data)

BuiltIn.defineSync 'replace', ['data', 'regex', 'replacer'], (data, regex, replacer) ->
  data.replace regex, replacer

BuiltIn.defineSync 'makeUUID', [], () ->
  uuid.v4()

BuiltIn.defineSync 'isUUID', ['v'], (v) ->
  uuid.isUUID(v)

BuiltIn.defineSync 'makeSalt', [], () ->
  crypto.makeSalt()

BuiltIn.define 'makeHmac', ['key', 'salt', 'passwd'], (key, salt, passwd, cb) ->
  crypto.makeHmac key, salt, passwd, cb

BuiltIn.define 'verifyHmac', ['key', 'salt', 'passwd', 'hmac'], (key, salt, passwd, hmac, cb) ->
  crypto.verifyHmac key, salt, passwd, hmac, (err, res) ->
    cb null, (if err then false else true)

BuiltIn.defineSync 'isHexString', ['str'], (str) ->
  if str.match(/^([0-9a-fA-F][0-9a-fA-F])+$/) then true else false

module.exports = BuiltIn

