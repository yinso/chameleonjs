Relation = require './relation'

# this just passed through everything to the underlying Relation
class AliasRelation extends Relation
  @compile: (vm, {inner, alias}, env) ->
    
  @make: (vm, [inner, alias], env) ->
    rel = new AliasRelation alias, inner
    rel
  constructor: (@_name, @inner) -> # this creates a new name...
    @type = @inner.type # sharing the type.
    @isa = @inner.isa
    @ctor = @inner.ctor
  mapKey: (table, column) ->
    res = @inner.mapKey table, column
    console.log 'AliasRelation.mapKey', table, column, res
    res
  fields: () ->
    @inner.fields()
  fieldNames: () ->
    @inner.fieldNames()
  data: () ->
    @inner.data()
  insert: (vm, recs, cb) ->
    @inner.insert vm, recs, cb
  project: (vm, fields, cb) ->
    @inner.project vm, fields, cb
  filter: (vm, whereCode, cb) ->
    @inner.filter vm, whereCode, cb
  select: (vm, cb) ->
    @inner.select vm, cb
  insertView: (vm, view, cb) ->
    @inner.insertView vm, view, cb
  makeN: (vm, recs, cb) ->
    @inner.makeN vm, recs, cb

module.exports = AliasRelation
