mongodb = require 'mongodb'
Connection = require './connection'
Exception = require '../shared/exception'
logger = require '../shared/log'

#
# mongodb query designs
#
# we'll need to map it to our own set of query object...
# this will be fully on the server-side so we don't have to worry about it... 

# important to deal with the tracking of 

# we can potentially have schema attached to every connection... but generally it only needs the outer layer to handle it...
# part of this will require us to convert the name of the table by a pattern.
class MongoDbConnection extends Connection
  # question is - do I want that schema inside MongoDb connection? Might be cool...
  constructor: ({server, port, database}) ->
    super()
    server = server or '127.0.0.1'
    port = port or 27017
    database = database or 'test'
    @server = new mongodb.Server server, port
    @conn = new mongodb.Db database, @server
  connect: (cb) ->
    @conn.open (err, db) =>
      @db = db
      cb err
  disconnect: (cb) ->
    try
      @db.close()
      cb null
    catch e
      cb e
  query: (query, cb) ->
    logger.debug 'MongoDbConnection.query', query, query.createIndex
    super query, cb
  createIndexQuery: ({createIndex, table, columns, unique}, cb) ->
    columnHash = {}
    for column in columns
      columnHash[column] = 1
    @db.ensureIndex @tableName(table), columnHash, {unique: unique, safe: true, name: createIndex}, cb
  dropIndexQuery: ({dropIndex, table}, cb) ->
    @db.dropIndex @tableName(table), dropIndex, cb
  insertQuery: ({insert, values}, cb) ->
    @db.collection(@tableName(insert)).insert @transformClause(values), {safe: true}, cb
  selectQuery: ({select, from, where}, cb) ->
    try
      where = @transformWhereClause(where)
      logger.debug 'selectQuery', where
      @db.collection(@tableName(from)).find(where).toArray (err, res) =>
        if err
          cb err
        else
          result = 
            for rec in res
              # we'll need to transform it via the column name...
              @transformResultColumns rec
          cb null, result
    catch e
      cb e
  transformResultColumns: (rec) ->
    output = {}
    for key, val of rec
      if rec.hasOwnProperty(key)
        output[@fromColumnName(key)] = val
    output
  _supportedFuncs:
    '==': 1
    '&&': 1
    '||': 1
    '!': 1
    '>=': '$gte'
    '<=': '$lte'
    '>': '$gt'
    '<': '$lt'
    '!=': '$ne'
    'in': '$in'
    'not_in': '$nin' # is this a separate operator? what about NULL?
  resolveFunc: (func) ->
    if func instanceof Object and func.id
      func.id
    else if typeof(func) == 'string'
      func
    else
      throw new Exception error: 'unknown_func_name', value: func
  supportedFunc: (name) ->
    @_supportedFuncs[@resolveFunc(name)]
  supportUpdateExpColumn: () -> false
  supportUpdateExpFunc: (func) -> false
  unNestedOperators:
    '$all': 1
    '$gt': 1
    '$gte': 1
    '$in': 1
    '$lt': 1
    '$lte': 1
    '$ne': 1
    '$nin': 1
    '$exists': 1
    '$mod': 1
    '$type': 1
    '$regex': 1
    '$options': 1
    '$where': 1
    '$size': 1
    '$isolated': 1
  transformSetClause: (exps) ->
    result = {}
    for {setField, exp} in exps
      result[setField] = @transformWhereClause exp
    logger.debug 'MongoDbConnection.transformSetClause', result
    result
  transformWhereClause: (exp) ->
    logger.debug 'MongoDbConnection.transformWhereClause', exp
    if not exp
      exp
    else if exp.literal
      exp.literal
    else if exp.id # this is the column name... so we'll need to think what it does.
      @toColumnName(exp.id)
    else if exp.funcall # if we are here it means the function is supported...
      # this one might be an ID... let's see 
      funcall = @resolveFunc(exp.funcall)
      args =
        for arg in exp.args
          @transformWhereClause arg
      # what does an equal clause look like?
      if funcall == '==' # the basics...
        # apply from left to right.
        @equalClause args
      else if funcall == '&&' # we'll combine the items within the args.
        @andClause args
      else if funcall == '||'
        @orClause args
      else if @supportedFunc(funcall) != 1
        @compClause args, funcall
      else # don't know what funcall it is...
        throw new Exception error: 'unsupported_funcall', value: exp
    else
      throw new Exception error: 'unsupported_where_clause', value: exp
  andClause: (args, op) ->
    $and: args
  orClause: (args) ->
    $or: args # the easiest
  equalClause: (args) -> # will not work correctly if these are multiple equals together.
    res = {}
    for i in [1...args.length]
      res[args[i - 1]] = args[i]
    res
  compClause: (args, op) ->
    res = {}
    for i in [1...args.length]
      obj = {}
      obj[@supportedFunc(op)] = args[i]
      res[args[i - 1]] = obj
    res
  transformClause: (clause) ->
    logger.debug 'transformClause', clause
    if not clause instanceof Object
      return clause
    if clause instanceof Array # this is a set of data here...
      return clause
    # http://docs.mongodb.org/manual/reference/operator/
    # all mongo operator starts with $ so those are not...
    # items 
    res = {}
    for key, val of clause
      if clause.hasOwnProperty(key)
        if @unNestedOperators.hasOwnProperty(key)
          logger.debug 'MongoDB.transformClause: unnested', key
          res[key] = val
        else if /^\$/.exec(key) # non-unnested ops, which means we'll need to further transform
          logger.debug 'MongoDB.transformClause: furtherTransform', key
          res[key] = @transformClause val
        else # this is a column - val doesn't need to be further transformed.
          logger.debug 'MongoDB.transformClause: columnName', key
          res[@toColumnName(key)] = val
    res
  updateQuery: ({update, set, where}, cb) ->
    whereExp =
      if where
        @transformWhereClause(where) # update requires
      else
        {$where: "1==1"} # mongodb requires a where clause -> this one always returns true 
    setExp = {'$set': @transformSetClause(set)}
    @db.collection(@tableName(update)).update whereExp, setExp, {multi: true}, cb
  deleteQuery: (query, cb) ->
    deleteTbl = query.delete
    {where} = query or {}
    @db.collection(@tableName(deleteTbl)).remove @transformWhereClause(where), safe: true, cb
  tableName: (name) ->
    res = name.toLowerCase()
    logger.debug 'MongoDB.tableName', name, '=>', res
    res
  fromColumnName: (name) ->
    col = 
      if /^_id$/.exec(name)
        'id'
      else
        name
    col
  toColumnName: (name) ->
    col = 
      if /^id$/.exec(name)
        '_id'
      else
        name
    logger.debug 'MongoDB.toColumnName', col
    col
  

module.exports = MongoDbConnection
