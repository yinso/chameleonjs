{Field, FieldList} = require './field'
Table = require './table'
assert = require 'assert'
_ = require 'underscore'
Exception = require '../shared/exception'
Schema = require './schema'

# what are we extending the table for?
# is this truly a table-based definition?
_.extend Table.BaseTable,
  addField: (spec, option = {}) ->
    field = new Field @, spec
    @fields.add field, option
  dropField: (field, options = {restrict: true}) ->
    # can schema check to see if a particular field is referenced...
    # dropping field requires the following...
    # 1 - is there any constraints bound against this field?
    # any indexes bound against this field will be dropped...
    # so - what's a quick way to determine whether or not a particuilar column
    # has itself exists...
    # does this mean that we'll need to keep the table into the field? Possibly...
    # that way we know for sure it belongs to a particular table...
    field = @fields.get field
    refs = Schema.current.fieldReferences field
    if refs.length == 0 # we can go ahead and drop it...
      @fields.drop field
    else if options.restrict
      throw new Exception error: 'field_referenced', value: refs
    else if options.cascade # we'll have to drop the references... and they won't be recreated.
      for cons in refs
        Schema.current.dropConstraint cons
  # what about alter field?
  # basically - the following are altered...
  # 1 - 

module.exports = Table
