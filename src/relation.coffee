#
# a relation is a soup'd up array... basically
#

Type = require './type2' # we might be able to inherit from Table... but do we need to?
async = require '../shared/async'
_ = require 'underscore'
logger = require '../shared/log'
Exception = require '../shared/exception'
OPCodes = require './opcodes'
Tuple = require './tuple'
BuiltIn = require './builtin'
Connection = require './connection'
Cursor = require './cursor'

# a relation is basically an array with a type value... i.e. creating a relation
#
# even though we have defined the tuple as a javascript object... they aren't really the same
# thing... are they??
#
# a join'd tuple isn't the same as a regular flat tuple... that's something to keep in mind.
#
# to fix the above we'll need to fix the output of the repl object.
class BaseRelation extends Type
  constructor: (@type) ->
    # every base relation has its own types...
    # a relation is also an environment (read-only type of environment)
    # it ought to be passed in with 
  name: () ->
  data: () ->
  get: (key, rel) ->
    if @type.props.hasOwnProperty(key)
      @type.props[key]
    else
      undefined
  mapKey: (table, key) ->
    if not @type.props.hasOwnProperty(key)
      throw new Exception error: 'invalid_select_field', value: key
    else
      key
  normalizeSelectFields: (fields, env) ->
    if fields == '*' # there is nothing to normalize... this is a special case?
      fields
    else if fields instanceof Array
      for f in fields
        @normalizeSelectField f, env
    else
      throw new Exception error: 'invalid_select_fields', value: fields
  normalizeSelectField: (field, env) ->
    column: field.column
    as: field.as
  normalizeID: (exp, env) ->
    prop = @get exp.id
    scope = env.get exp.id
    console.log 'Relation.normalizeID', exp, prop, scope
    if prop
      if not scope
        {relID: "#{@name()}.#{exp.id}"}
      else
        throw new Exception error: 'ambiguous_identifier_duplicate_with_lexical_scope', value: exp
    else
      exp
  normalizeRef: (exp, env) -> # can we deal with
    # exp.ref is made up of a ref & a key.
    # let's check to see where we are...
    if exp.ref.id # this is base...
      # in this case - ID can be the table name (or the alias itself)
      # we might need a different set of the function...
      if exp.ref.id == @name() and exp.key.literal # this is a literal key
        id = @normalizeID(id: exp.key.literal, env)
        if id.relID
          id
        else # no...
          exp
      else
        exp
    else
      ref: @normalizeRef(exp, env)
      key: exp.key 
  normalizeExp: (exp, env) -> # take a particular relation to make the appropriate values...
    console.log 'Relation.normalizeExp', exp
    if not exp # this is something that should not have occurred...
      exp
    else if exp.literal
      exp
    else if exp.id # SPECIFIC VALUES TO BE RESOLVED
      @normalizeID exp, env
    else if exp.ref # SPECIFIC VALUES TO BE RESOLVED
      @normalizeRef exp, env
    else if exp.funcall
      funcall: @normalizeID(exp.funcall, env)
      args: (@normalizeExp(arg, env) for arg in exp.args)
    else if exp.if
      if: @normalizeExp(exp.if, env)
      then: @normalizeExp(exp.then, env)
      else: @normalizeExp(exp.else, env)
    else if exp.begin
      begin: (@normalizeExp(e) for e in exp.begin)
    else if exp.block
      block: (@normalizeExp(e) for e in exp.block)
    else if exp.select # this is a select query.
      select: @normalizeSelectFields exp.select, env
      from: @normalizeExp exp.from, env
      where: @nomralizeExp exp.where, env
    else #
      throw new Exception error: 'unsupported_query_exp', value: exp
    #else if exp.join # this is a join clause
    #else if exp.insert # this is an insert query
    #else if exp.update # this is an update query
    #else if exp.delete # this is a delete query.
  
# we'll soon need to deal with one more type of object - connection...
# these are going to be built-into the system...
# every relation will by default be attached to a connection

# 
# query (a series of manipulation)
# relation (a basic object that can potentially be manipulated)
# connection (an object that can potentially drastically alter how the relation is *manipulated*)
# besides the memory relation, the rest ought to be uniform
# the question is... can they both have the same interface?
# let's try to enable mongodb
# 
class MemoryRelation extends BaseRelation
  @compile: (vm, {relation, props}, env) ->
    [pushProps, makeTuple] = Tuple.compile vm, {tuple: relation, props: props}, env
    [pushProps, [OPCodes.makeRelation, relation]]
  @make: (vm, {name, props}, env) ->
    type = Tuple.make vm, {name: name, props: props}, env
    relation = new MemoryRelation name, type
    relation.isa = BuiltIn.make "#{name}_isa", ['tuple'], (tuple, cb) ->
      @pushCallStack [[OPCodes.push, tuple], [OPCodes.push, type.isa], [OPCodes.call, 2]]
      @runAsync cb
    relation.ctor = BuiltIn.make "#{name}_ctor", ['tuple'], (tuple, cb) ->
      @pushCallStack [[OPCodes.push, tuple], [OPCodes.push, type.ctor], [OPCodes.call, 2]]
      @runAsync cb
    relation
  fields: () ->
    @type.props
  fieldNames: () ->
    prop.name for prop in @type.props
  fieldsCompileEnv: () ->
    env = {}
    for prop in @type.props
      env[prop.name] = prop
    env
  data: () ->
    @inner
  constructor: (@_name, @type, @inner = []) ->
    @connection = Connection._reg[':memory'] # by default we are using :memory as the connection type.
  name: () ->
    @_name
  project: (vm, fields, cb) ->
    # I think this turns out not to be a *projection* in the sense that it's a field...
    # what's happening is that the 
  filter: (vm, whereCode, cb) -> # the code is already compiled...
    rel = @
    helper = (rec) ->
      try
        cursor = new Cursor rel, rec
        code = [[OPCodes.pushCursor, cursor]].concat(whereCode).concat([[OPCodes.popCursor]])
        vm.pushCallStack code
        res = vm.runSync()
        if res then rec else false
      catch e
        logger.error 'Relation.filter_error', e.message
        false
    try
      filtered = _.filter @data(), helper
      logger.debug "Relation.filter", whereCode, filtered
      cb null, new MemoryRelation @name, @type, filtered
    catch e
      cb e
  select: (vm, cb) -> # whereExp... this will cause 
    cb null, @
  insertView: (vm, view, cb) -> # a view is already fully retrieved...
    @insert vm, view.inner, cb
  insert: (vm, recs, cb) ->
    @makeN vm, recs, (err, recs) =>
      if err
        cb err
      else
        @inner = @inner.concat recs
        cb null, @
  makeN: (vm, recs, cb) ->
    helper = (rec, next) =>
      @make vm, rec, next
    async.map recs, helper, (err, res) ->
      if err
        cb err
      else
        cb null, res # this is going to be a list of the values that are ctor'd
  make: (vm, rec, cb) ->
    vm.pushCallStack [[OPCodes.push, rec], [OPCodes.push, @type.ctor], [OPCodes.call, 2]]
    vm.runAsync cb

# 
# perhaps we would want to provide cursor as a way to iterate through relation, rather than
# directly exposing the underlying objects...
# 
# the nice thing about having a cursor is that we can use something that'll be more prone to
# 
# we'll
# 


module.exports = MemoryRelation


