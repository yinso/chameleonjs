fs = require '../shared/fs'
path = require 'path'
logger = require '../shared/log'

# this is close to writing against the system... we'll have to then filter for things
# that changes the actual data in the system and store those accordingly...
class ExpressionEntry
  constructor: (@repl, @filePath = @defaultPath()) ->
    try
      # we'll first try to load from the path.
      @repl.rli.history = fs.readFileSync(@filePath, 'utf-8').split('\n').reverse()
      @repl.rli.history.shift()
      @repl.rli.historyIndex = 0
    catch e
      # do nothing here...
      @repl.rli.history
    process.on 'exit', @flushLog
  flushLog: () =>
    console.log "save session..."
    fs.writeFileSync @filePath, @logToData(), 'utf-8'
  defaultPath: () ->
    path.join process.cwd(), ".chameleon_history.log" # can't change this for now...
  logToData: () ->
    @repl.rli.history.reverse().join('\n')

module.exports = ExpressionEntry