
{EventEmitter} = require 'events'
_ = require 'underscore'
Exception = require '../shared/exception'
async = require '../shared/async'
Registry = require './registry' # we might not need registry anymore...
assert = require 'assert'
Environment = require './environment'
BuiltIn = require './builtin'
Type = require './type2'
RefHolder = require './refholder'
BaseProperty = require './baseproperty'
Property = require './property'
UserFunction = require './function'
TupleType = require './tuple'
OPCodes = require './opcodes'
Parser = require './chamelang'
logger = require '../shared/log'
fs = require '../shared/fs'
Relation = require './relation'
SelectQuery = require './selectquery'
InsertQuery = require './insertquery'
Connection = require './connection'
Cursor = require './cursor'
JoinClause = require './joinclause'
AliasRelation = require './aliasrelation'

class CallStackFrame
  constructor: (@context, code, @env, @stack = []) ->
    if code instanceof UserFunction
      @func = code
      @code = @func.body
    else
      @code = code
    # return is the return address of the previous callstack frame...
    @ip = 0
    @cursor = null
  incIP: (jump = 1) ->
    @ip = @ip + jump
    if @ip > @code.length - 1
      @ip = -1
  hasIP: () ->
    #logger.log 'VM.hasIP', @ip
    @ip > -1
  currentInstruction: () ->
    @code[@ip]
  push: (obj) ->
    @stack.push obj
  pop: () ->
    if @stack.length > 0
      @stack.pop()
    else
      throw new Exception error: 'data_stack_underflow'
  popN: (n, reverse = true) ->
    args = []
    for i in [0...n]
      if @stack.length > 0
        if reverse
          args.unshift @stack.pop()
        else
          args.push @stack.pop()
      else
        throw new Exception error: 'stack_underflow'
    args
  pushEnv: (newEnv = {}) ->
    @env = new Environment newEnv, @env
  updateEnv: (updatedEnv = {}) ->
    @env.inner = _.extend @env.inner, updatedEnv
  popEnv: () ->
    if not @env.prev
      throw new Exception error: 'environment_underflow'
    @env = @env.prev
  dupe: () ->
    if @stack.length > 0
      @stack.push @top()
  top: () ->
    if @stack.length > 0
      @stack[@stack.length - 1]
    else
      throw new Exception error: 'data_stack_underflow'

# we are going to deal with the runtime of the evaluation...
class VM
  constructor: (@env = new BaseEnvironment(), @maxDepth = 500) ->
    @callStack = []
    @error = undefined
  clone: (env = @env) ->
    new VM env, @maxDepth
  parse: (text) ->
    logger.debug 'VM.parse', text
    Parser.parse text
  eval: (text, cb) ->
    try
      logger.debug '======================================================================'
      logger.debug text
      logger.debug '======================================================================'
      @evalExp @parse(text), cb
    catch e
      cb e
  evalSync: (text) ->
    @evalExpSync @parse(text)
  evalExp: (exp, cb) ->
    logger.debug 'VM.eval', exp
    try
      newEnv = new TopLevelEnvironment({}, @env) 
      code = @compile exp, newEnv # a bit expensive..... is there another way?
      #code = @compile exp, @env
      logger.debug 'VM.compile', code
      @pushCallStack code
      @runAsync cb
    catch e
      cb e
  callStackTop: () ->
    if @callStack.length > 0
      @callStack[@callStack.length - 1]
    else
      throw new Exception error: 'call_stack_underflow'
  pushCallStack: (proc) ->
    frame = new CallStackFrame(@, proc, @top?.env or @env) # why do we only start from the base?
    @callStack.push frame
    @top = frame
    logger.debug 'VM.pushCallStack', @callStack.length
    frame
  popCallStack: () ->
    if @callStack.length > 0
      @callStack.pop()
      logger.debug 'VM.popCallStack', @callStack.length
      @top = @callStackTop()
      @top
    else
      throw new Exception error: 'code_stack_underflow'
  resetCallStack: () ->
    @callStack = []
  pushArgsEnv: (proc, args) ->
    newEnv = proc.argsToFrame args # this line is dynamic, i.e. not staticly compiled code?
    logger.debug 'proc.pushEnv', proc.name, args, newEnv
    @top.pushEnv newEnv
  isExpression: (exp) ->
    if exp.def
      false
    else
      true
  handleError: () ->
    logger.debug 'VM.error', @error, @top
    while @error != null
      try
        @goto '__catch'
        @top.push @error
        @error = undefined
        return
      catch e # no error block -> popStack -> we'll need a single stack at the bottom...
        if @callStack.length > 1
          @popCallStack() # unwinds the callstack...
          continue # this return from the function?
        else
          logger.debug 'no_error_handler_use_global', e
          error = @error
          @error = undefined
          throw error # new Exception error: 'global_error_handling', value: error
  runTupleCall: (op, arg) ->
    logger.debug 'runTupleCall.got_here_0', @callStack
    proc = @top.pop()
    args = @top.pop()
    if proc.trace # we should deal with tab level in the future
      @traceIn proc, args
    if args not instanceof Object
      @throwError new Exception error: 'tupleCall_args_must_be_object', value: args
    else if proc instanceof BuiltIn # assume it's the sync version here.
      try
        res = proc.tupleRunSync @, args
        if proc.trace
          @traceOut proc, res
        @top.push res
        @top.incIP()
      catch e
        @throwError e
    else # we are good to go here...
      if op == OPCodes.tupleTailCall
        @popCallStack()
      else
        @top.incIP()
      @pushCallStack proc
      @top.pushEnv args
      logger.debug 'runTupleCall.got_here_4'
  runOneInstruction: (op, opArg) ->
    logger.debug 'VM.runOneInstruction', op, opArg, @top.stack
    if op == OPCodes.call or op == OPCodes.tailCall
      proc = @top.pop()
      args = @top.popN(opArg - 1)
      if proc.trace # we should deal with tab level in the future
        @traceIn proc, args
      if proc instanceof BuiltIn
        if proc.sync
          try
            res = proc.runSync @, args
            if proc.trace
              @traceOut proc, res
            @top.push res
            @top.incIP()
          catch e # this error here ought to be recoverable...
            @throwError e
        else
          @throwError new Exception error: 'builtin_async_not_executable_in_runOneOp', value:  proc
      else if proc instanceof UserFunction
        logger.debug 'func.call', proc.body
        if op == OPCodes.tailCall
          @popCallStack() # do we need anything from it?
        else
          @top.incIP()
        @pushCallStack proc
        @pushArgsEnv proc, args
      else
        @throwError new Exception error: 'not_a_function', value: proc
    else if op == OPCodes.tupleCall or op == OPCodes.tupleTailCall
      @runTupleCall op, opArg
    else if op == OPCodes.push
      @top.push opArg
      @top.incIP()
    else if op == OPCodes.pop
      @top.pop()
      @top.incIP()
    else if op == OPCodes.dupe
      @top.dupe()
      @top.incIP()
    else if op == OPCodes.pushEnv
      env = @top.pop()
      if not env instanceof Object
        @throwError new Exception error: 'invalid_environment', value: env
      @top.pushEnv env
      @top.incIP()
    else if op == OPCodes.newEnv
      @top.pushEnv {}
      @top.incIP()
    else if op == OPCodes.updateEnv
      env = @top.pop()
      if not env instanceof Object
        @throwError new Exception error: 'invalid_environment', value: env
      @top.updateEnv env
      @top.incIP()
    else if op == OPCodes.popEnv
      @top.popEnv()
      @top.incIP()
    else if op == OPCodes.refEnv
      try
        val = @top.env.getLexical opArg
        @top.push (if val instanceof RefHolder then @top.env.get val.id else val)
        @top.incIP()
      catch e
        @throwError e
    else if op == OPCodes.updateRef
      val = @top.top()
      @top.env.set opArg, val
      @top.incIP()
    else if op == OPCodes.return
      result = @top.pop()
      if @top.func?.trace
        @traceOut @top.func, result
      @top.popEnv()
      @popCallStack()
      @top.push result
    else if op == OPCodes.ifNotJump
      cond = @top.pop()
      if not cond
        @top.incIP opArg
      else
        @top.incIP()
    else if op == OPCodes.jump
      @top.incIP opArg
    else if op == OPCodes.throw
      @throwError()
    else if op == OPCodes.goto
      # the method to go to will be to search through the current code block...
      try
        @goto opArg
      catch e
        @throwError e
    else if op == OPCodes.label # we'll either process elsewhere or here...
      @top.incIP()
    else if op == OPCodes.defEnv
      @top.env.set opArg, @top.pop()
      @top.incIP()
    else if op == OPCodes.member
      ref = @top.pop()
      key = @top.pop()
      if not ref instanceof Object
        @throwError new Exception error: 'not_an_object', value: ref
      else
        @top.push ref[key]
        @top.incIP()
    else if op == OPCodes.cursorRef
      cursor = @top.cursor
      if not cursor # this means we'll need to push cursor somewhere...
        @throwError new Exception error: 'no_cursor_present', value: opArg
      else # we can access the value.
        @top.push cursor.get opArg
        @top.incIP()
    else if op == OPCodes.pushCursor
      if not opArg instanceof Cursor
        @throwError new Exception error: 'not_a_cursor', value: opArg
      else 
        @top.cursor = opArg
        @top.incIP()
    else if op == OPCodes.popCursor
      @top.cursor = null
      @top.incIP()
    else if op == OPCodes.makeJoinClause
      args = @top.popN(opArg)
      @top.push JoinClause.make args
      @top.incip()
    else if op == OPCodes.makeAlias
      rel = @top.pop()
      if not (rel instanceof Relation)
        @throwError new Exception error: 'alias_requires_relation', value: rel
      else
        @top.push new AliasRelation opArg, rel
        @top.incIP()
    else if op == OPCodes.makeArray
      array = @top.popN(opArg)
      @top.push array
      @top.incIP()
    else if op == OPCodes.makeObject
      object = @makeObject(@top.popN(opArg))
      @top.push object
      @top.incIP()
    else if op == OPCodes.makeObjectViaKeys
      # not a good name... but what this does is that we specify how many keys there are
      # and the stack is arranged so keys are all aligned first,
      # and then values are aligned next
      # so [makeObjectViaKeys, 2] means first 2 popped are keys, and the next two popped are values.
      keys = @top.popN(opArg)
      values = @top.popN(opArg)
      @top.push @makeObjectViaKeys(keys, values)
      @top.incIP()
    else if op == OPCodes.makeArg
      @top.push new Property @top.pop(), @, @top.env
      @top.incIP()
    else if op == OPCodes.makeTuple
      props = @top.pop()
      @top.push TupleType.make @, {name: opArg, props: props}, @top.env
      @top.incIP()
    else if op == OPCodes.makeRelation
      props = @top.pop()
      @top.push Relation.make @, {name: opArg, props: props}, @top.env
      @top.incIP()
    else if op == OPCodes.makeInsertQuery
      rel = @top.pop()
      values = @top.pop()
      logger.debug 'makeInsertQuery', rel instanceof Relation, rel instanceof InsertQuery
      if not (rel instanceof Relation)
        throw new Exception error: 'not_a_relation_for_insert', value: rel
      if not ((values instanceof Relation) or (values instanceof Array))
        throw new Exception error: 'not_a_value_for_insert', value: values
      @top.push InsertQuery.make @, {insert: rel, values: values}, @top.env
      @top.incIP()
    else if op == OPCodes.makeSelectQuery
      props = @top.pop()
      @top.push SelectQuery.make @, props, @top.env
      @top.incIP()
    else if op == OPCodes.makeJoinClause
      [join, lhs, rhs, exp]  = @top.popN(4)
      console.log 'makeJoinClause', exp, rhs, lhs, join
      @top.push SelectQuery.JoinClause.make @, {lhs: lhs, rhs: rhs, join: join, exp: exp}, @top.env
      @top.incIP()
    #else if op == OPCodes.pushCursor
      # we'll have to 
    else if op == OPCodes.validateRequired
      val = @top.top()
      if (val == null or val == undefined)
        @throwError new Exception error: 'value_required', value: opArg
      else
        @top.incIP()
    else if op == OPCodes.makeFunction
      args = @top.popN opArg
      logger.debug 'makeFunction', opArg, args
      @top.push UserFunction.make(args, @top.env)
      @top.incIP()
    else if op == OPCodes.resolveClosure
      func = @top.env.get(opArg)
      if not func instanceof UserFunction
        @throwError new Exception error: 'invalid_function_object', value: func
      else
        func.resolveClosure(@top.env)
        @top.incIP()
    else if op == OPCodes.trace
      func = @top.env.get opArg
      if func instanceof UserFunction or  func instanceof BuiltIn
        func.trace = true
        @top.incIP()
      else
        @throwError new Exception error: 'trace_not_a_function', value: func
    else if op == OPCodes.untrace
      func = @top.env.get opArg
      if func instanceof UserFunction or  func instanceof BuiltIn
        func.trace = false
        @top.incIP()
      else
        @throwError new Exception error: 'trace_not_a_function', value: func
    else if op == OPCodes.show
      res = 
        if opArg == 'env'
          @top.env
        else if opArg == 'types'
          Type._reg.inner
        else if opArg == 'stack'
          @top
      @top.push res
      @top.incIP()
    else if op == OPCodes.describe
      item = @top.pop()
      @top.push @describe item
      @top.incIP()
    else
      @throwError new Exception error: 'invalid_opcode', value: op
    logger.debug 'VM.runOneInstructionResult', op, opArg, ' ==> ', @top.stack
  format: (obj) ->
    if typeof(obj) == 'number'
      "#{obj}"
    else if typeof(obj) == 'string'
      obj
    # these are likely to be delegated toward the object itself later down the road.
    else if obj instanceof UserFunction
      "#<userFunction:#{obj.name}>"
    else if obj instanceof BuiltIn
      "#<nativeFunction:#{obj.name}>"
    else if obj instanceof Relation
      "#<Relation:#{obj.name()} => #{@format(obj.inner)}>"
    else if obj instanceof Type
      "#<Type:#{obj.name}>"
    else
      logger.format(obj)
  describe: (item) ->
    item
  handleAsyncCall: (op, opArg, cb) ->
    proc = @top.pop()
    args =
      if op == OPCodes.call or op == OPCodes.tailCall
        @top.popN(opArg - 1)
      else
        @top.pop() # for tupleCall & tupleTailCall
    if proc.trace # we should deal with tab level in the future
      @traceIn proc, args
    @top.incIP() # the inclusion of this line *blanked out* callStack? wierd!!!
    resultHandler = (err, res) =>
      if err
        @throwError err, false # false = noIncIP
      else
        if proc.trace
          @traceOut proc, res
        @top.push res
      @runAsync cb
    if op == OPCodes.call or op == OPCodes.tailCall
      proc.run @, args, resultHandler
    else
      proc.tupleRun @, args, resultHandler
  evalExpSync: (exp, env = @env.clone()) ->
    code = @compile exp, env
    logger.debug 'VM.evalSync', exp, code
    @pushCallStack code
    @runSync()
  runSync: () ->
    try
      while (@top.hasIP() or @error) 
        if @error # we need to handle the error object.
          @handleError()
          continue
        [op, opArg] = @top.currentInstruction()
        @runOneInstruction op, opArg
      @returnResultSync true
    catch e
      @resetCallStack()
      throw e
  runAsync: (cb) ->
    @runAsyncChooseReset true, cb
  runAsyncChooseReset: (toReset, cb) ->
    logger.debug 'VM.runAsync', @top.code
    sync = true
    try
      while (@top.hasIP() or @error) and sync
        if @error # we need to handle the error object.
          @handleError()
          continue
        [op, opArg] = @top.currentInstruction()
        logger.debug 'VM.asyncCode', op, opArg
        if op == OPCodes.call or op == OPCodes.tailCall
          proc = @top.top()
          if proc instanceof BuiltIn and not proc.sync # we'll have to handle this as async.
            sync = false
            @handleAsyncCall op, opArg, cb
          else
            @runOneInstruction op, opArg
        else if op == OPCodes.tupleCall or op == OPCodes.tupleTailCall
          proc = @top.top()
          if proc instanceof BuiltIn and not proc.sync # we'll have to handle this as async.
            sync = false
            @handleAsyncCall op, opArg, cb
          else
            @runOneInstruction op, opArg
        else if op == OPCodes.insert # this is an async statement
          query = @top.pop()
          sync = false
          if not (query instanceof InsertQuery)
            @top.incIP()
            cb new Exception error: 'not_a_insert_query', value: query
          else
            resultHandler = (err, res) =>
              if err
                @throwError err, false # false = noIncIP
              else
                @top.push res
                @runAsync cb
            @top.incIP()
            query.exec @, resultHandler 
        else if op == OPCodes.select
          query = @top.pop()
          sync = false
          logger.error 'OPCodes.select', query instanceof SelectQuery, query instanceof Relation
          if not (query instanceof SelectQuery)
            @top.incIP()
            cb new Exception error: 'not_a_select_query', value: query
          else
            resultHandler = (err, res) =>
              if err
                @throwError err, false # false = noIncIP
              else
                @top.push res
                @runAsync cb
            @top.incIP()
            query.exec @, resultHandler # values are already created...
        else
          @runOneInstruction op, opArg
      if sync
        logger.debug 'VM.code', 'last', @top.stack
        @returnResult toReset, cb
    catch e
      logger.error "VM.asyncCode", e.message
      @resetCallStack()
      cb e
  throwError: (error, toIncIP = true) ->
    if not error
      if @top.top()
        error = @top.pop()
      else
        error = new Exception error: 'stack_underflow'
    @error = error
    if toIncIP
      @top.incIP()
  traceIn: (proc, args) ->
    # depending on the stack level we'll deal with the trace level.
    logger.debug 'traceIn', proc.name, args
  traceOut: (proc, val) ->
    logger.debug 'traceOut', proc.name, '=>', val
  goto: (labelName) ->
    curIP = @top.ip
    for i in [0...@top.code.length]
      exp = @top.code[i]
      if exp[0] == OPCodes.label and exp[1] == labelName
        # this is where we'll go to...
        @top.ip = i
        logger.debug 'goto', labelName, exp, curIP, @top.ip
        return
    throw new Exception error: 'invalid_label', value: labelName
  makeObject: (ary) ->
    obj = {}
    for i in [0...ary.length] by 2
      obj[ary[i]] = ary[i + 1]
    obj
  makeObjectViaKeys: (keys, values) ->
    obj = {}
    for i in [0...keys.length]
      obj[keys[i]] = values[i]
    obj
  returnResultSync: (chooseReset) ->
    val = @top.stack.pop()
    logger.debug 'VM.return', val
    if chooseReset
      @resetCallStack()
    val
  returnResult: (chooseReset, cb) ->
    try
      val = @top.stack.pop()
      logger.debug 'VM.return', val
      if chooseReset
        @resetCallStack()
      cb null, val
    catch e
      cb e
  makeThenElse: (thenExp, elseExp) ->
    result = []
    result.push [OPCodes.ifNotJump, thenExp.length + 2]
    result = result.concat thenExp
    result.push [OPCodes.jump, elseExp.length + 1]
    result.concat elseExp
  makeBlock: (exps, env, newScope, last) ->
    logger.debug 'VM.makeBlock', exps, newScope, env
    compiled = []
    newEnv = if newScope then new Environment({}, env) else env
    if newScope
      compiled.push [OPCodes.newEnv]
    for i in [0...exps.length]
      e = exps[i]
      if i < exps.length - 1
        compiled = compiled.concat @compile(e, newEnv)
        if @isExpression(e)
          compiled.push [OPCodes.pop]
      else
        if not @isExpression(e)
          throw new Exception error: 'block_cannot_end_on_non_expression', values: e
        else
          compiled = compiled.concat @compile(e, newEnv, last)
    if newScope
      compiled.push [OPCodes.popEnv]
    compiled
  makeDefinitionArgOne: (def, env) ->
    if typeof(def) == 'string'
      Property.make @, {name: def}, env
    else
      Property.make @, def, env
  makeDefinitionArgs: (args, env) ->
    if args instanceof Array
      for arg in args
        @makeDefinitionArgOne arg, env
    else
      [ @makeDefinitionArgOne args, env ]
  makeDefinition: (exp, env, last) ->
    try
      args = @makeDefinitionArgs exp.def, env
      for arg in args
        env.set arg.name, arg.name # for compilation...
      body = @compile exp.exp, env, last
      for i in [(args.length - 1)..0]
        arg = args[i]
        if arg instanceof Property # validate the result that has been pushed to stack.
          body.push [OPCodes.push, arg.ctor], [OPCodes.call, 2]
        body.push [OPCodes.defEnv, arg.name]
        if exp.exp.function
          body.push [OPCodes.resolveClosure, arg.name]
      body
    catch e
      if exp.def instanceof Array
        for id in exp.def
          env.del id
      else
        env.del exp.def
      throw e
  makeInsertQuery: (exp, env) ->
    # we'll first have to
    try
      relation = @evalExpSync exp.insert
      if not (relation instanceof Relation)
        throw new Exception error: 'not_a_relation', name: insert, value: relation
      opcodes = []
      if exp.values
        # compile each values to their particular values.
        valuesExp = [].concat.apply [], (@compile(value, env) for value in exp.values)
        opcodes = valuesExp.concat [[OPCodes.push, relation], [OPCodes.insert, exp.values.length]]
      else if exp.select
        selectExp = @compile(exp.select)
        opcodes = selectExp.concat [[OPCodes.push, relation], [OPCodes.insertView]]
      else
        throw new Exception error: 'invalid_insert_exp', value: exp
      opcodes
    catch e
      logger.error 'VM.makeInsertQuery_error', e.message
      throw e
  compile: (exp, env, last = false) -> # each compilation ought to return a list of opcodes
    logger.debug 'VM.compile', exp, env.inner
    if exp.hasOwnProperty('literal') # this is 
      [[OPCodes.push, exp.literal]]
    else if exp.id
      value = env.get(exp.id)
      #logger.debug 'exp.id', exp.id, value, env, env.isFreeVariable(exp.id)
      if value == undefined
        throw new Exception error: 'unknown_identifier', value: exp.id
      else
        # what happens when we have id that's in a different position?
        code = 
          if env.isFreeVariable(exp.id)
            if env.isTopLevelVariable(exp.id)
              [[OPCodes.push, env.get(exp.id)]]
            else
              [[OPCodes.refClosure, exp.id]]
          else
            [[OPCodes.refEnv, exp.id]]
        if exp.alias
          if not value instanceof Relation
            throw new Exception error: 'invalid_alias_for_non_relation', value: exp
          else
            code.push [OPCodes.makeAlias, exp.alias]
        code
    else if exp.relID # query apparently requires a different type of structure...
      # when we get here we have previously verified that it DOES exist...
      # so we won't do anything with it...
      [[OPCodes.cursorRef, exp.relID]]
    else if exp.def # this is now a composite statement...
      @makeDefinition exp, env, last
    else if exp.funcall
      opcodes = [].concat.apply [], (@compile(e, env) for e in exp.args)
      funcallExp = @compile exp.funcall, env
      opcodes.concat funcallExp, [ [ (if last then OPCodes.tailCall else OPCodes.call), exp.args.length + 1 ] ]
    else if exp.array
      argsExp = (@compile(e, env) for e in exp.array)
      opcodes = [].concat.apply [], argsExp
      opcodes.concat [[OPCodes.makeArray, exp.array.length]]
    else if exp.object
      logger.debug 'makeObject', exp.object
      opcodes =
        [].concat.apply [], (for [k, v] in exp.object
          logger.debug 'makeObjectKV', k, v
          [ [ OPCodes.push, k ] ].concat @compile(v, env))
      opcodes.concat [[OPCodes.makeObject, exp.object.length * 2]]
    else if exp.ref # member statement.
      keyExp = @compile exp.key, env
      refExp = @compile exp.ref, env
      keyExp.concat refExp, [[ OPCodes.member ]] # it's always 2.
    else if exp.if # if statement.
      condExp = @compile exp.if, env
      thenExp = @compile exp.then, env, last
      elseExp = @compile exp.else, env, last
      condExp.concat @makeThenElse thenExp, elseExp
    else if exp.block
      @makeBlock exp.block, env, true, last
    else if exp.begin # block_expression
      @makeBlock exp.begin, env, not env.isTopLevel(), last
    else if exp.name # makeArg/Column... this part is... hmm...
      Property.compile @, exp, env
    else if exp.tuple # makeTuple
      TupleType.compile @, exp, env
    else if exp.relation # makeRelation
      Relation.compile @, exp, env
    else if exp.function # makeFunction
      UserFunction.compile @, exp, env
      # we'll have to reference the function when generating the approriate signature...
      # unless the signature is dynamic... is that what I want to accomplish?
      # need to think about that a bit more...
      # client will look like a call 3 -> then the return address ought to be
      # marked
    else if exp.join # this is a join clause.
      SelectQuery.JoinClause.compile @, exp, env
    else if exp.throw
      bodyExp = @compile exp.throw, env
      bodyExp.concat [[OPCodes.throw]]
    else if exp.try # try/catch/finally - this can be a difficult
      tryExp = @compile exp.try, env # try cannot be tail called.
      newEnv = {}
      newEnv[exp.catch] = 1
      catchHandleExp = @compile exp.handle, new Environment(newEnv, env)
      catchHandleExp = [[OPCodes.push, exp.catch], [OPCodes.makeObjectViaKeys, 1], [OPCodes.pushEnv]].concat catchHandleExp, [[OPCodes.popEnv]]
      finallyExp =
        if exp.finally
          @compile exp.finally, env, last # finally can be tail called.
        else
          []
      # the order is this...
      # run through try -> if error, it'll go to catchHandleExp.
      # then run through finally
      # run through catch Exp -> which will run through finally at the end.
      # finally needs to have a return statement at the end...?? no
      [].concat tryExp, [[OPCodes.goto, '__finally']], [[OPCodes.label, '__catch']], catchHandleExp, [[OPCodes.label, '__finally']], finallyExp
    else if exp.trace # we'll allow for the trace of the functionobject itself...
      [[OPCodes.trace, exp.trace]]
    else if exp.untrace
      [[OPCodes.untrace, exp.untrace]]
    else if exp.regex
      # ought this be something that's compiled here? or should it just be compiled
      # directly during the 
      [[OPCodes.push, new RegExp(exp.regex)]]
    else if exp.insert # this is the insert query...!!!
      logger.debug "VM.compile.insert", exp
      # insert query ought to first to do is to refer 
      # we'll get the values of the insertion...
      InsertQuery.compile @, exp, env
    else if exp.select # this is the select query...
      logger.error 'VM.compile.select', exp
      SelectQuery.compile @, exp, env
    else if exp.values
      # we'll push everything onto the stack.
      [].concat.apply([], (@compile(v, env) for v in exp.values))
    else if exp.show
      [[OPCodes.show, exp.show]]
    else if exp.describe
      [[OPCodes.refEnv, exp.describe], [OPCodes.describe]]
    else
      throw new Exception error: 'invalid_or_unsupported_exp', value: exp



BuiltIn.defineSync 'typeof', ['arg'], (arg) ->
  if arg == null
    null
  else if arg == undefined
    null
  else if arg instanceof BuiltIn
    'function'
  else if arg instanceof TupleType
    'type'
  else if arg instanceof Object
    'tuple'
  else if arg instanceof Array
    'array'
  else
    typeof(arg)

BuiltIn.define 'eval', ['arg'], (arg, cb) ->
  vm = @clone()
  vm.eval arg, cb

BuiltIn.define 'call', ['func', 'tuple'], (func, tuple, cb) ->
  oldVM = @
  vm = oldVM.clone()
  code = [[OPCodes.push, tuple], [OPCodes.push, func], [OPCodes.tupleCall]] # toplevel cannot use tailcall...
  vm.pushCallStack code
  vm.runAsync cb

BuiltIn.define 'load', ['path'], (path, cb) ->
  vm = @
  fs.readFile path, (err, data) ->
    helper = (exp, next) ->
      vm.evalExp exp, (err, res) ->
        if err
          next err
        else
          next null, res
    if err
      cb err
    else
      text = data.toString()
      try
        exp = vm.parse text
        if exp.begin
          async.map exp.begin, helper, (err, res) ->
            if err
              cb err
            else
              cb null, res[res.length - 1]
        else
          vm.evalExp exp, cb
      catch e
        cb e

class TopLevelEnvironment extends Environment
  constructor: (@inner = {}, @prev = null) ->
    #super @inner, @prev
  isTopLevel: () ->
   true
  isFreeVariable: (key) ->
    true
  isTopLevelVariable: (key) ->
    true
  isClosureVariable: (key) ->
    false # always top level here...
  isLexicalVariable: (key) ->
    false

# this thing will look quite like the basic schema object!
# I think that's exactly the base environment...
# but as currently defined it cannot be integrated into our code seamlessly... we'll
# have to create a new version
# first - all procedures that gets defined at this level 
class BaseEnvironment extends TopLevelEnvironment
  constructor: (@inner = {}) ->
    @functions = {} # new Registry noDupe: true
    @types = Type._reg # types ought to not be redefined @ top level.
    @builtIn = BuiltIn._reg # same as builtin.
    @connections = Connection._reg
  get: (key) ->
    if @functions.hasOwnProperty key
      @functions[key]
    else if @types.has key
      @types.has key
    else if @builtIn.has key
      @builtIn.has key
    else if @connections.hasOwnProperty(key)
      @connections[key]
    else if @inner.hasOwnProperty(key)
      @inner[key]
    else
      undefined
  getLexical: (key) ->
    @get key
  def: (key, val) ->
  set: (key, val) ->
    logger.debug 'BaseEnvironment.set', key, val
    if val instanceof UserFunction
      @functions[key] = val
    else if val instanceof BuiltIn
      @builtIn.register key, valTupleType
    else if val instanceof Type
      @types.register key, val
    else if val instanceof Connection
      @connections[key] = val
    else
      @inner[key] = val
  del: (key, val) ->
    if @functions.hasOwnProperty key
      delete @functions[key]
    else if @builtIn.has key
      @builtIn.delete key
    else if @types.has key
      @types.delete key
    else
      delete @inner[key]
  clone: () ->
    @



module.exports = VM

