Connection = require './connection'

# this is the schema-based connection object...
class SchemaConnection extends Connection
  constructor: (@schema, @inner) -> # this is a schema based external connection...
  connect: (cb) ->
    @inner.connect cb
  disconnect: (cb) ->
    @inner.disconnect cb
  createIndexQuery: (query, cb) ->
    @inner.query query, cb
  # we can perform adding, dropping, and altering schema @ this level?
  # we'll have to do that against the schema object as well...
  # also - mapping against expressions would be really cool for the projection values.
  # but that probably will be a much later work...
  insertQuery: (query, cb) ->
    {insert, values} = query
    # insert is a table. values is a record.
    if not @schema.models.has(insert)
      cb {error: 'unknown_table', value: insert}
    else # we have the table.
      @schema.models.has(insert).makeAsync values, (err, rec) =>
        if err
          cb err
        else
          @inner.insertQuery insert: insert, values: rec, cb
  selectQuery: (query, cb) ->
    {select, from, where} = query
    if not @schema.models.has(from)
        cb {error: 'unknown_table', value: from}
    else # we have the table... the question is... what do we do here?
      @inner.selectQuery query, cb
  updateQuery: (query, cb) ->
    {update, set, where} = query
    if not @schema.models.has(update)
      cb {error: 'unknown_table', value: update}
    else
      @inner.updateQuery query, cb
  deleteQuery: (query, cb) ->
    {where} = query
    deleteTbl = query.delete
    if not @schema.models.has(deleteTbl)
      cb error: 'unknown_table', value: deleteTbl
    else
      @inner.deleteQuery query, cb
  execQuery: (query, cb) ->
    {exec, args} = query
    # exec is a <model>.<proc> notaton - make sure that it fits so.
    # or it might be possible to be 
    [model, proc] = exec.split '.'
    if not @schema.models.has(model) 
      cb error: 'unknown_table', value: model
    else if not @schema.models.has(model).hasOwnProperty(proc)
      cb error: 'unknown_procedure', value: exec
    else
      @schema.models.has(model)[proc] args, cb

module.exports = SchemaConnection
