Environment = require './environment'
BuiltIn = require './builtin'

# also pull this guy out...
class BaseProperty
  constructor: (arg, vm, env) ->
    {@name} = arg
  @make: (vm, exp, env, newEnv = null) ->
    makeEnv = new Environment newEnv, env
    prop = new @ exp, vm, makeEnv
    prop.isa = @makeIsa prop, vm, makeEnv
    prop.ctor = @makeCtor prop, vm, makeEnv
    prop
  @makeIsa: (prop, vm, env) ->
    BuiltIn.make "#{prop.name}_isa", [prop.name], (arg, cb) ->
      if (arg == null or arg == undefined)
        cb new Exception error: 'value_required', prop: prop, value: arg
      else
        cb null, true
  @makeCtor: (prop, vm, env) ->
    BuiltIn.make "#{prop.name}_ctor", [prop.name], (arg, cb) ->
      if (arg == null or arg == undefined)
        cb new Exception error: 'value_required', prop: prop, value: arg
      else
        cb null, arg

module.exports = BaseProperty
