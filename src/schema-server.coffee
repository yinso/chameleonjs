# this is the version for extending the server-side...

Exception = require '../shared/exception'
assert = require 'assert'
Schema = require './schema'
fs = require '../shared/fs'
path = require 'path'
fs = require '../shared/fs'
_ = require 'underscore'
Constraint = require './constraint'
Table = require './table-server' # what the heck happens to the reference of constraints...
logger = require '../shared/log'

_.extend Schema.prototype, 
  loadPlugin: (filePath) ->
    logger.debug 'Schema.loadPlugin', filePath, 
    module = require path.resolve(filePath)
    name = module.name or path.basename(filePath, path.extname(path))
    type = path.basename(path.dirname(filePath))
    if type == 'type'
      @registerType module
    else if type == 'table'
      @registerTable module
  loadPlugins: (folderPath) -> # noticeably the . is the same as process.cwd()
    files = fs.readdirSync folderPath
    for file in files
      if path.extname(file) == '.coffee'
        @loadPlugin path.join(folderPath, file)
  loadAllPlugins: (root = process.cwd()) ->
    @loadPlugins path.join(root, 'type')
    @loadPlugins path.join(root, 'table')
  saveSchema: (root = process.cwd()) ->
    fs.writeFileSync path.join(root, 'schema.json'), JSON.stringify(@describe(), undefined, 2)
  loadSchema: (root = process.cwd()) ->
    # schema is basically a single file that contains all of the schema descriptions.
    data = JSON.parse fs.readFileSync(path.join(root, 'schema.json'))
    logger.debug 'Schema.loadSchema', data
    # for now ignore the types... 
    {tables, constraints} = data
    # first thing is to clear out the constraints as we'll reload them.
    @constraints.clear()
    for table in tables
      @loadTable table
    logger.debug 'Schema.loadSchema_constraints'
    for cons in constraints
      @registerConstraint cons
    logger.debug 'Schema.loadSchema_constraints_done'
  loadTable: (spec) ->
    table = @hasTable spec.name
    logger.debug 'Schema.loadSchema_table', spec
    if table # we just want to clear out the fields and reload the fields...
      # now we are to deal with the fields...
      logger.debug 'Schema.loaTable_hasTable', spec
      table.fields.clear()
      Table.processTable table, spec
    else
      logger.debug 'Schema.loaTable_noTable', spec
      # we can register the table definition.
      @registerTable spec
    logger.debug 'Schema.loadSchema_table_done', spec.name
  dropConstraint: (cons) ->
    # we'll need to verify whether or not this item already exists... or is it a spec...
    # and we'll have to create them appropriately.
    if cons instanceof Constraint
      # we can try to see if it can be deleted... this is part of the registry...
      @constraints.delete cons
    else if cons instanceof Object
      @constraints.delete cons.name
    else if typeof(cons) == 'string'
      @constraints.delete cons
    else
      throw new Exception error: 'unsupported_constraint_type', value: cons

module.exports = Schema
