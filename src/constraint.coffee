assert = require 'assert'
_ = require 'underscore'
Exception = require '../shared/exception'
async = require '../shared/async'
Registry = require './registry'
Schema = require './schema'
Default = require './default' # will there be a circular binding potential? don't know yet
Type = require './type'
{Field, FieldList} = require './field'
obj = require './object'
logger = require '../shared/log'

class Constraint
  @registerSchema: (schema) ->
    assert.ok schema instanceof Schema, "invalid_schema: #{schema}"
    @schema = schema
    @schema.setClass
      class: 'constraints'
      validate: @validateConstraint
  @validateConstraint: (cons) =>
    if cons instanceof Constraint
      cons
    else
      @create cons
  @register: (spec) ->
    @schema.registerConstraint spec
  @has: (name) ->
    @schema.hasConstraint name
  @create: (cons) -> # how do I know what type of constraint it is?
    if cons.type == 'index' # this is an index constraint.
      new Index cons
    else
      throw new Exception error: 'unsupported_constraint', value: cons
  

# all constraint will share the same name!!!
Constraint.registerSchema Schema.current

# we also want to let constra

class Index extends Constraint
  constructor: (spec) ->
    @type = 'index'
    {name, table, columns, unique, foreign, primary} = spec
    @table =
      if table instanceof Object
        table
      else
        Schema.current.hasTable table
    if not @table
      throw new Exception error: 'unknown_table', name: table
    else
      @columns = @verifyFields @table, columns
    @unique = unique or false
    @assertSinglePrimaryKey primary
    # deal with foreign key references. we'll need access to the schema object.
    if foreign
      @foreign = @constructor.schema.hasTable foreign.table
      if not @foreign
        throw new Exception error: 'unknown_foreign_table', name: foreign.table
      else
        @refs = @verifyForeignKey @foreign, foreign.columns
    @name = @indexName(name)
  fieldsName: () ->
    (field.name for field in @columns).join('')
  indexPrefix: () ->
    if @primary
      "pk"
    else if @unique
      "uk"
    else if @foreign
      "fk"
    else
      "idx"
  indexName: (name) ->
    if name
      name
    else
      "#{@indexPrefix()}_#{@table.name}_#{@fieldsName()}"
  verifyFields: (table, columns) ->
    output = 
      for column in columns
        logger.debug "table.fields", column, table.fields
        # this part is interesting... the fields are not something that's 
        if not table.fields.hasOwnProperty(column)
          throw new Exception error: 'unknown_column', name: "#{@table.name}.#{column}"
        table.fields[column]
    new FieldList().concat output
  assertSinglePrimaryKey: (primary = false) ->
    if primary
      for key, index of @table.constraints
        if index.primary # already has a primary key -> fail
          throw new Exception error: 'multiple_primary_key_declared', table: @table.name
    @primary = primary
  verifyForeignKey: (foreign, refs) ->
    columns = @verifyFields foreign, refs
    for [col, ref] in _.zip(@columns, columns)
      logger.debug 'Index.verifyForeignKeyMatch', col, ref
      if not col.equiv ref
        throw new Exception error: 'foreign_key_column_must_be_equivalent', column: col, reference: ref
    columns
  references: (field) ->
    helper = (fields) ->
      for f in fields
        if f == field
          return true
      false
    if field.object != @table
      false
    else if helper @columns
      true
    else if field.object != @foreign
      false
    else if helper @refs
      true
    else
      false 
  describe: () ->
    output =
      type: 'index'
      name: @name
      table: @table.name
      columns: @columns.names()
    if @primary
      output.primary = true
    if @unique
      output.unique = true
    if @foreign
      output.foreign =
        table: @foreign.name
        columns: @refs.names()
    output
  diff: (index) ->
    obj.diff @describe(), index.describe()

module.exports = Constraint
