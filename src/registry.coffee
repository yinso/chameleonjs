Exception = require '../shared/exception'
logger = require '../shared/log'

# a registry can have the following replaced.
# 1 -
# 
class Registry
  constructor: (@options = {}) ->
    logger.debug 'Registry.ctor', @options
    @name = @options.name
    @inner = {}
    if @options.validate instanceof Function
      @validate = @options.validate
  has: (key) ->
    if @inner.hasOwnProperty(key)
      @inner[key]
    else
      undefined
  register: (key, val) ->
    if val == undefined
      val = key
      key = val.name
    #logger.debug "#{@name}Registry.register", key, val, arguments.length
    if @options.noDupe and @has key
      throw new Exception error: "#{@name}_duplicate_definition", name: key
    @inner[key] = @validate val
    @inner[key]
  validate: (val) ->
    if @type
      new @type val
    else
      val
  delete: (key) -> # it's either a key, or an object...
    if typeof(key) == 'string'
      val = @has(key)
      delete @inner[key]
    else
      val = @has key.name
      delete @inner[key.name]
    val
  clear: () ->
    for key, val of @inner
      delete @inner[key]
  keys: () ->
    Object.keys @inner
  values: () ->
    output = []
    for key, val of @inner
      output.push val
    output

module.exports = Registry