Exception = require '../shared/exception'
BaseProperty = require './baseproperty'
Environment = require './environment'
OPCodes = require './opcodes'
Type = require './type2' # this line might need to be pulled out... hmm... push it to env?
logger = require '../shared/log'
BuiltIn = require './builtin'

# how do I pull this guy out?
class Property extends Type
  # this makes opcodes for making an Property
  @compile: (vm, exp, env, newEnv = null) ->
    # try to do as much as possible within compiled stage... 
    [[OPCodes.push, @make(vm, exp, env, newEnv)]] # push directly an Property
  @make: (vm, exp, env, newEnv = null) ->
    arg = name: exp.name
    if exp.type or exp.default or exp.optional or exp.where
      if exp.type
        arg.type = vm.evalExpSync exp.type #Type.declare exp.type
        # this obviously right now doesn't work approriately... 
        logger.debug 'Property.make.type', arg.type
        if not (arg.type instanceof Type) # first type seeing it requiring the parenthesis
          throw new Exception error: 'invalid_type', value: arg.type
      arg.optional = if exp.default then true else exp.optional or false
      if exp.default
        arg.default = exp.default
      if exp.where
        arg.where = exp.where
      if not newEnv
        newEnv = {}
        newEnv[exp.name] = arg
      logger.debug 'Property.make', arg, newEnv
      prop = new Property arg, vm, env, newEnv
      prop.isa = @makeIsa prop, vm, env, newEnv
      prop.ctor = @makeCtor prop, vm, env, newEnv
      prop
    else
      BaseProperty.make vm, arg, env, newEnv
  constructor: (arg, vm, env, newEnv = false) ->
    {@name, @type, @optional, @default, @where} = arg
    @optional = if @default then true else @optional or false
  @makeIsa: (prop, vm, env, newEnv) ->
    # we should also have the where clause compiled...
    # and what do we do with the whereclause? 
    compileEnv = new Environment newEnv, env
    logger.debug "Property.makeIsa", prop, newEnv
    whereExp = 
      if prop.where
        whereExp = vm.compile prop.where, compileEnv
      else
        undefined
    BuiltIn.make "#{prop.name}_isa", [prop.name], (arg, cb) ->
      logger.debug "#{prop.name}_isa", arg, whereExp
      if (arg == null or arg == undefined)
        if prop.optional
          cb null, true
        else # 
          cb new Exception error: 'value_required', prop: prop, value: arg
      else # we have an value - so we can validate the result.
        newVM = vm.clone(env)
        code = [[OPCodes.push, arg], [OPCodes.push, prop.type.isa], [OPCodes.call, 2]]
        newVM.pushCallStack code
        newVM.runAsync (err, res) ->
          if err
            cb err
          else if not res # i.e. the data has failed.
            cb null, res
          else if whereExp instanceof Array
            tuple = {}
            tuple[prop.name] = arg
            newVM.pushCallStack [[OPCodes.push, tuple], [OPCodes.pushEnv]].concat(whereExp)
            newVM.runAsync (err, res) ->
              if err
                cb err
              else
                cb null, if res then true else false
          else 
            cb null, res
  @makeCtor: (prop, vm, env, newEnv) ->
    compileEnv = new Environment newEnv, env
    logger.debug "Property.makeCtor", prop, newEnv
    defaultExp =
      if prop.default
        vm.compile prop.default, compileEnv
      else
        undefined
    BuiltIn.make "#{prop.name}_ctor", [prop.name], (arg, cb) ->
      newVM = vm.clone(env)
      if (arg == null or arg == undefined)
        if defaultExp # has a default value.
          tuple = {}
          tuple[prop.name] = arg
          newVM.pushCallStack [[OPCodes.push, tuple], [OPCodes.pushEnv]].concat(defaultExp)
          newVM.runAsync (err, res) ->
            if err
              cb err
            else 
              newVM.pushCallStack [[OPCodes.push, res], [OPCodes.push, prop.isa], [OPCodes.call, 2]]
              newVM.runAsync (err, res2) ->
                if err
                  cb err
                else if res2
                  cb null, res
                else
                  cb new Exception error: 'fail_validation', value: res
        else if prop.optional
          cb null, null
        else # error.
          cb new Exception error: 'value_required', prop: prop, value: arg
      else
        newVM.pushCallStack [[OPCodes.push, arg], [OPCodes.push, prop.isa], [OPCodes.call, 2]]
        newVM.runAsync (err, res2) ->
          if err
            cb err
          else if res2
            cb null, arg
          else
            cb new Exception error: 'fail_validation', value: arg

module.exports = Property
