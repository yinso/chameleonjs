Exception = require '../shared/exception'

class Cursor
  constructor: (@rel, @rec) ->
    if not @rec
      throw new Exception error: 'cursor_rec_is_empty', value: @
  mapKey: (key) ->
    [table, column] = key.split /\./ # we might not want to do this every time...
    # pass to REL
    @rel.mapKey table, column # every rel needs
  get: (key) ->
    # based on the key we'll map to the actual result.
    realKey = @mapKey key
    res = @rec[realKey]
    console.log 'Cursor.get', key, res
    res
  set: (key, val) ->
    # based 
    realKey = @mapKey key
    @rec[realKey] = val

module.exports = Cursor
