
Exception = require '../shared/exception'
_ = require 'underscore'
Default = require './default'
async = require '../shared/async'
Class = require '../shared/class'
assert = require 'assert'
obj = require './object'

# what to do with Field?
class Field
  constructor: (object, @options) ->
    Object.defineProperty @, 'object',
      value: object
      writable: false
      enumerable: false
      configurable: false    
    {@name, type, optional} = @options
    defaultSpec = @options.default
    @type = @constructor.Type.declare type # how to get Type over 
    if not @type
      throw new Exception error: 'invalid_type_definition', type: type
    @required = not optional
    if defaultSpec
      @required = false
      @default = new Default defaultSpec
  validate: (val, cb) ->
    if not @required
      cb null, val
    else if val == null or val == undefined
      cb new Exception error: 'property_required', name: @name
    else
      @type.validate val, cb
  noValue: (v) ->
    v == null or v == undefined # 0, [], '' are all valid values
  make: (val, cb) ->
    noValue = @noValue(val)
    if noValue and @default
      try
        cb null, @default.run()
      catch e
        cb e
    else if @required and noValue
      cb new Exception(error: "field_required", name: @name)
    else if not @required and noValue
      cb null, val
    else
      @type.make val, cb
  describe: () ->
    output = 
      name: @name
      type: @type.nameOrDescribe()
    if not @required
      output.optional = true
    if @default
      output.default = @default.describe()
    output
  diff: (col) ->
    obj.diff @describe(), col.describe()
    

# do I want to add this as a powered collection?
# also keep in mind that 

Array2 = Class(Array, {}); 

class FieldList extends Array2
  constructor: (fields...) ->
    super()
    @push fields...
  concat: (arrays...) ->
    output = new FieldList()
    for item in @
      output.push item
    for ary in arrays
      for item in ary
        output.push item
    output
  push: (fields...) ->
    for field in fields
      @_setField field
    super fields...
  shift: () -> # remove the first item.  
    field = super()
    delete @[field.name]
    field
  pop: () ->
    field = super()
    delete @[field.name]
    field
  unshift: (fields...) ->
    for field in fields
      @_setField field
    super fields...
  splice: (index, removed, fields...) ->
    for field in fields
      @_setField field
    # we should also deal with the removed...
    for i in [index...index + removed]
      field = @[i]
      delete @[field.name]
    super index, removed, fields...
  _setField: (field) ->
    assert.ok field instanceof Field
    if @hasOwnProperty(field.name)
      throw new Exception error: 'duplicate_field_name', name: field.name
    @[field.name] = field
  clear: () ->
    while @length > 0
      @pop()
  describe: () ->
    for f in @
      f.describe()
  get: (field) ->
    if typeof(field) == 'string' and @hasOwnProperty(field)
      @[field]
    else if field instanceof Field
      for f in @
        if f == field
          return f
      return undefined
    else
      return undefined
  index: (field) ->
    helper = (key) ->
      for f, i in @
        if f.name == key
          return i
      return -1
    helper if field instanceof Object than field.name else field
  drop: (field) ->
    index = @index field
    if index >= 0 # it exists...
      @splice index, 1
  add: (field, option = {}) -> # we'll have to have something else implementing these...
    index = 
      if option.before # what happens if it's the zeroth position?
        @index option.before
      else if option.after
        @index(option.after) + 1
      else
        @length
    @splice index, 0, field
  names: () ->
    f.name for f in @
  diff: (fields) ->
    inLeft = (fs1, fs2) ->
      output = []
      for f in fs1
        if not fs2.hasOwnProperty(f.name)
          ouput.push f.describe()
      output
    inBoth = (fs1, fs2) ->
      for f in fs1
        if fs2.hasOwnProperty(f.name)
          output.push f.diff(fs2[f.name])
      output
    fullDiff = obj.diff @, fields
    if not fullDiff
      null
    else
      left: inLeft @, fields
      right: inLeft fields, @
      both: inBoth @, fields
  join: (fieldList) -> 
    @concat fieldList     

module.exports =
  Field: Field
  FieldList: FieldList
