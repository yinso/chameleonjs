Exception = require '../shared/exception'
RefHolder = require './refholder'
BaseProperty = require './baseproperty'
Property = require './property'
Environment = require './environment'
OPCodes = require './opcodes'
logger = require '../shared/log'

class UserFunction
  @compile: (vm, exp, env) ->
    newEnv = @makeNewEnv exp.args, env
    bodyExp = 
      if exp.body.begin
        vm.makeBlock exp.body.begin, newEnv, false, true
      else
        vm.compile exp.body, newEnv, true
    bodyExp.push [OPCodes.return]
    validateExp = []
    argsExp = 
      for arg in exp.args
        argObj = Property.make vm, arg, newEnv
        if argObj instanceof Property
          logger.debug 'argObj.ctor', argObj.name, argObj.ctor instanceof UserFunction, argObj.ctor
          validateExp.push [OPCodes.refEnv, argObj.name], [OPCodes.push, argObj.ctor], [OPCodes.call, 2], [OPCodes.defEnv, argObj.name]
        [OPCodes.push, argObj]
    bodyExp = validateExp.concat bodyExp
    logger.debug 'makeFunction', exp
    argsExp.concat [[OPCodes.push, bodyExp], [OPCodes.push, exp.function], [OPCodes.makeFunction, exp.args.length + 2]]
  @makeNewEnv: (args, env) ->
    argsEnv = {}
    for {name} in args
      argsEnv[name] = new RefHolder(name)
    new Environment(argsEnv, env)
  @make: (exp, env) ->
    # not the same as the compile time... hmm... but the answer is correct here... the question is... what can be delayed about the compilation to move toward here?
    # maybe we move the compilation here?
    # that'll solve the current problem for sure...
    args = []
    body = undefined
    name = undefined
    for i in [0...exp.length]
      if exp[i] instanceof BaseProperty
        args.push exp[i]
      else if exp[i] instanceof Array
        body = exp[i]
      else
        name = exp[i]
    logger.debug 'UserFunction.make', name, args, body
    new @(name, args, body)
  resolveClosure: (env) ->
    result = []
    for opcode in @body
      if opcode[0] == OPCodes.refClosure
        # we'll retrieve the value now...
        result.push [OPCodes.push, env.get(opcode[1])]
      else
        result.push opcode
    @body = result
  constructor: (@name, @argsList, @body) ->
    @optionalArgs = @countOptionalArgs @argsList
    logger.debug 'UserFunction.ctor', @argsList
  countOptionalArgs: () ->
    i = 0
    for arg in @argsList
      if arg.optional
        i++
    i
  argsToFrame: (args) ->
    result = {}
    logger.debug 'FunctionExp.argsToFrame', args.length, @argsList.length, @optionalArgs
    if args.length < (@argsList.length - @optionalArgs) # way too few... this is an error
      throw new Exception error: 'invalid_function_arity', value: args.length
    if args.length < @argsList.length # we'll have to match them
      diff = @argsList.length - args.length
      skipped = 0
      # count backwards from funcArg.length - skip over optional ones, until
      # skipped == diff
      for i in [@argsList.length - 1..0]
        if @argsList[i].optional
          if skipped < diff
            skipped++
          else
            result[@argsList[i].name] = args[i - (diff - skipped)] # is this correct here?
        else
          result[@argsList[i].name] = args[i - (diff - skipped)]
        logger.debug 'FunctionExp.argsToFrame', i, @argsList[i].name, @argsList[i].optional, result
    else # we have enough to match them...
      for i in [0...@argsList.length]
        logger.debug 'FunctionExp.argsToFrame', i, @argsList[i], @argsList[i].name, result
        result[@argsList[i].name] = args[i] # this line ought to be merged into FuncArgExp...
    result
  run: (args, env, cb) -> # there isn't really a whole lot to do here it seems...
    cb null, @

module.exports = UserFunction
