_ = require 'underscore' # already has a deep equal function

# we'll now need a way to diff things.

# if they are the same - return null
# else if they are not the same - return {left: 

diff = (o1, o2, recur = true) ->
  # a few assumptions... we only care up to the basic types
  if o1 instanceof Object and o2 instanceof Object # both objects
    objectDiff o1, o2, recur
  # non-objects.
  else if typeof(o1) != typeof(o2)
    # what's the diff terminology?
    {left: o1, right: o2}
  # they are of the same type.
  else if o1 != o2
    {left: o1, right: o2}
  else
    null

objectDiff = (o1, o2, recur) ->
  inBoth = (o1, o2) ->
    result = {}
    for key, val of o1
      if o2.hasOwnProperty(key)
        res = if recur
          diff o1[key], o2[key]
        else if deepEqual(o1[key], o2[key]) # they do not deeply equal each other... hmm...
          null
        else
          {left: o1[key], right: o2[key]}
        if res
          result[key] = res
    result

  inLeft = (o1, o2) ->
    result = {}
    for key, val of o1
      if o1.hasOwnProperty(key) and not o2.hasOwnProperty(key)
        result[key] = val
    result

  if o1 == o2
    null
  else
    left: inLeft o1, o2, recur
    right: inLeft o2, o1, recur
    both: inBoth o1, o2, recur


deepEqual = (o1, o2) ->
  left = (o1, o2) ->
    for key, val of o1
      if not o2.hasOwnProperty(key)
        return false
    true
  both = (o1, o2) ->
    for key, val of o1
      if not deepEqual(o1[key], o2[key])
        return false
    true
  if o1 == o2 # the same object.
    true
  else if o1 instanceof Object and o2 instanceof Object
    left(o1, o2) and left(o2, o1) and both(o1, o2)
  else
    false

module.exports =
  diff: diff
  deepEqual: deepEqual # this allows us to quickly determine what we need...
  
