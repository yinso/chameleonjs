Relation = require './relation'
Type = require './type2' # we might be able to inherit from Table... but do we need to?
async = require '../shared/async'
_ = require 'underscore'
logger = require '../shared/log'
Exception = require '../shared/exception'
OPCodes = require './opcodes'
Tuple = require './tuple'
BuiltIn = require './builtin'
BaseProperty = require './baseproperty'
Environment = require './environment'
UserFunction = require './function'
Cursor = require './cursor'
JoinClause = require './joinclause'
AliasRelation = require './aliasrelation'

#
# this turns out to be the select query that's specific for the memory objects...
# maybe we build something on top of it...???
# 
class SelectQuery extends Relation
  @JoinClause: JoinClause
  @compile: (vm, {select, from, where}, env, toExec = true) ->
    code = [[OPCodes.push, 'from']]
    try
      table = @resolveRelation(vm, from, env)
      if not (table instanceof Relation)
        throw new Exception 'from_not_a_relation', value: from
      code.push [OPCodes.push, table]
      code = code.concat @compileSelectColumns(vm, select, table, env)
      if where
        code.push [OPCodes.push, 'where'], [OPCodes.push, table.normalizeExp(where, env)]
      else 
      code.push [OPCodes.makeObject, if where then 8 else 6]
      code.push [OPCodes.makeSelectQuery]
      if toExec
        code.push [OPCodes.select]
      code
    catch e
      logger.error 'VM.makeSelectQuery', e.message
      throw e
  @resolveRelation: (vm, from, env) ->
    vm.evalExpSync from, env
  @compileFrom: (vm, from, where, env) ->
    vm.compile from, env
  @compileSelectColumns: (vm, select, table, env) ->
    if select == '*'
      resultType = table.type
      [[OPCodes.push, 'select'], [OPCodes.push, '*'], [OPCodes.push, 'resultType'], [OPCodes.push, resultType]]
    else
      columns = @normalizeSelectColumns(select, table, env)
      resultType = @makeNewTupleType vm, columns, env
      code =
        [[OPCodes.push, {object: ([name, column] for {column, name} in columns)}]]
      [[OPCodes.push, 'resultType'], [OPCodes.push, resultType], [OPCodes.push, 'select']].concat code
  @makeNewTupleType: (vm, columns, env) ->
    props = 
      for col in columns
        if col.column.id
          env.get(col.column.id)
        else
          BaseProperty.make vm, {name: col.name}, env
    tuple = Tuple.make vm, {name: undefined, props: props}, env
    logger.error 'makeNewTupleType', columns, tuple
    tuple
  @normalizeSelectColumns: (columns, table, env) ->
    for col, i in columns
      {column, name} = @normalizeSelectColumn(col, i, env)
      column: table.normalizeExp(column, env)
      name: name
  @normalizeSelectColumn: (col, i, env) ->
    if col.name
      col
    else
      {column} = col
      if column.id
        {column: column, name: column.id}
      else
        {column: column, name: "exp#{i}"}
  @make: (vm, {select, from, where, resultType}, env) ->
    if not (from instanceof Relation)
      throw new Exception 'from_not_a_relation', value: from
    query = new SelectQuery(select, from, where, resultType)
    if where
      query.whereExp = vm.compile where, env
    if select and select != '*'
      console.log 'SelectQuery.make_selectExp', select
      query.selectExp = vm.compile select, env
    query
  constructor: (@select, @from, @where, @resultType) ->
    # the where clause is the last filter act...
  project: (vm, relation, cb) ->
    helper = (rec, next) =>
      # the helper - craete a query
      cursor = new Cursor relation, rec
      code = [[OPCodes.pushCursor, cursor]].concat(@selectExp).concat [[OPCodes.popCursor]]
      console.log 'SelectQuery.project', code
      vm.pushCallStack code
      vm.runAsync next
    if @selectExp
      async.map relation.inner, helper, (err, res) =>
        if err
          cb err
        else
          cb null, new Relation(relation.name, @resultType, res)
    else
      cb null, relation      
  exec: (vm, cb) ->
    @from.select vm, (err, res) =>
      if err
        cb err
      else if not @whereExp
        @project vm, res, cb
      else
        res.filter vm, @whereExp, (err, res) =>
          if err
            cb err
          else
            logger.error 'SelectQuery.exec', res, @select
            @project vm, res, cb

module.exports = SelectQuery