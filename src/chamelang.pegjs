
/**********************************************************************
PREAMBLE Code
**********************************************************************/
{

var logger = require('../shared/log');

function reduce(ary, iterator, memo) {
  return ary.reduce(iterator, memo);
}

function contains(ary, item) {
  for (var i = 0; i < ary.length; ++i) {
    if (ary[i] === item) {
      return ary[i];
    }
  }
  return undefined;
}

function extend(dest) {
  var args = [].slice.call(arguments, 1); 
  for (var i = 0; i < args.length; ++i) {
    source = args[i]; 
    for (var key in source) {
      dest[key] = source[key];
    }
  }
  return dest;
}

// this might or might not be useful anymore... hmmm...
function leftAssociative(lhs, rest) {
  if (rest.length == 0) {
    return lhs;
  }
  var args = [ lhs ]; 
  var op = rest[0].op;
  while (rest.length > 0) {
    var rhs = rest.shift();
    if (rhs.op == op) {
      args.push(rhs.rhs);
    } else {
      rest.unshift(rhs);
      break; 
    }
  }
  var result = {funcall: {id: op}, args: args}
  if (rest.length == 0) {
    return result;
  } else {
    return leftAssociative(result, rest); 
  }
}

function make(val) {
  return val;
}

function convertTable(spec) {
  var columns = getColumns(spec.decls);
  var constraints = getConstraints(spec.decls);
  return {name: spec.name, columns: columns, constraints: constraints};
}

function getColumns(decls) {
  var helper = function (memo, decl) {
    if (decl.column) {
       memo.push(decl.column);
    }
    return memo;
  }
  return reduce(decls, helper, []);
}

function getConstraints(decls) {
  var helper = function (memo, decl) {
    if (decl.constraint) {
       memo.push(decl.constraint);
    }
    return memo;
  }
  return reduce(decls, helper, []);
}

function makeColumn(col, constraints) {
  var column = extend.apply(col, [col].concat(constraints));
  return column;
}

function toObject(obj) {
  var result = {}
  for (var key in obj.object) {
      var kv = obj.object[key];
      result[kv[0]] = kv[1];
  }
  logger.debug('toObject', obj, result);
  return {object: result}; 
}

function makeFloat(str) {
  return parseFloat(str);
}

function refExpression(head, rest) {
  // head will be either an ID or another inner expression...
  // we'll have to nest things 
  var result = head;
  for (var i = 0; i < rest.length; ++i) {
      var next = rest[i];
      if (next.call) {
         result = {funcall: result, args: next.call};
      } else if (next.ref) {
        result = {ref: result, key: next.ref}; 
      } else {
        throw new Error("unsupported_reference_exp: " + JSON.stringify(next));
      }
  }
  return result;
}

function joinClause(table1, joinExps) {
  logger.debug('joinClause', table1, joinExps);
  if (joinExps.length == 0) {
    return table1;
  } else {
    var lhs = table1;
    var exp = undefined; 
    for (var i = 0; i < joinExps.length; i++) {
       exp = joinExps[i];
       exp.lhs = lhs; 
       lhs = exp; 
       logger.debug('joinClause', i, lhs, exp, joinExps[i]);
    }
    return exp;
  }
}

function makeProperty(prop, constraints) {
  // logger.debug('makeProperty', prop, constraints);
  for (var i in constraints) {
    // logger.debug('constraints', i, constraints[i]);
    extend(prop, constraints[i]);
  }
  return prop;
}


/*
 binding can be one of the following form.
 everything will be an object.
 so the test is to see if it's 
 binding.object
 binding.array
 binding.name (individual item)
*/
function getBindingIdentifiers(bindings) {
  var result = [];
  logger.debug('getBindingIdentifiers', bindings);
  if (bindings.name) { // base case.
    result.push(bindings);
    return result; 
  } else if (bindings.object) {
    for (var i in bindings.object) {
      var binding = bindings.object[i];
      innerBindings = getBindingIdentifiers(binding);  
      result = result.concat(innerBindings); 
    }
    return result; 
  } else if (bindings.array) {
    for (var i in bindings.array) {
      var binding = bindings.array[i];
      innerBindings = getBindingIdentifiers(binding);  
      result = result.concat(innerBindings); 
    }
    return result; 
  } else {
    throw new Error('invalid_binding_identifier: ' + binding);
  }
}

function orderBindingReferences(bindings, id, key) {
  var result = []
  if (bindings.name) {
     if (id instanceof Object) {
        result.push({ref: id, key: {literal: key ? key : bindings.name}});
     } else if (typeof(id) == 'string') {
        result.push({ref: {id: id}, key: {literal: key ? key : bindings.name}});
     } else {
        result.push({id: bindings.name});
     }
     return result; 
  } else if (bindings.object) {
    for (var i in bindings.object) {
      var binding = bindings.object[i];
      var innerBindings = orderBindingReferences(binding, id); 
      result = result.concat(innerBindings);
    }
    return result; 
  } else if (bindings.array) {
    for (var i in bindings.array) {
      var binding = bindings.array[i];
      var innerBindings = null;
      if (binding.object) {
         innerBindings = orderBindingReferences(binding, {ref: {id: id}, key: {literal: i}}, i);
      } else if (binding.array) {
         innerBindings = orderBindingReferences(binding, {ref: {id: id}, key: {literal: i}}, i);
      } else {
         innerBindings = orderBindingReferences(binding, id, i);
      }
      result = result.concat(innerBindings);
    }
    return result; 
  } else {
    throw new Error('invalid_binding_identifier: ' + binding);
  }
}

function defPatternBinding(bindings, bodyExp) {
  logger.debug('defPatternBinding', bindings, bodyExp);
  filteredBindings = getBindingIdentifiers(bindings);
  // we'll need to have a list of values...
  // {foo, bar} = {foo: 1, bar: 2}; 
  // def: ['foo', 'bar'], exp: 
  // {begin: [
  //   {def: 'x', exp}
  //   {values: [
  //     {ref: {id: 'x'}, key: 'foo'} // x.foo
  //     {ref: {id: 'x'}, key: 'bar'} // x.bar
  //   ]}
  // ]}
  // we'll then need to figure out how to 
  var id = 'x'; 
  orderedBindings = {values: orderBindingReferences(bindings, id)};
  newBodyExp = {block: [{def: id, exp: bodyExp}, orderedBindings]};
  return {def: filteredBindings, exp: newBodyExp};
}

var keywords = [
    'begin'
    , 'end'
    , 'if'
    , 'as'
    , 'then'
    , 'else'
    , 'create'
    , 'alter'
    , 'drop'
    , 'table'
    , 'select'
    , 'from'
    , 'where'
    
    , 'inner'
    , 'outer'
    , 'left'
    , 'right'
    , 'join'
    , 'on'
    
    , 'having'
    , 'group'
    , 'insert'
    , 'update'
    , 'delete'
    , 'set'
    , 'def'
    , 'and'
    , 'or'
    , 'true'
    , 'false'
    , 'null'
    //, 'undefined'
    , 'type'
    , 'typeof'
    , 'try'
    , 'catch'
    , 'finally'
    , 'throw'
    , 'trace'
    , 'untrace'
    , 'values'
    , 'tuple'
    , 'relation'
    , 'describe'
    , 'show'
];

}

/* ===== Syntactical Elements ===== */

/**********************************************************************
TOP LEVEL Grammar

This way we are returning multiple expressions at once...
**********************************************************************/

program
  = _ head:any_expression _ rest:any_expression_list _ { return {begin: [head].concat(rest)}; }
  / _ exp:any_expression _ { return exp; }

any_expression_list
  = head:any_expression _ rest:any_expression_list _ { return [head].concat(rest); }
  / head:any_expression _ { return [head]; }

any_expression
  = exp:command _ { return exp; }             // specific to the runtime, not language
  / exp:statement _ { return exp; }           // DDL
  / exp:expression _ { return exp; }          // DML & language
  / comment

/**********************************************************************
Expression
**********************************************************************/

expression
  = block_expression
  / query_expression
  / inline_expression

/**********************************************************************
BLOCK Expression
**********************************************************************/

block_expression
  = DefineExp
  / BeginExp
  / TraceExp
  / UnTraceExp

/**********************************************************************
BEGIN Expression

  begin
    <exp> ... 
  end
**********************************************************************/

BeginExp
  = ('{' / 'begin') _ exp:expression _ tail:expression* _ ('}' / 'end') _ { 
    return {begin: [exp].concat(tail)}; 
  }

/**********************************************************************
DEFINE Expression
  def <name> = <exp>
  
  or 

  def <name> (<arg>...) <exp>

  def {name1, nam2} = <exp>
  => def foo = <exp>
     def name1 = foo.name1
     def name2 = foo.name2
  // to do this we'll need the ability to handle multiple values being 
  // returned... 
  // def values will take the latest push and then assign multiple items
  // according to the list in the first place...??
  // or it'll just handle multiple pushes?
  // 
  => def (name1, name2) = 
     begin
       def foo = exp
       values (name1, name2)
     end
**********************************************************************/

DefineExp
  = DefineValuesExp
  / 'def' _ id:PropertyDeclExp _ '=' _ exp:expression _ { return {def: id, exp: exp}; }
  / DefineTypeExp
  / DefineRelationExp
  / DefineFunctionExp

DefineValuesExp
  = 'def' _ bindings:_bindingsExp _ '=' _ exp:expression _ { 
    return defPatternBinding(bindings, exp); 
  }

_bindingsExp
  = _bindingsObjectExp
  / _bindingsArrayExp

_bindingsObjectExp
  = '{' _ head:_bindingItemExp _ tail:_tailBindingItemExp* _ '}' _ { return {object: [head].concat(tail)}; }

_bindingsArrayExp
  = '[' _ head:_bindingArrayItemExp _ tail:_tailBindingArrayItemExp* _ ']' _ { return {array: [head].concat(tail)}; }

_bindingItemExp
  = PropertyDeclExp
  / _bindingsArrayExp

_tailBindingItemExp 
  = ',' _ id:_bindingItemExp _ { return id; }

_bindingArrayItemExp
  = _bindingItemExp
  / _bindingsObjectExp

_tailBindingArrayItemExp
  = ',' _ binding:_bindingArrayItemExp { return binding; }


/**********************************************************************
ValuesExp
**********************************************************************/
ValuesExp
  = 'values' _ '(' _ values:_valuesList _ ')' _ { return {values: values}; }

_valuesList
  = head:inline_expression _ tail:_tailValueExp* _ { return [head].concat(tail); }

_tailValueExp
  = ',' _ exp:inline_expression { return exp; }

/**********************************************************************

def type <typeName> 

// we'll need to be able to define contract.
// regex: /^abc/ => ought to return (a) -> /^abc/.exec a
// length > 0 => ought to return (a) -> a.length > 0
// minLength: 0 

type email = string where regex(//) and minLength(10) // these are done as functions...

type email = 
  s string where match(/^abc/, s) and s.length > 10 

type email = 
  string with s where match(/^abc/, s) and s.length > 10

type tuple (name string, email email)

// the key is to have something that makes sense on how it wouuld be evaluated.

type emailOrString = email or string

def type email 
  base: string
  regex: //

def type hexString
  base: string
  regex: //

# scalar types are basically a single item object
# that'll inherit from a base type and a set of validation methods to 
# ensure that it fits the model of the base type...
# there can be other operations on the particular type... will have to 
# think about those later...
# so it would be cool to write them that way...

def type User (login string not null, 

def type password (hash: hexString*, salt: hexString*)

# what are these??
# these are object types...
# we'll need to support a maker function...
def type token ()



**********************************************************************/

DefineTypeExp
  = 'typeof' _ '(' _ exp:expression _ ')' _ {
    return {funcall: {id: 'typeof'}, args: [exp]};
  }
  / 'type' _ name:identifier _ '=' _ type:_propDeclTypeExp _ constraints:_propConstraintExp* { 
    prop = {name: name, type: type};
    return {def: name, exp: makeProperty(prop, constraints)};
  }  
  / 'type' _ id:identifier _ '=' _ 'tuple' _ props:_tuplePropertyList _ {
    return {def: id, exp: {tuple: id, props: props}};
  }

_tuplePropertyList
  = '(' _ arg:PropertyDeclExp _ tail:_tailFunctionFormalParam* _ ')' _ {
    return [arg].concat(tail);
  }


DefineRelationExp
  = 'type' _ id:identifier _ '=' _ 'relation' _ props:_tuplePropertyList _ {
    return {def: id, exp: {relation: id, props: props}};
  } 

DefineFunctionExp
  = 'def' _ id:identifier _ args:_functionFormalParamList _ exp:expression _ {
    return {def: id, exp: {function: id, args: args, body: exp}};
  }
  / 'func' _ id:identifier _ args:_functionFormalParamList _ exp:expression _ {
    return {function: id, args: args, body: exp};
  }
  / 'func' _ args:_functionFormalParamList _ exp:expression _ {
    return {function: 'anonymous', args: args, body: exp};
  }

_functionFormalParamList
  = '(' _ ')' _ { return []; }
  / '(' _ arg:PropertyDeclExp _ tail:_tailFunctionFormalParam* _ ')' _ {
    return [arg].concat(tail);
  }

_tailFunctionFormalParam
  = ',' _ param:PropertyDeclExp _ { return param; }

/**********************************************************************
PropertyDeclExp

reused for def & function... the two ought to eventually merge together
with each other...

**********************************************************************/
// function formal parameter ought to match the way columns are created...
PropertyDeclExp
  = prop:PropertyIdentifierExp { return prop; }
  / id:identifier { return {name: id}; }

// this is very, very similar to columnDeclExp - should try to combine them...
PropertyIdentifierExp
  = name:identifier _ type:_propDeclTypeExp _ constraints:_propConstraintExp* { 
      prop = {name: name, type: type};
      return makeProperty(prop, constraints);
    }

_propConstraintExp
  = exp:_columnDeclNullExp _ { return exp; }
  / _columnDeclDefaultExp
  / where:WhereClause { return {where: where}; }

_propDeclTypeExp
  = id:identifier { return {id: id}; }

/**********************************************************************
TraceExp

**********************************************************************/
TraceExp
  = 'trace' _ id:identifier _ { return {trace: id}; }

UnTraceExp
  = 'untrace' _ id:identifier _ { return {untrace: id}; }

/**********************************************************************
Inline Expression - expressions that can be embedded within another expression.

Technically even block expressions can be embedded into another expression, but
some are logically more embeddable than others.

For example - select statement isn't exactly an embedded expression, but a few
other expressions can potentially embed it. So it depends.

**********************************************************************/
inline_expression
  = IfExp
  / TryCatchExp
  / ThrowExp
  / OrExp
  // / in_expression
  / atomic_expression

/**********************************************************************
IF Expression

This logically is a block expression... hmmm...
**********************************************************************/
IfExp 
  = 'if' _ cond:OrExp _ 'then' _ thenExp:expression _ 'else' _ elseExp:expression _ { 
    logger.debug('if_parsed', {if: cond, then: thenExp, else: elseExp}); 
    return {if: cond, then: thenExp, else: elseExp}; 
    }

/**********************************************************************
Try Catch Expresison
**********************************************************************/
TryCatchExp
  = 'try' _ exps:inline_expression* _ 'catch' _ val:_errorExp _ exp:inline_expression _ 
    'finally' _ finallyExp:inline_expression _ {
    return {try: {begin: exps}, catch: val, handle: exp, finally: finallyExp};
  }
  / 'try' _ exps:inline_expression* _ 'catch' _ val:_errorExp _ exp:inline_expression _  {
    return {try: {begin: exps}, catch: val, handle: exp};
  } 

_errorExp
  = val:identifier { return val; }

/**********************************************************************
Throw Expression
**********************************************************************/
ThrowExp
  = 'throw' _ exp:inline_expression {
    return {throw: exp};
  }

/**********************************************************************
Atomic Expression - expressions that are semantically a single item.
**********************************************************************/
atomic_expression 
  = ReferenceExp
  / NotExp // unary
  / ArrayExp
  / ObjectExp
  / ParenedExp // unary
  / LiteralExp

/**********************************************************************
Parened Expression - expression within parenthesis. This is a single unit.
**********************************************************************/
ParenedExp
  = _ '(' _ exp:expression _ ')' _ { return exp; }

/**********************************************************************
LiteralExp - numbers, string, etc.
**********************************************************************/
LiteralExp
  = number // unary
  / regular_expression
  / string // unary
  / 'true' { return {literal: true}; }
  / 'false' { return {literal: false}; }
  / 'null' { return {literal: null}; }
  // / 'undefined' { return {literal: undefined}; }

/**********************************************************************
Array Expression - the values can be any expression (i.e. not just literals)
**********************************************************************/
ArrayExp
  = '[' _ ']' _ { return {array: []}; }
  / '[' _ head:_arrayItemExp _ rest:_arrayItemTailExp* _ ']' _ { 
    return {array: [head].concat(rest)}; 
  }

_arrayItemExp
  = inline_expression

_arrayItemTailExp
  = ',' _ item:_arrayItemExp { return item; }

/**********************************************************************
Object Expression - specify an Object, where the value can be any expression
**********************************************************************/
ObjectExp
  = '{' _ '}' _ { return {object: {}}; }
  / _ "{" _ head:_objectKeyValExp _ rest:_objectKeyValTailExp* "}" _ { 
    return {object: [head].concat(rest)};
  }

_objectKeyValExp
  = name:_objectKeyExp _ ":" _ value:inline_expression _ { return [name, value]; }

_objectKeyExp
  = identifier
  / string

_objectKeyValTailExp
  = ',' _ keyval:_objectKeyValExp _ { return keyval; }

/**********************************************************************
Reference Expression - for referring a specific item (as well as eval as funcall)
The reason of having both is that they are of the same precedence.

  abc
  abc.foo
  abc.foo()

**********************************************************************/
ReferenceExp
  = ref:_referenceHeadExp _ key:_referenceKeyExpList _ { return refExpression(ref, key); }
  / _referenceHeadExp

_referenceHeadExp
  = LiteralExp
  / NotExp // unary
  / ArrayExp
  / ObjectExp
  / ParenedExp // unary
  / id:identifier { return {id: id}; }
  / '(' _ inner:inline_expression _ ')' { return inner; }

_referenceKeyExpList
  = head:_referenceKeyExp _ rest:_referenceKeyExpList { return [head].concat(rest); }
  / head:_referenceKeyExp {return [head]; }

_referenceKeyExp
  = '.' _ key:identifier _ { return {ref: {literal: key}}; }
  / '[' _ key:inline_expression _ ']' { return {ref: key}; }
  / '(' _ ')' _ { return {call: []}; }
  / '(' _ args:_funcallParamsExp _ ')' _ { return {call: args}; }

_funcallParamsExp
  = lhs:inline_expression rest:_funcallParamTailExp* _ { return [lhs].concat(rest); }

_funcallParamTailExp
  = ',' _ rhs:inline_expression _ { return rhs; }

/**********************************************************************
Operator Expressions

Different operator have different precedences

// operator precedence
// . [idx] (ref/member operator), () (funcall)
// ! not (logical not; unary)
// * / % (arithemtic multiplicative)
// + - (arithmetic additive)
// <= < >= > (less than/greater than)
// == != (equal, not equal)
// = (assignment) (block expression)

All operations are basically treated uniformly with funcalls @ parser 
level - this is the same behavior as lisp family.

**********************************************************************/

/**********************************************************************
NOT Exp -> !<exp>
**********************************************************************/
NotExp  // highest precedence
  = '!' _ exp:atomic_expression _ { return {funcall: {id: '!'}, args: [exp]}; }

/**********************************************************************
Multiply Exp -> <exp> (*, /, %) <exp> ...
**********************************************************************/
MultiplyExp 
  = lhs:atomic_expression _ rest:_multiplyTailExp* _ { return leftAssociative(lhs, rest); }

_multiplyTailExp
  = op:('*' / '/' / '%') _ rhs:atomic_expression _ { return {op: op, rhs: rhs}; }

/**********************************************************************
Add Exp -> <exp> (+, -) <exp> ...
**********************************************************************/
AddExp 
  = lhs:MultiplyExp _ rest:_addTailExp* _ { return leftAssociative(lhs, rest); }

_addTailExp
  = op:('+' / '-') _ rhs:MultiplyExp _ { return {op: op, rhs: rhs}; }

/**********************************************************************
Comparison Exp -> <exp> (+, -) <exp> ...
**********************************************************************/
ComparisonExp 
  = lhs:AddExp _ rest:_comparisonTailExp* _ { return leftAssociative(lhs, rest); }

_comparisonTailExp
  = op:('<=' / '<' / '>=' / '>' / '==' / '!=') _ rhs:AddExp _ { return { op: op, rhs: rhs }; }


/**********************************************************************
AND (Logical) Exp -> <exp> (&&, and) <exp> ...
**********************************************************************/
AndExp 
  = lhs:ComparisonExp _ rest:_andTailExp* _ { return leftAssociative(lhs, rest); }

_andTailExp
  = ('&&' / 'and') _ rhs:ComparisonExp _ { return { op: '&&', rhs: rhs}; }

/**********************************************************************
OR (Logical) Exp -> <exp> (&&, and) <exp> ...
**********************************************************************/
OrExp
  = lhs:AndExp _ rest:_orTailExp* _ { return leftAssociative(lhs, rest); }

_orTailExp
  = ('||' / 'or') _ rhs:AndExp _ { return { op: '||', rhs: rhs}; }

/**********************************************************************
IN Expression -> item 'in' [array_item]
... uncertain whether we want to have this expression available... should think about it more.

**********************************************************************/
in_expression // mostly ensure that a single reference
  = lhs:atomic_expression _ 'in' _ rhs:ArrayExp { return {funcall: {id: 'in'}, args: [lhs, rhs]}; }

/**********************************************************************
QUERY Expression
**********************************************************************/

query_expression
  = SelectQuery
  / InsertQuery
  / DeleteQuery
  / UpdateQuery

/**********************************************************************
Select Query Expression
**********************************************************************/
SelectQuery
  = 'select' _ exp:_selectTableColumnExps _ table:JoinClause _ where:WhereClause? { return {select: exp, from: table, where: where}; }
  / 'select' _ exp:_selectNoTableExps _ { return {select: exp}; }

_selectTableColumnExps
  = '*' _ { return '*'; }
  / exp:_selectColumnExp _ exps:(_selectColumnTailExp)* _ { return [exp].concat(exps); }

_selectNoTableExps 
  = '*' _ { return []; }                        
  / exp:_selectColumnExp _ exps:(_selectColumnTailExp)* _ { return [exp].concat(exps); }

_selectColumnTailExp
  = ',' _ exp:_selectColumnExp _ { return exp; }

_selectColumnExp
  = column:inline_expression _ 'as' _ name:string_or_identifier { return {column: column, name: name}; }
  / column:inline_expression _ { return {column: column}; } // there is no name defined on this one..

/**********************************************************************
JOIN Clause
This is reused across all queries.
**********************************************************************/
JoinClause
  = 'from' _ table1:tableName _ exps:joinExp* _ { return joinClause(table1, exps); }

joinExp 
  = innerJoinExp
  / crossJoinExp
  / leftJoinExp
  / rightJoinExp

innerJoinExp
  = exp:_commonJoinExp { exp.join = 'inner'; return exp; }
  / 'inner' _ exp:_commonJoinExp { exp.join = 'inner'; return exp; }

_commonJoinExp
  = 'join' _ table2:tableName _ 'on' _ exp:_whereClauseExp _ { return {rhs: table2, exp: exp}; }

crossJoinExp
  = 'cross' _ exp:_commonJoinExp { exp.join = 'cross'; return exp; }

leftJoinExp
  = 'left' _ exp:_commonJoinExp { exp.join = 'left'; return exp; }
  / 'left' _ 'outer' _ exp:_commonJoinExp { exp.join = 'left'; return exp; }

rightJoinExp
  = 'right' _ exp:_commonJoinExp { exp.join = 'right'; return exp; }
  / 'right' _ 'outer' _ exp:_commonJoinExp { exp.join = 'right'; return exp; }

tableName
  = name:identifier _ alias:identifier _ { return {id: name, alias: alias}; }
  / id:identifier { return {id: id}; }
  / '(' _ inner:SelectQuery _ ')' _ alias:identifier _ { return {inner: inner, alias: alias}; }

/**********************************************************************
WHERE Clause
This is reused across all queries.
**********************************************************************/
WhereClause
  = 'where' _ exp:_whereClauseExp _ { return exp; }

_whereClauseExp
  = inline_expression

/**********************************************************************
DELETE Query Expression
**********************************************************************/
DeleteQuery
  = 'delete' _ 'from'? _ table:identifier _ where:WhereClause? _ { return {delete: table, where: where}; }

/**********************************************************************
INSERT Query Expression
**********************************************************************/
// keep in mind the object can be a valid expression - i.e. it also needs to be evaluated.
InsertQuery
  = 'insert' _ 'into'? _ table:identifier _ 'values' _ values:_insertExpList {
    return {insert: {id: table}, values: values};
  }
  / 'insert' _ 'into'? _ table:identifier _ exp:SelectQuery _ {
    return {insert: {id: table}, select: exp}; 
  }

_insertExpList
  = exp:any_expression _ tail:_tailInsertExp* _ {
    return [exp].concat(tail);
  }

_tailInsertExp
  = ',' _ exp:any_expression _ { return exp; }

_insertFieldList
  = '(' _ field:identifier _ rest:_insertFieldTailExp* _ ')' _ {
    return [field].concat(rest);
  }

_insertFieldTailExp 
  = ',' _ field:identifier _ { return field; }

_insertValuesExp 
  = '(' _ val:inline_expression _ rest:_insertValueTailExp* _ ')' _ { 
    return [val].concat(rest);
  }

_insertValueTailExp
  = ',' _ exp:inline_expression { return exp; }

/**********************************************************************
UPDATE Query Expression
**********************************************************************/
UpdateQuery
  = 'update' _ table:identifier _ 'set' _ columns:_updateSetExps _ where:WhereClause? {
    return {update: table, set: columns, where: where};
  } 

_updateSetExps
  = head:_updateSetExp _ tail:_updateSetTailExp* _ { 
    return [ head ].concat(tail); 
  }

_updateSetExp
  = id:identifier _ '=' _ exp:inline_expression _ { return {setField: id, exp: exp}; }

_updateSetTailExp
  = ',' _ exp:_updateSetExp _ { return exp; }


/**********************************************************************
STATEMENTS
**********************************************************************/
statement 
  = CreateTableStatement
  / DropTableStatement
  / AlterTableStatement
  / CreateIndexStatement
  / DropIndexStatement

/**********************************************************************
CREATE TABLE
**********************************************************************/
// we will want to allow for definitions of other things here... we'll also need to ensure that things are sorted appropriately...
CreateTableStatement
  = 'create' _ 'table' _ table:identifier _ '(' _ columns:_tableDeclExpList _ ')' _ { 
    return {createTable: convertTable({name: table, decls: columns})}; 
  }

_tableDeclExpList 
  = head:_tableDeclExp _ tail:_tableDeclTailExp* _ { return [ head ].concat(tail); }

_tableDeclTailExp
  = ',' _ decl:_tableDeclExp _ { return decl; }

_tableDeclExp 
  = PrimaryKeyDeclExp
  / UniqueKeyDeclExp
  / ForeignKeyDeclExp
  / ColumnDeclExp

/**********************************************************************
Column Declaration
**********************************************************************/
ColumnDeclExp
  = name:identifier _ type:_columnDeclTypeExp _ constraints:_columnConstraintExpList? { 
      col = {name: name, type: type};
      return {column: makeColumn(col, constraints)};
    }

_columnConstraintExpList
  = head:_columnConstraintExp tail:_columnConstraintTailExp* { return [ head ].concat(tail); }

_columnConstraintTailExp
  = _ constraint:_columnConstraintExp { return constraint; }

_columnConstraintExp
  = _columnDeclNullExp
  / _columnDeclDefaultExp
  / where:WhereClause { return {where: where}; }
  / 'primary' _ 'key' _ { return {primary: true}; }
  / 'unique' _ { return {unique: true}; }
  / 'foreign' _ 'key' _ 'references' _ table:identifier _ '(' _ columns:identifiers _ ')' _ { return {foreign: {table: table, columns: columns}}; }

_columnDeclTypeExp
  = identifier

_columnDeclNullExp
  = 'null' _ { return {optional: true}; }
  / 'not' _ 'null' _ { return {optional: false}; }

_columnDeclDefaultExp
  = 'default' _ val:_columnDeclDefaultValExp { return {default: val}; }

_columnDeclDefaultValExp
  = inline_expression

/**********************************************************************
Primary Key Declaration
**********************************************************************/
PrimaryKeyDeclExp
  = 'primary' _ 'key'? _ name:identifier? _ '(' _ columns:identifiers _ ')' _ { return {constraint: {name: name, columns: columns, primary: true}}; }
  / 'constraint' _ name:identifier? _ 'primary' _ 'key'? _ '(' _ columns:identifiers _ ')' _ { return {constraint: {name: name, columns: columns, primary: true}}; }

/**********************************************************************
Unique Key Declaration
**********************************************************************/
UniqueKeyDeclExp
  = 'unique' _ 'key'? _ name:identifier? _ '(' _ columns:identifiers _ ')' _ { return {constraint: {name: name, columns: columns, unique: true}}; }
  / 'constraint' _ name:identifier? _ 'unique' _ 'key'? _ '(' _ columns:identifiers _ ')' _ { return {constraint: {name: name, columns: columns, unique: true}}; }

/**********************************************************************
Foreign Key Declaration
**********************************************************************/
ForeignKeyDeclExp
  = 'foreign' _ 'key' _ name:identifier? _ '(' _ columns:identifiers _ ')' _ 'references' _ table:identifier _ '(' _ references:identifiers _ ')' _ { 
      return {constraint: {name: name, columns: columns, foreign: {table: table, columns: references}}}; 
    }
  / 'constraint' _ name:identifier? _ 'foreign' _ 'key' _ '(' _ columns:identifiers _ ')' _ 'references' _ table:identifier _ '(' _ references:identifiers _ ')' _ {
      return {constraint: {name: name, columns: columns, foreign: {table: table, columns: references}}}; 
    }

/**********************************************************************
DROP TABLE
**********************************************************************/
DropTableStatement
  = 'drop' _ 'table' _ table:identifier { return {dropTable: {name: table}}; }

/**********************************************************************
ALTER TABLE
**********************************************************************/
AlterTableStatement
  = 'alter' _ 'table' _ table:identifier _ exps:_alterTableDeclList _ { 
    return {alterTable: {name: table, alter: exps}}; 
  }

_alterTableDeclList
  = head:_alterTableDeclClause _ tail:_alterTableDeclTailClause* { 
    return [ head ].concat(tail); 
  }

_alterTableDeclTailClause
  = ',' _ exp:_alterTableDeclClause { return exp; }

_alterTableDeclClause
  = _addColumnClause 
  / _dropColumnClause
  / _alterColumnClause
  / _addConstraintClause
  / _dropConstraintClause

// by default the added column needs to be null...
_addColumnClause
  = 'add' _ 'column' _ column:ColumnDeclExp { return {alter: 'addColumn', exp: column.column}; }

_dropColumnClause
  = 'drop' _ 'column' _ column:identifier { return {alter: 'dropColumn', exp: column}; }

_alterColumnClause 
  = 'alter' _ 'column' _ column:ColumnDeclExp { return { alter: 'alterColumn', exp: column.column}; }

_addConstraintClause
  = 'add' _ exp:PrimaryKeyDeclExp { return {alter: 'addConstraint', exp: exp.constraint}; }
  / 'add' _ exp:UniqueKeyDeclExp { return {alter: 'addConstraint', exp: exp.constraint}; }
  / 'add' _ exp:ForeignKeyDeclExp { return {alter: 'addConstraint', exp: exp.constraint}; }

_dropConstraintClause
  = 'drop' _ 'constraint'? _ name:identifier { return {alter: 'dropConstraint', exp: name}; }

/**********************************************************************
CREATE INDEX
**********************************************************************/
CreateIndexStatement
  = 'create' _ 'unique' _ 'index' _ name:identifier _ 'on' _ table:identifier _ 
    '(' _ columns:identifiers _ ')' _ foreign:ForeignKeyDeclExp? { 
        var result = {createIndex: {name: name, table: table, 
               columns: columns, unique: true, foreign: foreign}}; 
        if (result.createIndex.foreign == '') {
           delete result.createIndex.foreign;
        }
        return result; 
  }
  / 'create' _ 'index' _ name:identifier _ 'on' _ table:identifier _ 
    '(' _ columns:identifiers _ ')' _ foreign:ForeignKeyDeclExp? { 
        var result = {createIndex: {name: name, table: table, 
               columns: columns, foreign: foreign}}; 
        if (result.createIndex.foreign == '') {
           delete result.createIndex.foreign;
        }
        return result; 
  }

/**********************************************************************
DROP INDEX
**********************************************************************/
DropIndexStatement
  = 'drop' _ 'index' _ name:identifier _ 'on' _ table:identifier _ { 
           return {dropIndex: {name: name, table: table}}; 
  }

identifiers
  = head:identifier _ tail:tail_identifiers* { return [ head ].concat(tail); }

tail_identifiers
  = ',' _ id:identifier { return id; }

string_or_identifier
  = string
  / identifier

/**********************************************************************
Command Expression
**********************************************************************/
command
  = 'query' _ args:ObjectExp _ { return {command: 'query', args: args}; }
  / ShowCommand
  / DescribeCommand
  / SaveCommand
  / 'quit' _ { return {command: 'quit'}; }
  / DiffCommand
  / ConfigCommand
  / LoadCommand

/**********************************************************************
Config Expression
**********************************************************************/
// config expression is used to change the configuration values.
ConfigCommand
  = ConfigSetConnectionCommand
  / ConfigDiffSchemaCommand

ConfigSetConnectionCommand
  = 'config' _ 'set' _ 'connection' _ '=' _ args:ObjectExp { return {command: 'config', args: {setConnection: toObject(args).object}}; }

ConfigDiffSchemaCommand
  = 'config' _ 'diff' _ 'schema' _ path:string _ { return {command: 'config', args: {diffSchema: path}}; } 

/**********************************************************************
Diff Expression

// diff expresison is used to compare two tables... this might be obsoleted 

**********************************************************************/
DiffCommand
  = 'diff' _ 'table' _ table1:identifier _ table2:identifier { 
    return {command: 'diffTable', args: {table1: table1, table2: table2}}; 
  }

/**********************************************************************
SHOW Command
**********************************************************************/
ShowCommand
  = 'show' _ 'tables' _ { return {command: 'showTables'}; }
  / 'show' _ 'types' _ { return {show: 'types'}; }
  / 'show' _ 'indexes' _ { return {command: 'showIndexes'}; }
  / 'show' _ 'env' _ { return {show: 'env'}; }
  / 'show' _ 'stack' _ { return {show: 'stack'}; }

/**********************************************************************
DESCRIBE Command
**********************************************************************/
DescribeCommand
  = 'describe' _ 'table' _ name:identifier _ { return {command: 'describeTable', args: name}; }
  / 'describe' _ 'type' _ name:identifier _ { return {command: 'describeType', args: name}; }
  / 'describe' _ 'index' _ name:identifier _ { return {command: 'describeIndex', args: name}; }
  / 'describe' _ 'schema' _ { return {command: 'describeSchema', args: null}; }
  / 'describe' _ name:identifier _ { return {describe: name}; }

/**********************************************************************
SAVE Command
**********************************************************************/
SaveCommand
  = 'save' _ 'schema' _ path:string _ { return {command: 'saveSchema', args: path}; }
  / 'clear' _ 'schema' _ { return {command: 'clearSchema', args: null}; }
  / 'load' _ 'schema' _ path:string _ { return {command: 'loadSchema', args: path}; }

/**********************************************************************
LOAD Command
**********************************************************************/
LoadCommand
  = 'load' _ path:string _ { return {command: 'load', args: path.literal}};

/**********************************************************************
LEXICAL EXPRESSION
**********************************************************************/
/* ===== Lexical Elements ===== */

id_char1
  = [a-z]
  / [A-Z]
  / '_'

id_char_rest
  = id_char1
  / [0-9]

identifier
  = c:id_char1 rest:id_char_rest* & { // testing block - identifier must not exist as keyword 
    var id = [c].concat(rest).join('');
    return !contains(keywords, id);
  } { 
    return ([c].concat(rest)).join(''); 
  }


regular_expression
  = '/' chars:regular_expression_char+ '/' { return {regex: chars.join('')}; }

regular_expression_char
  = '\\/' 
  / [^/]

string "string"
  = '"' chars:chars '"' _ { return {literal: chars}; }
  / "'" chars:chars "'" _ { return {literal: chars}; }

chars
  = chars:char* { return chars.join(""); }

char
  // In the original JSON grammar: "any-Unicode-character-except-"-or-\-or-control-character"
  = [^"'\\\0-\x1F\x7f]
  / '\\"'  { return '"';  }
  / "\\'"  { return "'"; }
  / "\\\\" { return "\\"; }
  / "\\/"  { return "/";  }
  / "\\b"  { return "\b"; }
  / "\\f"  { return "\f"; }
  / "\\n"  { return "\n"; }
  / "\\r"  { return "\r"; }
  / "\\t"  { return "\t"; }
  / whitespace 
  / "\\u" digits:hexDigit4 {
      return String.fromCharCode(parseInt("0x" + digits));
    }

hexDigit4
  = h1:hexDigit h2:hexDigit h3:hexDigit h4:hexDigit { return h1+h2+h3+h4; }

number "number"
  = exp:_number { 
    //logger.debug('number_parsed', exp);
    return {literal: exp}; 
  }

_number 
  = int:int frac:frac exp:exp _ { return makeFloat([int,frac,exp].join('')); }
  / int:int frac:frac _     { return makeFloat([int,frac].join('')); }
  / int:int exp:exp _      { return makeFloat([int,exp].join('')); }
  / int:int _          { return makeFloat(int); }
  / "infinity" { return Number.POSITIVE_INFINITY; }
  / '+' _ 'infinity' { return Number.POSITIVE_INFINITY; }
  / '-' _ 'infinity' { return Number.NEGATIVE_INFINITY; }

int
  = d:digits { return d.join(''); }
  / "-" digits:digits { return ['-'].concat(digits).join(''); }

frac
  = "." digits:digits { return ['.'].concat(digits).join(''); }

exp
  = e digits:int { return 'e'+digits; }

digits
  = digit+

e
  = [eE] [+-]?

/*
 * The following rules are not present in the original JSON gramar, but they are
 * assumed to exist implicitly.
 *
 * FIXME: Define them according to ECMA-262, 5th ed.
 */

digit
  = [0-9]

digit19
  = [1-9]

hexDigit
  = [0-9a-fA-F]

/* ===== Whitespace ===== */

_ "whitespace"
  = whitespace*

// Whitespace is not defined in the original JSON grammar, so I assume a simple
// conventional definition consistent with ECMA-262, 5th ed.
whitespace
  = comment 
  / [ \t\n\r]

lineTermChar
  = [\n\r\u2028\u2029]

lineTerm "end of line"
  = "\r\n"
  / "\n"
  / "\r"
  / "\u2028" // line separator
  / "\u2029" // paragraph separator

sourceChar
  = .

// should also deal with comment.
comment
  = multiLineComment
  / singleLineComment

singleLineCommentStart
  = '#'  // bash style
  / '//' // c style
  / '--' // sql style

singleLineComment
  = singleLineCommentStart chars:(!lineTermChar sourceChar)* lineTerm? { 
    return {comment: chars.join('')}; 
  }

multiLineComment
  = '/*' chars:(!'*/' sourceChar)* '*/' { return {comment: chars.join('')}; }
  / '#{' chars:(!'}#' sourceChar)* '}#' { return {comment: chars.join('')}; }
