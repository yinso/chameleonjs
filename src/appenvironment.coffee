###

what is an environment?

for example - when you log into chameleon, immediately you run the default environment.

an environment contains the following.

1 - the particular connection (there can be multiple) that are being loaded.

2 - the schema version associated with the environment (this will differ from the
    REPL environment until the changes are applied against the connection within the system)

# there can be multiple environments, and we can load and switch environments... it means the environment
# will be configured somewhere...

# let's say that we are going to have a single 

###

# 

fs = require '../shared/fs'
async = require '../shared/async'
Exception = require '../shared/exception'
path = require 'path'
Config = require './config'
Connection = require './connection' # the question still remains - how would connection be utilized here?
MongoDbConnection = require './mongodb-connection'
{Schema} = require './model'
logger = require '../shared/log'

class ConnectionFactory
  @types:
     mongodb: MongoDbConnection
  @make = (type, config) ->
    if not @types.hasOwnProperty(type)
      throw new Exception error: 'unknown_connection_type', type: type, config: config
    else
      new @types[type](config)

# how do I deal with the particular type of connection that we need to issue?
# first of all - environment will need to provide a specific sets of connections for use...
# but probably should be used via environment... 
class AppEnvironment extends Connection
  constructor: (@runtime) ->
  load: (config = Config.defaultConfig(), cb) ->
    # let's see if we have here...
    @config = new Config config
    logger.debug 'Environmnet.load', config
    @initialize cb
  configPath: () ->
    path.join process.cwd(), 'config.json'
  loadConfig: (cb) ->
    fs.readFile @configPath(), (err, data) =>
      if err
        cb err
      else
        try
          config = JSON.parse data.toString()
          logger.debug 'Environmnet.loadConfig', config
          @load config, cb
        catch e
          cb e
  loadSchema: (cb) ->
    @schema = new Schema @config.name
    fs.readFile @schemaPath(), (err, data) =>
      if err # file does not exist - create it.
        logger.debug 'AppEnvironment.loadSchema => ', err
        fs.writeFile @schemaPath(), JSON.stringify({types: [], tables: [], indexes: []}), cb
      else
        try
          data = data.toString()
          logger.debug 'AppEnvironment.loadSchema => ', data
          obj = JSON.parse data
          @schema.loadSchema obj
          cb null
        catch e
          cb e
  schemaPath: () ->
    path.join process.cwd(), 'env', @config.name, 'schema.json'
  connect: (cb) ->
    config = @config.connection
    logger.debug 'AppEnvironment.connect', config
    helper = (err) =>
      try
        @connection = ConnectionFactory.make config.type, config
        @connection.connect cb
      catch e
        cb e
    if not config
      cb null
    else if @connection
      @disconnect helper
    else
      helper()
  setConnection: ({name, type, config}, cb) ->
    name ?= 'default'
    logger.debug 'AppEnvironment.setConnection', name, type, config
    try
      @config.setConnection name, type, config
      @connect cb
    catch e
      cb e
  query: (query, cb) ->
    logger.debug 'query', query
    # the query will determine which table it will be connected to...
    # this would be interesting... for now let's just assume that 
    if @connection
      @connection.query query, cb
    else
      cb error: 'no_connection_established'
  diffSchemaPath: () ->
    path.join process.cwd(), 'env', @config.name, 'schema_diff.json'
  diffSchema: (path, cb) ->
    # we'll have to get hold of the outside environment...
    logger.debug 'AppEnvironment.diffSchema', path
    try
      result = @runtime.schema.diff @schema
      logger.debug 'AppEnvironment.diffSchema', result, result.models.both.user
      fs.writeFile @diffSchemaPath(), JSON.stringify(result, undefined, 2), cb
    catch e
      cb e
  disconnect: (cb) ->
    if @connection
      @connection.disconnect cb
    else
      cb null
  initialize: (cb) ->
    logger.debug 'AppEnvironment.initialize'
    if @config.connection
      @connect cb
    else
      cb null
  configure: (config, cb) ->
    if config.setConnection
      value = config.setConnection
      @setConnection {name: value.name, type: value.type, config: value}, cb
    else if config.diffSchema
      @diffSchema config.diffSchema, cb
    else
      cb error: 'unknown_configuration_args', value: config
  
module.exports = AppEnvironment