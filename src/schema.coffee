Registry = require './registry'
_ = require 'underscore'
logger = require '../shared/log'

# let's see what to do with the base of schema.
# a schema and its manipulation should be separated... I would say...
# because this just has to do with the basic structure for manipulation.
class Schema # this is a complete environment...!!!
  constructor: (@name) ->
    logger.debug 'Schema.ctor', @name
    @plugins = new Registry noDupe: true, name: 'plugins' # these are for code within
    @defaults = new Registry noDupe: true, name: 'defaults'
    @validators = new Registry noDupe: true, name: 'validators'
    @types = new Registry noDupe: true, name: 'types'
    @constraints = new Registry noDupe: true, name: 'constraints'
    @tables = new Registry noDupe: true, name: 'tables'
  setClass: (option) ->
    logger.debug 'Schema.setClass', option, @[option.class] # why would this be 
    if not @[option.class] instanceof Registry
      throw new Exception error: 'unknown_registry_type', value: option
    registry = @[option.class]
    if option.type instanceof Function
      registry.type = option.type
    if option.validate instanceof Function
      registry.validate = option.validate
    if option.has instanceof Function
      registry.has = option.has
  registerDefault: (key, val) ->
    @defaults.register key, val
  hasDefault: (name) ->
    @defaults.has name
  registerValidator: (key, val) -> # requires the knowledge of validator?
    @validators.register key, val
  hasValidator: (name) ->
    @validators.has name
  registerType: (key, val) ->
    @types.register key, val
  hasType: (name) ->
    @types.has name
  registerConstraint: (key, val) ->
    logger.debug 'Schema.registerCostraint', key, val
    @constraints.register key, val
  hasConstraint: (name) ->
    @constraints.has name
  registerTable: (key, val) ->
    table = @tables.register key, val
    logger.debug 'Schema.registerTable', table
    for cons in table.constraints or []
      @registerConstraint cons.name, cons
  hasTable: (name) ->
    @tables.has name
  fieldReferences: (field) ->
    # go through all of the constraints... and see which ones are referencing this particular field.
    _.filter @constraints.values(), (cons) -> cons.references field
  describe: () ->
    # validators & defaults are not part of the schema description
    # as they involve code.
    types =
      for type in @types.values()
        type.describe()
    logger.debug 'Schema.describe_types', types
    tables = 
      for table in @tables.values()
        table.describe()
    constraints = 
      for cons in @constraints.values()
        cons.describe()
    types: types
    tables: tables
    constraints: constraints
  diff: (schema) ->
    # we should come back to this...
    # lastly - we now should provide a way to load classes...
    # the classes ought not be registered immediately -> instead
    # they should be provided out...
    # they'll be loaded into modules.
    # the below isn't something that makes a lot of sense immediately...
    # because that's not something that can be acted upon.
    output = obj.diff @describe(), schema.describe()
    if not output
      null
    else
      output # this can be quite a bit of work... hmmm...
  # how should the plugins be organized...
  # if they are just modules
  # should we ask the modules themselves to *add* things or should we allow
  # for types to add themselves?
  # 1 - all tables are types...
  # 2 - should we load all plugins from a basic directory?
  # 3 - loading plugins this way will mean
  # 4 - we'll also have to worry about loading the schemas only on the server-side...
  # 5 - is this a schema that can be used by both? it's unclear...
  # 
    
  @base = new Schema 'base' # this is the basic schema that's available everywhere..  
  @current = @base # is this one place where we can set everything to start from beginning?
    

# how to reorganize
# there are a few basic rules.
# 1 - by default there is a single schema.
# 2 - the single schema will permeate everything available.
# 3 - in special cases there are multiple schemas.
#     the only special case, really, is during the time where we are *upgrading* an environment
#     against the baseline.
# 4 - that means it's important to optimize against a single schema in the first place!
#
# okay - we'll have schema being the base of which everything is designed against.


module.exports = Schema
