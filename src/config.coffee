
fs = require '../shared/fs'
Exception = require '../shared/exception'

# before we worry about changing the connection we'll deal with the simple version.
class Config
  @load = (config) ->
    new Config config
  constructor: ({@name, @connection}) ->

module.exports = Config
