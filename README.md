Chameleon - A Relational-Oriented Language
=============================

Chameleon is a relational-oriented language that are inspired by the following language designs

* Scheme - functional-oriented programming; dynamic typing
* SQL - You'll see familiar SQL query syntax in the langauge itself.
* Erlang - Concurrency and error handling


Chameleon has the following features

* functions are first class entities
* tail call optimization
* relations are first class entities
* runtime design by contract
* uniform data access via relation modelling

The current version of Chameleon is implemented in NodeJS and Coffeescript. There are a few NodeJS-ism that have not yet been abstracted away to make the runtime workable inside browser yet, but that'll eventually be overcome.








