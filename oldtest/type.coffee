#Validator = require '../src/validator'
Type = require '../src/type'
Exception = require '../shared/exception'
Schema = require '../src/schema-server'

path = require 'path'

# loading the plugins doesn't allow for writing out the changes of the fields to separate
# places... if we can write it out we can basically load it back in... for now we are not
# ready to do that yet so we'll need additional approaches to update the changes of the fields
# 
Schema.current.loadAllPlugins path.join(__dirname, '../plugin')

Schema.current.saveSchema path.join(__dirname, '../chameleon')

Schema.current.loadSchema path.join(__dirname, '../chameleon')

Token = Type.has 'token' 
Password = Type.has 'password'

{assert} = require 'chai'

Type.register
  name: 'test'
  base: 'object'
  fields: [
    {name: 'foo', type: 'integer'}
    {name: 'bar', type: 'string'}
    {name: 'baz', type: 'float', default: 16}
    {name: 'xyz', type: {base: 'list', type: 'integer', minLength: 1}, default: {object: [10]}}
  ]

describe 'object maker test', () ->
  t1 = Type.has 'test'

  it 'should validate 0', (done) ->
    t1.validate {foo: 0, bar: 'hello'}, (err, res) ->
      console.log 'should validate 0', err, res
      if err
        done err
      else
        done null

  it 'should validate', (done) ->
    t1.validate {foo: 15, bar: 'hello'}, (err, res) ->
      console.log err, res
      if err
        done err
      else
        done null
  it 'should make', (done) -> # should fill in a default value...
    
    t1.make {foo: 15, bar: 'hello'}, (err, res) ->
      console.log err, res
      if err
        done err
      else
        done null
  


describe 'ssn Test', () ->
  s1 = '000-00-0000'
  it 'should validate', (done) ->
    Type.validate 'ssn', s1, (err, val) ->
      console.log 'ssn_test', err, val
      if err
        done err
      else
        try
          assert.equal val, s1
          done null
        catch e
          done e
  
  s2 = '000'
  it 'should fail validation', (done) ->
    Type.validate 'ssn', s2, (err, val) ->
      console.log 'ssn_failing_test', err, val
      if err
        done null
      else
        done new Exception(error: 'should_fail_but_succeeded', value: val)

  it 'should create ssn object', (done) ->
    Type.make 'ssn', s1, (err, val) ->
      console.log 'ssn_create_test', err, val
      if err
        done err
      else
        try
          assert.ok typeof(val) == 'string'
          done null
        catch e
          done e

  it 'should create ssn1 object', (done) ->
    Type.make 'ssn1', s1, (err, val) ->
      console.log 'ssn1_create_test', err, val
      if err
        done err
      else
        try
          assert.equal val, s1
          done null
        catch e
          done e

# token test... where is it?

# let's start the token test here...

describe 'token Test', () ->
  t1 = null
  it 'should create a token', (done) ->
    Type.make 'token', {}, (err, token) ->
      console.log err, token
      t1 = token
      # time to figure out how do we verify what we have created?
      if err
        done err
      else
        try
          Token.verify t1, (err, res) ->
            if err
              done err
            else
              done null

describe 'password Test', () ->
  p1 = {password: 'test1Password!'}
  passwordObj = null
  it 'should create a password', (done) ->
    Type.make 'password', p1, (err, passwd) ->
      console.log err, passwd
      passwordObj = passwd
      if err
        done err
      else
        done null
  it 'shold verify password', (done) ->
    Password.verify passwordObj, p1, (err, res) ->
      console.log err, res
      if err
        done err
      else
        done null

  it 'should verify password token', (done) ->
    Password.verifyToken passwordObj, (err,res) ->
      console.log err, res
      if err
        done err
      else
        done null

  it 'should update password token', (done) ->
    Password.updateToken passwordObj, (err, res) ->
      console.log err, res
      if err
        done err
      else
        done null
  
describe 'Field.reference test', () ->
  Session = Schema.base.hasTable 'Session'
  it 'has ID reference', (done) ->
    try
      refs = Schema.current.fieldReferences Session.fields['id']
      console.log 'Field.refs = ', refs
      assert.ok refs.length > 0
      done null
    catch e
      done e
  it 'cannot be deleted under restrict', (done) ->
    try
      Session.dropField 'id'
      done new Exception error: 'should_be_restricted_for_dropping_id'
    catch e
      console.log 'expect_failing_to_delete_Session_id', e
      done null
  it 'can be deleted under cascade', (done) ->
    try
      Session.dropField 'id', cascade: true
      assert.ok not Schema.current.constraints.hasOwnProperty('pk_Session_id')
      done null
    catch e
      done e
