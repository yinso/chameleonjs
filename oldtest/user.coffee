require '../src/password'
User = require '../src/user'
{assert} = require 'chai'

describe 'User ctor test', () ->
  it 'should create user object successfully', (done) ->
    User.makeAsync {login: 'yc', password: {password: 'test1Password!'}}, (err, user) ->
      console.log 'user ctor', user
      if err
        done err
      else
        done null
