Connection = require '../src/schema-connection'
{Schema} = require '../src/model'
MongoDbConnection = require '../src/mongodb-connection'
Password = require '../src/password'
User = require '../src/user'
{assert} = require 'chai'

conn = null

describe 'connection init test', () ->
  it 'should open successfully', (done) ->
    try
      conn = new Connection Schema.base,
        new MongoDbConnection {}
      done null
    catch e
      done e

# how do we ensure that the connection will try to open up the database tables?
# this might not want to be done every time... but we would want to test the logic.
# for example - the uniqueness constraint shall be enforced by 


describe 'connection connect test', () ->
  it 'should connect okay', (done) ->
    conn.connect (err, res) ->
      console.log err, res
      done err

describe 'insert test', () ->
  it 'should insert okay', (done) ->
    conn.query insert: 'user', values: {login: 'yc', password: {password: 'test1Password!'}}, (err, res) ->
      console.log err, res
      done err

describe 'select test', () ->
  it 'should select okay', (done) ->
    conn.query select: '*', from: 'user', (err, res) ->
      console.log err, res
      done err

describe 'update test', () ->
  it 'should update successfully', (done) ->
    conn.query update: 'user', set: {'profile': {abc: 1}, 'password.token': undefined}, where: {login: 'yc'}, (err, res) ->
      console.log err, res
      done err

describe 'select again test', () ->
  it 'should select okay', (done) ->
    conn.query select: '*', from: 'user', (err, res) ->
      console.log err, res
      done err

describe 'delete test', () ->
  it 'should delete okay', (done) ->
    conn.query delete: 'user', (err, res) ->
      console.log err, res
      done err

describe 'connection close test', () ->
  it 'should disconnect okay', (done) ->
    conn.disconnect (err, res) ->
      console.log err, res
      done err



