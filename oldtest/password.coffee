Password = require '../src/password'
{assert} = require 'chai'

describe 'hmac Test', () ->
  key = Password.makeSalt()
  salt = Password.makeSalt()
  passwd = 'hello1World!'
  password = {password: 'test1Password!'}
  it 'should verify ok', (done) ->
    console.log key: key, salt: salt, passwd: passwd
    Password.makeHmac key, salt, passwd, (err, hmac) ->
      console.log err: err, hmac: hmac
      if err
        done err
      else
        Password.verifyHmac key, salt, passwd, hmac, (err, res) ->
          if err
            done err
          else
            done null 
  it 'should make out a password object', (done) ->
    Password.makeAsync password, (err, res) ->
      # should res instanceof Password pass? for now let's not do that.
      # how do we add additional functions that this object can be used?
      # we can do something about its prototype being a maker...
      console.log 'Password.makeAsync', err, res
      if err
        done err
      else
        try
          assert.ok res.key
          assert.ok res.salt
          assert.ok res.hash
          done null
        catch e
          done e
  
  passwdObj = null
  
  it 'should verify password', (done) ->
    Password.makeAsync password, (err, res) ->
      console.log 'Password.makeAsync', err, res
      if err
        done err
      else
        passwdObj = res
        Password.verify passwdObj, password, (err, res) ->
          if err
            done err
          else
            done null
  
  it 'should verify password token', (done) ->
    Password.verifyToken passwdObj, (err, res) ->
      if err
        console.log 'Password.verifyToken_error', err
        done err
      else
        done null

  it 'should update password token', (done) ->
    Password.updateToken passwdObj, (err, res) ->
      console.log 'Password.updateToken', err, res
      if err
        done err
      else
        done null
