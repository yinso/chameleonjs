
{assert} = require 'chai'

Type = require '../src/type'
Exception = require '../shared/exception'
Schema = require '../src/schema-server'
Relation = require '../src/relation'
path = require 'path'
#Schema.current.loadAllPlugins path.join(__dirname, '../plugin')

#Schema.current.loadSchema path.join(__dirname, '../chameleon')

Type.register
  name: 'test20'
  base: 'object'
  fields: [
    {name: 'foo', type: 'integer'}
    {name: 'bar', type: 'string'}
    {name: 'baz', type: 'float', default: 16}
    {name: 'xyz', type: {base: 'list', type: 'integer', minLength: 1}, default: {object: [10]}}
  ]

Type.register
  name: 'test30'
  base: 'object'
  fields: [
    {name: 'foo1', type: 'integer'}
    {name: 'bar1', type: 'string'}
    {name: 'baz1', type: 'float', default: 16}
  ]


R1 = undefined

R2 = undefined

describe 'make relation', () ->
  it 'relation.make.ok', (done) ->
    try
      R1 = new Relation(Type.declare('test20'))
      console.log 'relation.R1', R1
      done null
    catch e
      done e
  
  it 'relation.should be able to insert', (done) ->
    R1.insert [{foo: 1, bar: 'hello'}, {foo: 2, bar: 'x'}, {foo: 3, bar: 'y', baz: 20.5}], (err, inserted) ->
      if err
        done err
      else
#        console.log 'relation.inserted', inserted, R1,
        console.log 'relation.inserted', inserted[0], inserted[0].get
        console.log 'relation.inserted', inserted[0].get('foo')
        done null

  it 'relation.should be able to search', (done) ->
    cond = ({foo, bar, baz, xyz}) ->
      foo == 2 and xyz.length >= 1
    R1.where cond, (err, records) ->
      if err
        done err
      else
        console.log 'relation.where', records
        done null

  it 'relation should be able to delete', (done) ->
    cond = ({foo}) ->
      foo == 1
    R1.delete cond, (err, deleted) ->
      if err
        done err
      else
        try
          assert.equal 2, R1.length
          console.log 'relation.delete', deleted, R1.length
          done null
        catch e
          done e

  it 'relation should be able to update', (done) ->
    where = ({foo}) ->
      foo == 2
    setExps =
      foo: (obj, cb) -> cb null, 2
      bar: ({bar}, cb) -> cb null, 'hello' + bar
    R1.update where, setExps, (err, updated) ->
      if err
        done err
      else
        try
          assert.equal 2, R1.length
          console.log 'relation.update', updated, R1
          done null
        catch e
          done e

  it 'update should be atomic', (done) ->
    where = ({foo}) ->
      foo > 1
    setExps =
      bar: (obj, cb) -> cb null, 1
    R1.update where, setExps, (err, updated) ->
      if err
        console.log 'relation.update_atomic', updated, R1
        done null
      else
        done new Exception error: 'expect_to_have_error', value: updated

  it 'should cross join appropriately', (done) ->
    R2 = new Relation Type.declare('test30')
    R2.insert [{foo1: 1, bar1: 'x'}, {foo1: 2, bar1: 'y'}], (err, res) ->
      if err
        done err
      else
        try
          R3 = R1.crossJoin R2
          console.log 'relation.crossJoin', R3[0] instanceof Type.has('object')
          console.log 'relation.crossJoin', R3[0].get('0.foo'), R3[0].get('1.foo1')
          # one thing to keep in mind is that the index will need to change as we handle
          # more join... i.e. what's 1 isn't necessarily 1 if we use auto index...
          # perhaps besides auto index we'll then use
          # and while the items are nested they aren't once we get to 
          # so the question is... how do we define access at this point?
          # we can do 1.foo, and 2.foo, for example, or we can do an alias
          # for the record... (i.e. this is built-in @ this level)...
          # if we want to consider alias - that ought to be attached @ the fieldList level
          # and that means we'll need to have a way to create a new FieldList type...
          # is this a field list type thing?
          # let's think about this...
          
          done null
        catch e
          done e

  # what about inner join?
  # inner join will require some sort of conditions to fulfill...

  # we should now deal with projection & join...
  # that'll need to be done @ the Type part...
  # projection is probably easier?
  # maybe, maybe not... hmm...
  
  # let's think about types... if we want to do join we'll first do join on the fields
  