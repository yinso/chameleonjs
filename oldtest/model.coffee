{Type, Constraint, ListOf, Default, Model, Schema} = require '../src/model'
{assert} = require 'chai'


Integer = Schema.base.makeType 'integer'
Float = Schema.base.makeType 'float'

describe 'type Test', () ->
  it 'integer should be validated', (done) ->
    try
      assert.ok Integer.validate 10
      assert.throws () -> Integer.validate 10.5, true
      assert.throws () -> Integer.validate 'not number', true
      done null
    catch e
      done e

  it 'float should be validated', (done) ->
    try
      assert.ok Float.validate 10
      assert.ok Float.validate 10.5
      assert.throws () -> Float.validate 'not number', true
      done null
    catch e
      done e

describe 'type async Test', () ->
  it 'integer should be validated asyncly', (done) ->
    Integer.validateAsync 10, (err, res) ->
      if err
        done err
      else
        assert.equal res, 10
        done null
  
  it 'integer should be validated asyncly 2', (done) ->
    Integer.validateAsync 10.5, (err, res) ->
      if err
        done null
      else
        done error: 'validation_failed', err: err, res: res

  it 'integer should be validated asyncly 2', (done) ->
    Integer.validateAsync 'not a number', (err, res) ->
      if err
        done null
      else
        done error: 'validation_failed', err: err, res: res

  it 'float should be validated asyncly', (done) ->
    Float.validateAsync 10, (err, res) ->
      if err
        done err
      else
        assert.equal res, 10
        done null

  it 'float should be validated asyncly', (done) ->
    Float.validateAsync 10.5, (err, res) ->
      if err
        done err
      else
        assert.equal res, 10.5
        done null

  it 'float should be validated asyncly 2', (done) ->
    Float.validateAsync 'not a number', (err, res) ->
      if err
        done null
      else
        done error: 'validation_failed', err: err, res: res

console.log 'got here'

listOfInt = Schema.base.makeType {listof: {type: 'integer', min: 2, max: 5}}

console.log 'got here'

describe 'listOf async Test', () ->
  it 'listOf should be validated asyncly', (done) ->
    listOfInt.validateAsync [1, 2, 3], (err, res) ->
      if err
        console.log 'listOf.validateAsync', err
        done err
      else
        done null
  it 'listOf should only validate array', (done) ->
    listOfInt.validateAsync 'x', (err, res) ->
      if err
        console.log 'listOf.expect error', err
        done null
      else
        done {error: 'expect_an_error_but_no_error'}
  
  it 'listOf should only validate array of integer', (done) ->
    listOfInt.validateAsync ['x'], (err, res) ->
      if err
        console.log 'listOf.expect error', err
        done null
      else
        done {error: 'expect_an_error_but_no_error'}

  it 'listOf should only validate array of integer within range', (done) ->
    listOfInt.validateAsync [1], (err, res) ->
      if err
        console.log 'listOf.expect error', err
        done null
      else
        done {error: 'expect_an_error_but_no_error'}

  it 'listOf should only validate array of integer within range 2', (done) ->
    listOfInt.validateAsync [1, 2, 3, 4, 5, 6], (err, res) ->
      if err
        console.log 'listOf.expect error', err
        done null
      else
        done {error: 'expect_an_error_but_no_error'}

# create constraints.
describe 'constraint Test', () ->
  it 'listOf should be created', (done) ->
    try
      listOfFloat = Schema.base.makeType {listof: {type: 'float'}} # should it be listof or listOf?
      listOfFloat.validateAsync [1,2,3,4,5,6], (err, res) ->
        if err
          done err
        else
          done null
    catch e
      done e

describe 'default test', () ->
  # problem with default is that things are 
  it 'default should work', (done) ->
    try
      d1 = Schema.base.makeDefault 5
      assert.equal d1.run(), 5
      done null
    catch e
      done e
  
  it 'default timestamp should work', (done) ->
    try
      now = (new Date()).getTime()
      d1 = Schema.base.makeDefault {proc: 'timestamp'}
      assert.ok d1.run() >= now
      done null
    catch e
      done e

  it 'uuid default should parse', (done) ->
    try
      d1 = Schema.base.makeDefault {proc: 'make_uuid'}
      assert.ok d1.run() # verifying uuid being unique globally is very hard...
      done null
    catch e
      done e



testModel = null

testModel2 = null
  
describe 'model Test', () ->
  it 'Model should be creatable', (done) ->
    try
      testModel = Schema.base.makeModel
        name: 'test'
        columns: [ {name: 'foo', type: {listof: {type: 'integer', min: 0, max: 5}}}
          {name: 'bar', type: 'float', min: 10, max: 20} # let's see if we can add a min constraint.
          {name: 'ssn', type: 'ssn' }
          ]
      testModel2 = Schema.base.makeModel
        name: 'test2'
        columns: [ {name: 'foo', type: 'integer'}
          {name: 'bar', type: 'float'}
          {name: 'ssn', type: 'ssn', default: '000-00-0000' }
        ]
      done null
    catch e
      console.log 'makeModel error: ', e
      done e
  it 'object should be validated', (done) ->
    try
      assert.ok testModel.validate {foo: [10], bar: 15, ssn: '000-00-0000'}
      assert.throws () -> testModel.validate {foo: 'x', bar: '10'}, true
      done null
    catch e
      done e

  it 'optional should work', (done) ->
    try
      assert.ok testModel2.validate {foo: 10, bar: 15, ssn: '000-00-0000'}, true
      assert.throws () -> testModel2.validate {foo: 'x', bar: '10'}, true
      assert.ok testModel2.validate {foo: 10, bar: 15}, true
      done null
    catch e
      done e
  
  it 'min constraint should work', (done) ->
    try
      testModel.validateAsync {foo: [10], bar: 9}, (err, res) ->
        if err
          console.log 'min_constraint_expect_error', err
          done null
        else
          done error: 'min_constraint_did_not_work'
    catch e
      done e

  it 'max constraint should work', (done) ->
    try
      testModel.validateAsync {foo: [10], bar: 100}, (err, res) ->
        if err
          console.log 'max_constraint_expect_error', err
          done null
        else
          done error: 'max_constraint_did_not_work'
    catch e
      done e

describe 'adding model to Type', () ->
  it 'Model should be registerable to Type', (done) ->
    try
      Schema.base.registerType name: 'test', test: testModel
      t = Schema.base.makeType 'test'
      assert.ok t.validate {foo: [10], bar: 15, ssn: '000-00-0000'}
      assert.throws () -> t.validate {foo: 'x', bar: '10'}, true
      done null
    catch e
      done e

describe 'model async Test', () ->
  it 'object should be validated', (done) ->
    obj = {foo: [10], bar: 15, ssn: '000-00-0000'}
    testModel.validateAsync obj, (err, res) ->
      if err
        done err
      else
        try
          assert.equal res, obj
          done null
        catch e
          done e

# time to figure out the maker
describe 'type maker test', () ->
  it 'should return value that makes the object', (done) ->
    try
      val = Schema.base.makeType('integer').make()
      assert.equal val, 0
      assert.throws () -> Schema.base.makeType('integer').make('x')
      done null
    catch e
      done e
  it 'makeAsync should work for integer too', (done) ->
    Schema.base.makeType('integer').makeAsync 10, (err, res) ->
      try
        assert.equal 10, res
        done null
      catch e
        done e
    

# let's see about model async test...
describe 'model maker test', () ->
  it 'model should make an object with default values', (done) ->
    try
      testModel.make {foo: [10], bar: 10, ssn: '000-00-0000'}
      done null
    catch e
      done e
  it 'model should make an object async style', (done) ->
    testModel.makeAsync {foo: [10], bar: 10, ssn: '000-00-0000'}, (err, res) ->
      try
        assert.deepEqual res, {foo: [10], bar: 10, ssn: '000-00-0000'}
        done null
      catch e
        done e
  it 'model should throw error for missing required property', (done) ->
    testModel.makeAsync {bar: 10, ssn: '000-00-0000'}, (err, res) ->
      console.log 'testModel.makeAsync', err, res
      if err
        done null
      else
        done error: 'expect_foo_required'

  it 'model should fill in missing value with defaults', (done) ->
    testModel2.makeAsync {foo: 10, bar: 10}, (err, res) ->
      console.log 'testModel2.makeAsync', err, res
      if err
        done err
      else
        try
          assert.equal '000-00-0000', res.ssn
          done null
        catch e
          done e
