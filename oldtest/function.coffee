require '../shared/function'
fs = require '../shared/fs'

# let's see if we are able to actually use this without issues.

identity = (x) -> x

cpsFib = (n, cb = identity) ->
  helper = (n, prev, current, cb) ->
    try
      if n < 2
        cb.tail null, current
      else
        helper.tail n - 1, current, current + prev, cb
    catch e
      cb.tail e
  helper.tail n, 0, 1, cb

describe 'tail call elimination test', () ->
  it 'should compute without timeout or stack overflow', (done) ->
    cpsFib 1000, (err, res) ->
      console.log 'fib 1000 ==> ', err, res
      if err
        done err
      else
        done null

# the question is... do you have to do TCO everywhere now?
# can we use this only in partial sense?

foo = (n, cb) ->
  cpsFib n, cb

bar = (n, cb) ->
  cb null, n * n * n * n

baz = (n, cb) ->
  cb.tail null, n # because this is the valid path...

test = (n, cb) ->
  foo.tail n, (err, res) ->
    if err
      cb.tail err
    else
      bar res, (err, res) ->
        if err
          cb.tail err
        else
          baz.tail res, cb

# 
# with this design a big part of the redesign can be eliminated
#
# it can also be used only when needed - i.e. we don't have to call tail everywhere... we just
# need to implement it where it matters (i.e. places where we have recursion as loops)
# so within the expression we don't need to deal with 

describe 'partial tail call elimination test', () ->
  # even if the last call is returned... there is no guarantee
  # that this is successful as Javascript can be triggered via process.tick or setTimeout
  # i.e. this method is javascript only - which is not what we need
  # (we need our language's TCO).
  # okay - so this is a waste of time... I should do that @ the expression level somehow...
  it 'should still compute correctly', (done) ->
    # needs to terminate before getting to the last value... hmmm...
    # the question is... how to insert that...
    test 5, (err, res) ->
      console.log 'test.tco 5 ==> ', err, res
      done err, res

# the key is simply remove the callstack...
asyncFib = (n, cb = identity) ->
  helper = (n, prev, current, cb) ->
    console.log 'tco helper', n, prev, current
    try
      if n < 2
        cb null, current
      else
        helper.tail n - 1, current, current + prev, cb
    catch e
      cb e
  helper n, 0, 1, cb

describe 'async fib should work', () ->
  it 'should handle async okay', (done) ->
    asyncFib 2000, (err, res) ->
      console.log 'tco asyncFib 1000', err, res
      done err  

foo1 = (filePath, cb) ->
  fs.readFile filePath, (err, data) ->
    if err
      cb err
    else
      cb err, data.toString()

bar1 = (data, cb) ->
  console.log '====> bar', data
  cb null, data

baz1 = (data, cb) ->
  cb.tail null, data

test1 = (filePath, cb) ->
  foo1 filePath, (err, res) ->
    if err
      cb err
    else
      bar1 res, (err, res) ->
        if err
          cb.tail err
        else
          baz1.tail res, cb

#describe 'tail call with async', () ->
#  it 'should handle async okay', (done) ->
#    test1 './function', (err, res) ->
#      console.log 'tco async', err, res
#      done err, res

# TCO does not work with async... we need special tools to deal with async.
# 