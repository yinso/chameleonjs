{assert} = require 'chai'
GitClient = require '../shared/git'
{resolveRoot, mkdirp, rmrf} = require '../shared/fs'
fs = require 'fs'
path = require 'path'

parentPath = '../'
grandParent = path.join parentPath, parentPath
path = './repos/test'

client = new GitClient(parentPath)

describe 'resolveRoot Test', () ->
  fakePath = './nothing/will/exist'

  it "#{fakePath} should resolve to '.'", (done) ->
    resolveRoot fakePath, (err, res) ->
      assert.deepEqual res,  {base: '.', paths: ['nothing', 'will', 'exist']}
      done(err)

describe 'mkdirp Test', () ->
  it "#{path} should be created!", (done) ->
    mkdirp path, (err, made) ->
      assert.ok !err
      done(err)

describe 'GitClient Test', () ->
  it "#{parentPath} should be a git repo (async)", (done) ->
    client.isa parentPath, (res) ->
      assert.ok res
      done()
  it "views/ should not be a git repo (async)", (done) ->
    client.isa 'views/', (res) ->
      assert.isFalse res
      done()
  it "#{path} should be created as git repo", (done) ->
    client.init path, (err, stdout, stderr) ->
      assert.ok !err
      console.log stdout
      done(err)
