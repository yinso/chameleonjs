MongoDbConnection = require '../src/mongodb-connection'
{assert} = require 'chai'

conn = null
rec = {foo: 1, bar: 2}
describe 'open test', () ->
  it 'should open succesfully', (done) ->
    try
      conn = new MongoDbConnection server: '127.0.0.1', port: 27017, database: 'test'
      conn.connect (err, res) ->
        console.log err, res
        done err
    catch e
      done e

describe 'insert test', () ->
  it 'should insert successfully', (done) ->
    conn.query insert: 'test', values: rec, (err, res) ->
      console.log err, res
      rec = res[0]
      if err
        done err
      else
        done null

describe 'insert dupe test', () ->
  it 'should fail', (done) ->
    conn.query insert: 'test', values: {_id: rec._id, foo: 10, bar: 15}, (err, res) ->
      console.log err, res
      if err
        done null
      else
        done error: 'fail_dupe_test'

describe 'select test', () ->
  it 'should select successfully', (done) ->
    conn.query select: '*', from: 'test', (err, res) ->
      console.log err, res
      done err

# update does not return the result of the record - so we'll need to
# update the data ourselves if we already have them...
describe 'update test', () ->
  it 'should update successfully', (done) ->
    conn.query update: 'test', where: {_id: rec._id}, set: {foo: 2}, (err, res) ->
      console.log err, res
      done err

describe 'select again test', () ->
  it 'should select successfully', (done) ->
    conn.query select: '*', from: 'test', where: {_id: rec._id}, (err, res) ->
      try
        console.log err, res
        assert.equal res[0].foo, 2
        done err
      catch e
        done e

describe 'delete test', () ->
  it 'should delete successfully', (done) ->
    conn.query delete: 'test', (err, res) ->
      console.log err, res
      done err

describe 'close test', () ->
  it 'should close sucessfully', (done) ->
    conn.disconnect (err) ->
      if err
        done err
      else
        done null
  