will need to deal with serialization of the schema, including the following

1 - type

;; integer
;; float
;; string 
;; listof <n>
;; procedure
;; table
;; constraint
;; foreign key... maybe I should create foreign key first?
;; I'll need to define constraint
;; the above are types that are built-into the system at this time.

;; regex-based types.
define type <name> = regex /regex/

;; aliasing a type; integer must exist at this time.
define type int aliasing integer 

;; a type that sits on top of another type.... for example
;; most types will be defined on top of string...

define type json from string ???

;; for now - maybe we don't worry too much about define type?
;; the tough part is in the scalar type... the composite type isn't
;; that hard...
;; the scalar type has to do with how to serialize it...
;; i.e. it needs to be loaded with certain signatures (and a list of
;; associated functions that can be run against that type)

;; but we'll need to 

;; we can make it similar to creating classes, along with the
   functions
   that are derived in... okay - this would actually be quite hard
   so we'll come back to this...
   for now - allow for backloading is good enough...

;; defining procedures, etc. are all easy to understand... just need
   to chain them together.
   
;; the next step is...???

;; determine the schema management...

;; 1 - we would want to craft ways to save what has been changed to
   the database to ensure things are kept in sync...
   
;; that means the following...

;; always have a way to version the snapshot
;; but also log the changes.

okay - let's think about this...

1 - we are not trying to do something similar to D. we are trying to
do something in between D and SQL. the goal is to provide programming
to SQL rather than strictly adhering to D.

2 - how to version schema?

obviously - you'll first need to be able to dump everything out at
once.

we'll need to be able to dump things out that can be specified, but
not things that have been built into the runtime itself.

so we'll need to know what's built into runtime and what's not.

For example - any of the user defined types are not built into runtime

but built-in type such as integer is.

So we have to have a built-in flag so it doesn't get serialized (it
won't be possible to serialize those)

We'll also have to allow for extension from "beneath" - this is mainly
for types, drivers, etc. those cannot be serialized (yet they ought to
be part of the migration) - so we'll need to have a place to hold the
extensions

so far - it's something like this...

/configs (including connections)
/plugins (for extension-based code - this depends on the underlying
system) (and this include connection modules)
/types (will have to include this in the future - for now this is just
base code)
/tables
/indexes
/procedures
/data
/logs

for storing metadata... maybe enable 

insert metadata into blah... 

it's possible people will forget about this... so we'll also have mark
a table as a metadata table.

set metadata tag on table app_menu

set prefix

set will be a swiss army knife to configure the system....


Configurations...

configuration will have to be specifically environment bound

For example - in your local dev box you'll have your data source
pointed to localhost, but you don't want that in your test or QA
environment.

So you'll have to be able to define environment, and connections are
bound against the environment.

we'll need to allow reconfiguring of a table into a different
configuration (i.e. connection, sharding, etc)..... 

....


I'm getting closer toward where things need to go...
it's time to design this with the 


set $env = 'default' # this will store the things in a particular
environment name. default is the default environment name...


def type ssn <string> regex: /^\d\d\d-?\d\d-?\d\d\d\d$/

def type naturalNumber <integer> {min: 0, max: +inf}

def type 

def type password <object> (
  password string
);



