Table = require '../../src/table'

class Session 
  @fields: [
    {name: 'id', type: 'uuid', default: {proc: 'make_uuid'}, primary: true}
    {name: 'data', type: 'object', default: {object: {}}}
  ]

module.exports = Session
