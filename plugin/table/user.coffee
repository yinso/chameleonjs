Type = require '../../src/type'
Table = require '../../src/table'
Password = require '../type/password'

class User extends Type.has('object')
  @fields: [
    {name: 'id', type: 'uuid', default: {proc: 'make_uuid'}, primary: true}
    {name: 'login', type: 'string', unique: true}
    {name: 'password', type: 'password'}
  ]

module.exports = User

