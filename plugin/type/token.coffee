Exception = require '../../shared/exception'
crypto = require '../../shared/crypto'
assert = require 'assert'
Type = require '../../src/type'
_ = require 'underscore'
logger = require '../../shared/log'

dateAdd = (d, days) ->
  new Date(d.getTime() + days * 86400)


class token extends Type.has('object')
  @base: 'object'
  @fields: [ # shouldn't this be an array?  it probably should...
    {name: 'id', type: 'string', default: {proc: 'make_uuid'}}
  ]
  @key: 'ca7372783d86e3473cbc4d4f2038881769936c5fbae51d9b71ac7595d5270c8a'
  @secret: 'bd2b02f34c8396fcf3969bea875b77446bebf9bd84721156d6d82184793486e7'
  @dateRange: 14
  make: (obj, cb) ->
    @generateHash obj, (err, res) =>
      if err
        cb err
      else
        cb null, new @(obj)  
  constructor: (obj) ->
    _.extend @, obj
    logger.debug 'Token.make', obj
  @generateHash: ({id, from}, cb) ->
    d = from or new Date()
    d2 = dateAdd d, @dateRange
    document = JSON.stringify
      s: @secret
      i: id
      f: d.getTime()
      t: d2.getTime()
    @encrypt document, (err, encrypted) =>
      logger.debug 'Token.enrypt', err, @makeHash
      if err
        cb err
      else
        @makeHash encrypted, (err, hashed) =>
          if err
            cb err
          else
            cb null, {id: id, data: encrypted, hash: hashed}
  @encrypt: (document, cb) ->
    try
      cipher = crypto.createCipher 'aes256', @key
      res = cipher.update document, 'utf8', 'hex'
      res += cipher.final 'hex'
      logger.debug 'Token.enrypt', res
      cb null, res
    catch e
      cb e
  @makeHash: (document, cb) ->
    try
      logger.debug 'Token.hash'
      hmac = crypto.createHmac 'sha256', @key
      hmac.update document
      res = hmac.digest 'hex'
      logger.debug 'Token.hash', res
      cb null, res
    catch e
      logger.debug 'Token.hash_error', e
      cb e
  @update: (token, cb) -> # first verify the current token - if it works then create a new one with the same uuid
    @verify token, (err, res) =>
      if err
        cb err
      else
        @make id: token.id, cb
  @verify: (token, cb) ->
    @verifyHash token, (err, res) =>
      if err
        cb err
      else
        @verifyDocument token, cb
  @verifyDocument: ({id, data}, cb) ->
    @decrypt data, (err, res) =>
      if err
        cb err
      else
        if res.i == id # we have matched...
          @verifyTimeframe res, cb
        else
          cb error: 'invalid_data', value: res
  @decrypt: (data, cb) ->
    try
      decipher = crypto.createDecipher 'aes256', @key
      res = decipher.update data, 'hex', 'utf8'
      res += decipher.final 'utf8'
      logger.debug 'Token.decrypt', res
      cb null, JSON.parse(res)
    catch e
      logger.debug 'Token.decrypt_error', e
      cb e
  @verifyHash: ({data, hash}, cb) ->
    @makeHash data, (err, res) ->
      if err
        cb err
      else
        if hash == res # good to go.
          cb null, hash
        else
          cb error: 'invalid_hash', value: hash
  @verifyTimeframe: (res, cb) ->
    current = (new Date()).getTime()
    if current >= res.f and current <= res.t
      logger.debug 'Token.verifyTimeframe', res.f, current, res.t
      cb null, res
    else
      cb error: 'expired_token', res

#Type.register token

module.exports = token