Exception = require '../../shared/exception'
crypto = require '../../shared/crypto'
assert = require 'assert'
Type = require '../../src/type'
Token = require './token'
_ = require 'underscore'
logger = require '../../shared/log'

Type.register
  name: 'hexString'
  base: 'string'
  regex: /^([a-fA-F0-9]{2})+$/

class password extends Type.has('object')
  @base: 'object'
  @fields: [ # how to get these fields to be made appropriately? hmm...
    {name: 'algo', type: 'string', default: 'sha256hmac'}
    {name: 'key', type: 'hexString', default: {proc: 'make_crypto_salt'}}
    {name: 'salt', type: 'hexString', default: {proc: 'make_crypto_salt'}}
    {name: 'hash', type: 'hexString', optional: true}
  ]
  @make: (pwd, cb) ->
    if not pwd.password
      cb error: 'blank_password'
    else
      if @passWordStrengthOK pwd.password
        # let's make the fields... how do we do that?
        @_makeFields pwd, cb
      else
        cb error: 'weak_password', msg: '1 number, 1 uppercase letter, and 1 symbol character'
  @_makeObject: (pwd, cb) ->
    # let's make it... 
    logger.debug 'Password.makeHmac', pwd.key, pwd.salt, pwd.password
    # key/salt are not string... this is where the problem is... hmm
    crypto.makeHmac pwd.key, pwd.salt, pwd.password, (err, hash) =>
      logger.debug 'Password.makeHmac_error', err, hash
      if err
        cb err
      else
        delete pwd.password
        pwd.hash = hash
        Type.make 'token', {}, (e1, token) =>
          pwd.token = token
          cb e1, new @(pwd)
  constructor: (pwd) ->
    _.extend @, pwd
    logger.debug 'Password.make', @
  @makeSalt: crypto.makeSalt
  @makeHmac: crypto.makeHmac
  @verifyHmac: crypto.verifyHmac
  # also need to deal with password policy... for now let's keep it simple
  # 1 number, 1 symbol, 1 uppercase letter.
  @verify: ({key, salt, hash}, {password}, cb) ->
    @verifyHmac key, salt, password, hash, (err, res) =>
      if err
        cb err
      else
        Type.make 'token', {}, (e1, r1) =>
          res.token = r1
          cb e1, res
  @verifyToken: ({token}, cb) ->
    Token.verify token, cb
  @updateToken: ({token}, cb) ->
    Token.update token, cb
  @passWordStrengthOK: (password) ->
    /\d/.exec(password) and /[^\w]/.exec(password) and /[A-Z]/.exec(password)
  
#Type.register password

module.exports = password
