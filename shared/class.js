var _ = require('underscore');

// how do you inherit from 
function Class(base, opts) {
  if (!base instanceof Function) {
    throw new Error('base is not a function');
  }
  var constructor;
  if (opts.hasOwnProperty('constructor') && opts['constructor'] instanceof Function) {
    constructor = opts['constructor'];
  } else {
    constructor = function () {
      // this will have arguments - we'll push the values into itself.
      for (var i = 0; i < arguments.length; ++i) {
        this.push(arguments[i]);
      }
    };
    opts.constructor = constructor;
  }
  constructor.prototype = _.extend(new base(), opts);
  constructor.__super__ = base.prototype; // for coffee script
  return constructor;
}

// example of Array subclass.
var Foo = Class(Array, {foo: function(i) { return this[i]; }});

var Bar = Class(Foo, {bar: function(i) { return this[i] * this[i]; }});

Class.Foo = Foo;
Class.Bar = Bar;

// let's subclass a number - this turns out impossible I think.
// but we probably can subclass a string.

module.exports = Class;

