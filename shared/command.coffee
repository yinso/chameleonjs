
argv = require('optimist')
  .usage('gitjs - same arguments as git')
  .argv

GitClient = require './git'
{quote} = require './util'
logger = require './shared'

currentPath = process.cwd()

client = new GitClient currentPath

basicOutput = (err, stdout, stderr) ->
  if err
    logger.debug "Error: ", err
    logger.debug stdout
    process.exit 1
  else
    logger.debug stdout

#if command == 'init'
#  GitClient.initRepo args[0], basicOutput

logger.debug argv
client.exec argv, basicOutput

