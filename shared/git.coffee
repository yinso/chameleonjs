# git client to

path = require 'path'
Process = require './process'
fs = require '../shared/fs'
logger = require './log'

class GitStatus
  constructor: ({X, Y, from, to}) ->
    @X = X
    @Y = Y
    @from = from
    @to = to
  isUnmerged: () ->
    # X = D & Y = D
    # X = A & Y = A
    # X = U or Y = U
    (@X == 'D' and @Y == 'D') or (@X == 'A' and @Y == 'A') or (@X == 'U' or @Y == 'U')
  isMerged: () -> not @isUnmerged()
  isRenamed: () ->
    @to != null
  isStaged: () ->
    # X != U
    # X != ' '
    # X == D & Y == ' ' or Y == M
    # X == A & Y == ' ' or Y == M or Y == D
    (@X == 'D' and (@Y == ' ' or @Y == 'M')) or
    (@X == 'A' and (@Y == ' ' or @Y == 'M' or @Y == 'D')) or
    @X != 'U' or @X != ' '
  @parseLine: (line) ->
    staged = (X, Y) ->
      X != ''
    change = (X, Y) ->
      X
    # the line has the following format.
    # XY file
    # or
    # XY file\0->\0file\0 (rename form)
    # first split on \s
    X = line[0]
    Y = line[1]
    rest = line.substring(3).split /\0/
    new GitStatus {X: X, Y: Y, from: rest[0], to: rest[2] or null}
  @parse: (status) ->
    statuses = status.split /\n/
    statuses.pop()
    (GitStatus.parseLine(line) for line in statuses)

class GitClient extends Process
  constructor: (filePath) ->
    super {proc: 'git', cwd: filePath, hasCommand: true}
  isa: (filePath, cb) ->
    @_is {}, [ filePath ], (err, res) ->
      if err
        cb false
      else
        cb res
  _is: (options, args, cb) ->
    hasDotGit = (p) -> p == '.git'
    if args.length == 0
      cb false
    else
      fs.readdir args[0], (err, files) ->
        if err
          cb err
        else
          cb null, files.filter(hasDotGit).length != 0
  init: (path, cb) ->
    @isa path, (res) =>
      logger.debug "git init #{path} => #{res}"
      if res
        cb null, "#{path} is already a git repo", null
      else
        @_init {}, [ path ], cb
  _init: (options, args, cb) ->
    fs.mkdirp args[0], (err, made) =>
      logger.debug "git _init #{args[0]} => #{err}"
      if err
        cb err, null, null
      else
        @execProc 'init', options, args, cb
  resolve: (relPath) ->
    path.resolve @cwd, relPath
  # save file to a particular relative location.
  writeFile: (relPath, data, encoding, cb) ->
    fs.writeFile @resolve(relPath), data, encoding, cb

  # pre-formatting options
  options:
    status: (options) ->
      options.porcelain = true
      options
    log: (options) ->
      format =
        commit: '%H'
        author: '%an <%ae>'
        date: '%ad'
        parent: '%P'
        message: '%s'
      options['pretty='] = "format:#{JSON.stringify(format)}"
      options
  # formatting stdout
  stdout:
    status: (status) -> GitStatus.parse(status)
    log: (logs) ->
      for line in logs.split /\n/
        JSON.parse(line)
        #line
# the above forms a basic git client...
module.exports = GitClient
