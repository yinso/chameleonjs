
# for dealing with function prototypes...

# 
# Tail Call Optimization
# 

# this is the true tail design for javascript at this moment... unfortunately.
# 
# process.nextTick should be fast enough, but setTimeout is quite slow...
#
# the nice thing is that this version will work for both sync and async callbacks, 
# whereas a trampoline based design at https://github.com/spencertipping/js-in-ten-minutes
# only works with sync methods.
#
# the trick appears to be that sync can be adapted to async but not vice versa (because
# async automatically rewinds the callstack)
#
# and direct return can be adapted to CPS but not vice versa either. both for the same reason.

asyncNext = if process then process.nextTick else (cb) -> setTimeout(cb, 0)

Function::tail = () ->
  args = Array::slice.apply(arguments)
  asyncNext () =>
    @.apply @, args

