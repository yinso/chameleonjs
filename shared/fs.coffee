fs = require 'fs'
path = require 'path'
rimraf = require 'rimraf'
Exception = require '../shared/exception'

#resolveRoot = path, ({dir, [path...]}) -> ...
resolveRoot = (filePath, cb) ->
  helper = ({base, paths}, cb) ->
    dir = path.dirname base
    segment = path.basename base
    paths.unshift segment
    result = {base: dir, paths: paths}
    fs.stat dir, (err, stat) ->
      if err # no file... keep going.
        helper result, cb
      else if stat.isDirectory() # good to go.
        cb null, result
      else # file exist but is not a directory... this is an error!!!
        # an exception object created as this is a custom exception.
        cb new Exception(error: 'root_path_not_directory', path: dir, stat: stat)
  helper {base: filePath, paths: []}, cb

mkdirp = (filePath, cb) ->
  helper = (base, paths) ->
    if paths.length == 0 # we are done.
      cb null, true
    else
      dir = path.join base, paths[0]
      paths.shift()
      fs.mkdir dir, 0o777, (err) ->
        if err
          cb err, false
        else
          helper dir, paths
  fs.stat filePath, (err, stat) ->
    if err # file not exist - keep going.
      resolveRoot filePath, (err2, result) ->
        if err2
          cb err2, false
        else
          # time to make the directories...
          helper result.base, result.paths
    else if stat.isDirectory() # good to go.
      cb null, true
    else # file exist but not directory
      # custom exception
      cb new Exception(error: 'file_exist_and_not_directory', path: filePath, stat: stat), false

fs.rmrf = rimraf
fs.resolveRoot = resolveRoot
fs.mkdirp = mkdirp

module.exports = fs
