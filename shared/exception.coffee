###
# error
#
# functions for generating error objects.
###

_ = require 'underscore'

# Error object ought to have the following
# name: the type of object (used for subtype)
# message: the text representation of the error object
# error: textual string of error code.
# httpCode: mapping this to http value.
# Error object should only be created during the throw phase - or when you know where you should do so
# i.e. most of the callback should not be creating an exception object (it should assume the passed in err is an exception object)

# we also need a way to handle {error: err} as well... this should not have been done.
# i.e. if we pass in the error object into this app exception.

logger = require './log'

class Exception extends Error
  constructor: (opts) ->
    super(opts.message or opts.toString())
    logger.error 'Exception', opts
    _.extend @, opts
  @make: (opts = {}, moreOpts = {}) ->
    if opts.error instanceof Error
      inner = opts.error
      delete opts.error
      err = new Exception inner
      _.extend err, opts, moreOpts
      err
    else
      new Exception(_.extend(opts, moreOpts))
  @badRequest: (opts) ->
    @make opts, {httpCode: 400}
  @authRequired: (opts) ->
    @make opts, {httpCode: 401}
  @forbidden: (opts) ->
    @make opts, {httpCode: 403}
  @notFound: (opts) ->
    @make opts, {httpCode: 404}
  @internal: (opts) ->
    @make opts, {httpCode: 500}
  @busy: (opts) ->
    @make opts, {httpCode: 503}

module.exports = Exception
