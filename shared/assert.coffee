assert = require 'assert'

isa = (val, type, inherits = false) ->
  if (val == null || val == undefined)
    val == type
  else
    if (inherits)
      val.constructor == type || val instanceof type
    else
      val.constructor == type

inherits = (val, type) ->
  isa val, type, true

assert.isa = (val, type, msg) ->
  assert.ok isa(val, type), msg

assert.notIsa = (val, type, msg) ->
  assert.ok !isa(val, type), msg

assert.inherits = (val, type, msg) ->
  assert.ok inherits(val, type), msg

assert.notInherits = (val, type, msg) ->
  assert.ok !inherits(val, type), msg

module.exports =
  assert: assert
  isa: isa
  inherits: inherits

