crypto = require 'crypto'


# goal 4/1 - populate the schema coffee and redefine backend routes with the schema.
# start to look at transferring data over to mongodb...
# need to start to implement users...

b2h = []
h2b = {}
for i in [0...256] by 1
  b2h[i] = (i ^ 0x100).toString(16).substring(1)
  h2b[b2h[i]] = i

toHex = (bytes) ->
 for byte in bytes
  b2h[byte]

makeSalt = (size = 32) ->
  toHex(crypto.randomBytes(size)).join('')

# we'll need helper functions that'll generate a hash...
makeHmac = (key, salt, passwd, cb) ->
  try
    hmac = crypto.createHmac 'sha256', key
    hmac.update salt + passwd + salt
    cb null, hmac.digest('hex')
  catch e
    cb e, null

verifyHmac = (key, salt, passwd, hmac, cb) ->
  makeHmac key, salt, passwd, (err, res) ->
    if err
      cb err, res
    else
      if res == hmac
        cb null, res
      else
        cb error: 'invalid_password'

# it's kinda like a default but not really....


crypto.makeSalt = makeSalt
crypto.makeHmac = makeHmac
crypto.verifyHmac = verifyHmac

module.exports = crypto
