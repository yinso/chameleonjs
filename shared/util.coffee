{assert} = require 'chai'
{flatten} = require 'underscore'

###
# Failed is a sentinel class that indicates that the parse has failed.
###
class Failed
  constructor: () ->
    @failed = true
  @instance: new Failed()

parseEquals = (parser, input, result) ->
  assert.equal parser.parse(input).result, result

parseFails = (parser, input) ->
  parseEquals parser, input, Failed.instance

toString = (ary) ->
  flatten(ary).join('')

integer = (ary, radix = 10) ->
  parseInt toString(ary), radix

float = (ary) ->
  parseFloat toString(ary)

quote = (str, quote = "'") ->
  helper = (c) ->
    if c == '\b'
      '\\b'
    else if c == '\f'
      '\\f'
    else if c == '\n'
      '\\n'
    else if c == '\r'
      '\\r'
    else if c == '\t'
      '\\t'
    else if c == '\\'
      '\\\\'
    else if c == '\''
      '\\\''
    else if c == '\0'
      "\\0"
    else if c == '"'
      "\\\""
    else
      i = c.charCodeAt()
      if (i > 127)
        '\\u' + i.toString(16)
      else
        c
  quote + (helper(c) for c in str).join('') + quote

module.exports =
  assert: assert
  parseEquals: parseEquals
  parseFails: parseFails
  Failed: Failed
  flatten: flatten
  toString: toString
  toInteger: integer
  toFloat: float
  quote: quote

