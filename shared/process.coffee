fs = require '../shared/fs'
{exec, execFile} = require 'child_process'
_ = require 'underscore'
logger = require './log'

class Process
  constructor: ({proc, cwd, hasCommand}) ->
    @proc = proc
    @origCwd = process.cwd()
    @cwd = cwd or @origCwd
    @hasCommand = hasCommand or false
  chdir: (newPath) ->
    @cwd = newPath
  exec: (argv, cb) ->
    command = @parseCommand argv
    args = @parseArgs argv
    options = @parseOptions argv
    if command and @options?[command]
      options = @options[command](options)
    if command and @["_#{command}"] and _.isFunction(@["_#{command}"])
      @["_#{command}"] options, args, cb
    else
      @execProc command, options, args, cb
  execProc: (command, options, args, cb) ->
    execArgs =  @_toArgs(command, options, args)
    logger.debug 'execProc', execArgs
    execFile @proc, execArgs, {cwd: @cwd, env: process.env}, @_callback(command, cb)
  _callback: (command, cb) ->
    if @stdout?[command] and _.isFunction(@stdout[command])
      parser = @stdout[command]
      (err, stdout, stderr) ->
        if err
          cb err
        else
          cb null, parser(stdout)
    else
      cb
  _toArgs: (command, options, args) ->
    output = []
    if command
      output.push command
    for key, val of options
      if key.length == 1
        output.push "-#{key}"
        output.push val
      else if key.charAt(key.length - 1) == '='
        output.push "--#{key}#{val}"
      else if _.isBoolean(val)
        output.push "--#{key}"
      else
        output.push "--#{key}"
        output.push val
    output.concat args
  parseCommand: (argv) ->
    command = null
    if argv.command
      command = argv.command
      delete argv.command
    else if @hasCommand and argv._.length > 0
      command = argv._.shift()
    command
  parseArgs: (argv) ->
    args = argv._ or []
    delete argv._
    args
  parseOptions: (argv) ->
    delete argv.$0
    argv

module.exports = Process
