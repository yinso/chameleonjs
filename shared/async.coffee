
fold = (ary, iterator, initial, callback) ->
  # the goal is to capture the results from each iteration
  # and then invoke the final callback @ the end.
  # we'll assume that the iterator will automatically return result (i.e. it's not making another call?
  # but it can be async itself...
  # so how to hook togehter a bunch of async calls?
  # we can build it up so that way it calls itself recursively...
  # obviously the only problem with this approach is
  helper = (i, result) ->
    if i == ary.length
      callback null, result
    else
      val = ary[i]
      iterator val, result, (err, result) ->
        if err
          callback err
        else
          helper i + 1, result
  helper 0, initial

map = (ary, iterator, callback) ->
  helper = (i, result) ->
    if i == ary.length
      callback null, result
    else
      val = ary[i]
      iterator val, (err, res) ->
        if err
          callback err
        else
          result.push res
          helper i + 1, result
  helper 0, []

filter = (ary, iterator, callback) ->
  helper = (i, result) ->
    if i == ary.length
      callback null, result
    else
      val = ary[i]
      iterator val, (err, res) ->
        if err
          callback err
        else if res
          result.push res
          helper i + 1, result
        else
          helper i + 1, result
  helper 0, []

forEach = (ary, iterator, callback) ->
  # we won't do the shift version anymore... we'll pass in an i.
  helper = (i, result) ->
    if i == ary.length
      callback null # no result.
    else
      val = ary[i]
      iterator val, (err, result) ->
        if err
          callback err
        else # don't really care about the previous result? possibly...
          helper i + 1, result
  helper 0, null


objectMap = (keyVals, iterator, callback) ->
  arys =
    for key, val of keyVals
      [key, val]
  result = {}
  helper = ([key, val], next) ->
    iterator key, val, (err, res) ->
      if err
        next err
      else
        result[key] = res
        next null
  forEach arys, helper, (err, res) ->
    if err
      callback err
    else
      callback null, result

first = (ary, iterator, callback) ->
  helper = (i, interim) ->
    if i == ary.length # no results - error
      callback null, interim
    else
      val = ary[i]
      iterator val, (err, result) ->
        if result
          callback err, result
        else
          helper i + 1, result
  helper 0, null


module.exports =
  fold: fold
  forEach: forEach
  first: first
  map: map
  filter: filter
  objectMap: objectMap

