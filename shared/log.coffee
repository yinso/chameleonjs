
# this ought to exist in util level function... hmmm...
# we'll later want to have this to be wrapped in a way that can be dynamically controlled.

# the different logging that we are doing.
#
# 1 - basic levels of logging.
# 2 - redirection of the logging values...
# 3 - ensure that it's not 

# best places to log...
# log should take in a particular key
# the very first item ought to be considered as a key.

# the key ought to be a list of values that can be dynamically adjusted...
# we can create new keys...

# the tabbing logic.
# ought the tab be done via join?
# or out the tab be done later at the end?
# at the original level there isn't any tab.
# [<tab_level>, <formatted_val>]
# 
# \t{
# \t\t<key>: \t\t\t{ # the prefix in val doesn't make sense when it's nested...
# \t\t\t<key>:\t\t\t\t{
fs = require 'fs'

global = this

class Line
  constructor: (@tabs = '  ', @inner = [], @tabLevel = 0, @length = 0) ->
  write: (text...) ->
    @copy text
  copy: (text) ->
    for t in text
      @push t
  push: (text) ->
    @inner.push text
    @length += text.length
  indent: (tabLevel) ->
    @tabLevel = tabLevel
  makeTabs: () ->
    (@tabs for i in [0...@tabLevel]).join('')
  generate: () ->
    [ @makeTabs() ].concat(@inner).join('')
  merge: (line) ->
    newLine = new Line @tabs, [].concat(@inner), @tabLevel, @length
    newLine.copy line.inner
    newLine

class Lines
  constructor: (tabSpace = 2) ->
    @tabs = (' ' for i in [0...tabSpace]).join('')
    @inner = []
    @current = new Line @tabs
    @inner.push @current
  write: (text...) ->
    @current.write text...
  newLine: () ->
    @current = new Line @tabs
    @inner.push @current
  indent: (tab) ->
    @current.indent tab
  end: () ->
    texts = (line.generate() for line in @inner)
    texts.join('\n')
  lengthOfLastNLines: (n) ->
    if n > @inner.length
      console.log "lines.lengthOfLastNLines", "ERROR", n, @
      throw new Error("n_exceed_current_lines: #{n} >= #{@inner.length}")
    else
      start = @inner.length - n # if len = 5, n = 3, then we start from
      length = 0
      for i in [start...@inner.length]
        length += @inner[i].length
      length
  mergeLastNLines: (n) ->
    if n > @inner.length
      throw new Error("n_exceed_current_lines: #{n} >= #{@inner.length}")
    else
      start = @inner.length - n # if len = 5, n = 3, then we start from
      removed = @inner.splice(start, n)
      result = removed[0]
      for i in [1...removed.length]
        result = result.merge(removed[i])
      @inner.push result
  mergeLastNLinesIfLengthLessThan: (n, len = 80) ->
    length = @lengthOfLastNLines(n)
    if length < len
      @mergeLastNLines(n)

_arrayHelper = (obj, tab, cache, lines) ->
  index = cache.indexOf(obj)
  if index != -1
    lines.write "$<ref:#{index}>"
  else
    cache.push obj
    lines.indent(tab)
    lines.write '['
    lines.newLine()
    for item, i in obj
      if obj.hasOwnProperty(i)
        lines.indent(tab + 1)
        if i > 0
          lines.write ', '
        _formatHelper item, tab + 1, cache, lines
        lines.newLine()
    lines.indent(tab)
    lines.write ']'
    lines.mergeLastNLinesIfLengthLessThan(obj.length + 2, 80)
  lines

_objectHelper = (obj, tab, cache, lines) ->
  hasOwnProperty = Object.prototype.hasOwnProperty
  index = cache.indexOf(obj)
  if index != -1
    lines.write "$<ref:#{index}>"
  else if not obj.hasOwnProperty
    lines.write "$<empty>"
  else if obj == global
    lies.write "${global}"
  else
    cache.push obj
    lines.indent(tab)
    lines.write '{'
    lines.newLine()
    keys = Object.keys obj
    for key, i in keys
      item = obj[key]
      lines.indent(tab + 1)
      if i > 0
        lines.write ", "
      lines.write key
      lines.write ": "
      _formatHelper item, tab + 1, cache, lines
      lines.newLine()
    lines.indent(tab)
    lines.write "}"
    lines.mergeLastNLinesIfLengthLessThan(i + 2, 80)
    lines


_formatHelper = (obj, tab = 0, cache = [], lines = new Lines()) ->
  if obj == null
    lines.write 'null'
  else if obj == undefined
    lines.write 'undefined'
  else
    type = typeof(obj)
    if type == 'number' or type == 'boolean'
      lines.write "#{obj}"
    else if type == 'string'
      lines.write JSON.stringify(obj)
    else if type == 'function'
      lines.write "<Function:#{obj.name or 'anonymous'}>"
    else if obj instanceof Array
      _arrayHelper obj, tab, cache, lines
    else 
      _objectHelper obj, tab, cache, lines
  lines

format = (obj, tab = 0) ->
  _formatHelper(obj, tab).end()

disabledLabels = {}

disableLabel = (label) ->
  disabledLabels[label] = label

enableLabel = (label) ->
  delete disabledLabels[label]

isLabelEnabled = (label) ->
  not disabledLabels.hasOwnProperty(label)

levels =
  CRITICAL: 0
  ERROR: 1
  WARN: 2
  DEBUG: 3
  INFO: 4

currentLevel = 3

setLevel = (level) ->
  if levels.hasOwnProperty(level)
    currentLevel = levels[level]
  else
    throw new Error "unknown_log_level: #{level}"

getStackInfo = () ->
  err =
    try
      throw new Error()
    catch e
      e
  callerLine = err.stack.split('\n')[5]
  [_, at, name, pos] = callerLine.split(/\s+/)
  [file,line,col] = pos.substring(1, pos.length - 1).split(':')
  [name, file, line, col]

logStream = process.stdout

setLogStream = (filePath) ->
  logStream = fs.createWriteStream filePath, {'flags': 'a'}
  process.on 'exit', () ->
    logStream.destroy()
    #console.log 'logStream', logStream

log = (label, args...) ->
  if isLabelEnabled(label)
    formattedArgs = (format(arg) for arg in args).join(' ')
    logStream.write "<#{label}> - #{formattedArgs}\n"

logByLevel = (label, level, args...) ->
  if isLabelEnabled(label)
    formattedArgs = (format(arg) for arg in args).join(' ')
    logStream.write "<#{level}:#{label}> - #{formattedArgs}\n"

errorLogByLevel = (label, level, args...) ->
  if isLabelEnabled(label)
    formattedArgs = (format(arg) for arg in args).join(' ')
    process.stderr.write "<#{level}:#{label}> - #{formattedArgs}\n"

logWithLevel = (level) ->
  if not levels.hasOwnProperty(level)
    throw new Error "unknown_log_level: #{level}"
  else
    levelVal = levels[level]
    errorLevel = levels['ERROR']
    (label, args...) ->
      if levelVal <= currentLevel
        logByLevel label, level, args...
      if levelVal <= errorLevel
        errorLogByLevel label, level, args...
        

critical = logWithLevel('CRITICAL')

error = logWithLevel('ERROR')

warn = logWithLevel('WARN')

debug = logWithLevel('DEBUG')

info = logWithLevel('INFO')

module.exports =
  log: log
  disableLabel: disableLabel
  enableLabel: enableLabel
  setLevel: setLevel
  critical: critical
  error: error
  warn: warn
  debug: debug
  info: info
  setLogStream: setLogStream
  format: format

