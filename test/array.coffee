# **********************************************************************
# shared code base
# **********************************************************************
# this is from the shared code base
# we might further separate those into its own module as well.
# (ought to consider fixing amdify)
Class = require '../shared/class'
logger = require '../shared/log'

Array2 = Class(Array, {})

assert = require 'assert'

describe 'array2 can be emptyed', () ->
  it 'array2 can be emptyed', (done) ->
    try
      ary = new Array2()
      assert.ok ary instanceof Array, 'ary_not_instanceof_array'
      ary.push 1, 2, 3, 4, 5
      logger.debug 'arry2.length 1', ary, ary.length
      ary.splice(0, ary.length)
      logger.debug 'arry2.length 2', ary, ary.length
      assert.equal 0, ary.length
      logger.debug 'arry2.length 3', ary, ary.length
      done null
    catch e
      done e
