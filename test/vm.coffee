Exception = require '../shared/exception'
VM = require '../src/vm'
assert = require 'assert'
async = require '../shared/async'
Relation = require '../src/relation'

vm = null

describe 'VM.ctor', () ->
  it 'has been created', (done) ->
    try
      vm = new VM()
      assert.ok vm, 'VM_failed'
      done null
    catch e
      done e

testCases = [
  # 
  # literals
  # 
  ['1', 1]
  ['"hello"', 'hello']
  ['1.5', 1.5]
  
  # 
  # compound objects
  # 
  ['{foo: 1, bar: 2 * 3}', {foo: 1, bar: 6}]
  ['[1, 2, 3]', [1, 2, 3]]
  
  # 
  # arithmetics
  # 
  ['1 + 1', 2]
  ['2 * 2', 4]
  ['3 + 4 * 2', 11]
  
  # 
  # comparisons
  # 
  ['1 > 1', false]
  ['1 >= 1', true]
  ['1 == 1', true]
  ['1 < 1', false]
  ['1 <= 1', true]
  ['1 != 1', false]
  
  # 
  # logical expressions
  # 
  ['1 and 1', 1]
  ['1 and 0', false]
  ['0 or 1', 1]
  ['0 or 0', false]
  ['! 0', true]
  
  # 
  # if expressions
  # 
  ['if 1 then 2 else 3', 2]
  
  # 
  # try/catch expressions
  # 
  ['try throw 1 catch e e', 1]
  
  # 
  # block expression
  # 
  ['begin 1 2 3 4 5 * 2 end', 10]
  
  # 
  # definition
  # 
  ['def a = 5']
  ['a', 5] # somehow it became a lexical definition... hmm...
  ['def a = 10']
  
  # 
  # functions
  # 
  ['def foo(a, b) a + b']
  ['foo', (obj) -> obj]
  ['foo(a, a + 2)', 22]
  
  # 
  # fibonacci
  # 
  ['def fib(n) begin
  def loop(n, prev, cur) begin
    if n < 2 then
      cur
    else
      loop(n - 1, cur, cur + prev)
  end
  if n <= 0 then
    0
  else
    loop(n, 0, 1)
end', undefined]
  ['fib(10)', 55]
  ['fib(20)', 6765]
  ['fib(80)', 23416728348467684]
  
  # 
  # DBC
  # type
  # 
  ['def foo(a integer, b integer) a + b']
  ['foo(1, 2)', 3]
  ['foo(1, 2.5)', undefined, new Exception error: 'invalid_integer', values: 2.5]
  ['def b integer null = null']
  ['b', null]
  ['def [{a float, b integer null}, c string default "hello"] = [{a: 5}]']
  ['a', 5]
  ['b', null]
  ['c', 'hello']
  
  # 
  # optional param
  # 
  ['def foo(a integer default 3, b integer default 2 + 3) a + b']
  ['foo(1, 2)', 3]
  ['foo(1)', 6]
  ['foo()', 8]
  
  # 
  # fibonacci with optional parameters
  # 
  ['def fib(n integer default 10) begin
  def loop(n, prev integer default 0, cur) begin
    if n < 2 then
      cur
    else
      loop(n - 1, cur, cur + prev)
  end
  if n <= 0 then
    0
  else
    loop(n, 1)
end']
  ['fib()', 55]
  ['fib(10)', 55]
  ['fib(20)', 6765]
  ['fib(80)', 23416728348467684]
  
  # 
  # eval... should fix the error for returning... hmm...
  # 
  ['eval("1 + 1 = 2")', undefined, new Exception error: 'invalid_integer', values: 2.5]
  
  # 
  # apply - this is tuple apply at this moment...
  # change this to call.
  # 
  ['call(fib, {n: 10})', 55]
  ['call(fib, {n: 20})', 6765]
  
  # 
  # needs to ensure the creation of the type a bit less redundant... remove the word type?
  # just do the following...
  # def user = tuple(...) # this creates a type... but is this what we want?
  # type user = tuple(...) # this is probably better.
  # type ssn = integer where ssn > 5
  # 
  ['type natural = integer where natural > 0']
  ['type lt10 = natural where lt10 < 10']
  ['lt10.isa(11)', false]
  ['lt10.isa(0)', false]
  ['lt10.isa(9)', true]
  
  #
  # we ought to start define the password types...
  # let's make a hash function
  # 
  ['type hexString = string where isHexString(hexString)']
  ['type uuid = string where isUUID(uuid)']
    ['def makePassword(passwd string, k hexString default makeSalt(), s hexString default makeSalt())  {key: k, salt: s, hash: makeHmac(k, s, passwd)}']
  ['type password = tuple (key hexString, salt hexString, hash hexString)']
  ['def verifyPassword(pwd password, password string) verifyHmac(pwd.key, pwd.salt, password, pwd.hash)']
  ['def pwd1 = makePassword("this_is_a_password")'] # how do I move this into ctor?
  ['verifyPassword(pwd1, "this_is_a_password")', true]
  ['verifyPassword(pwd1, "this_is_not_the_password")', false]
  ['type user = tuple(id integer, login string, password password)']
  ['user.isa({login: "yc", id: 1, password: makePassword("this_is_a_password")})', true]
  
  # 
  # relation tests
  #
  ['type test = relation(col1 integer, col2 integer)']
  ['insert into test values {col1: 1, col2: 2}, {col1: 2, col2: 3}, {col1: 3, col2: 4}', undefined , undefined, true]
  { stmt: 'select * from test', relationResult: [{col1: 1, col2: 2}, {col1:2, col2: 3}, {col1: 3, col2: 4}]}
  
  # 
  # where test.
  # 
  { stmt: 'select * from test where col2 > 2', relationResult: [{col1:2, col2: 3}, {col1: 3, col2: 4}]}
  
  # 
  # projection_test
  # 
  { stmt: 'select col1 from test', relationResult: [{col1: 1}, {col1:2}, {col1: 3}]}
  { stmt: 'select col1 + 2 from test', relationResult: [{exp0: 3}, {exp0: 4}, {exp0: 5}]}
  
  # both filter & project together
  { stmt: 'select col1 + 2 as col from test where col2 > 2 ', relationResult: [{col: 4}, {col: 5}]}
  
  # 
  # relation alias test
  # 
  { stmt: 'select t.col1 from test t', relationResult: [{exp0: 1}, {exp0: 2}, {exp0: 3}]}
  
  ]

runTest = (testCase) ->
  if testCase instanceof Array
    [stmt, expected, expectedError, noVerify, relationResult] = testCase
  else #if testCase instanceof Object
    {stmt, expected, expectedError, noVerify, relationResult} = testCase
  it "#{stmt} ==> #{expected}", (done) ->
    vm.eval stmt, (err, actual) ->
      if err
        if expectedError
          done null
        else
          done err
      else
        try
          if expected instanceof Function
            assert.ok expected(actual), "#{stmt} !#{expected.toString()}; actual == #{actual}"
          else if noVerify # we are just skipping the result i.e. we don't care...
            assert.ok true
          else if relationResult
            assert.ok (actual instanceof Relation), "actual_not_a_relation: #{actual}"
            assert.deepEqual actual.inner, relationResult
          else
            assert.deepEqual actual, expected, "#{stmt} !== #{expected}; actual == #{actual}"
          done null
        catch e
          done e

describe 'VM.eval', () ->
  for test in testCases
    runTest test

describe 'VM.evalDBTest', () ->
  

###

all the following should parse & execute correctly!!!

select * from test t

{select: "*", from: {id: "test", alias: "t"}, where: ""}

select t.* from test t // fail parse right now

select test.* from test // fail parse right now

select col1 from test

{select: [{column: {id: "col1"}}], from: {id: "test"}, where: ""}

select test.col1 from test

{
  select: [{column: {ref: {id: "test"}, key: {literal: "col1"}}}]
  , from: {id: "test"}
  , where: ""
}

select test.col1 from test t where t.col2 > col1

{
  select: [{column: {ref: {id: "test"}, key: {literal: "col1"}}}]
  , from: {id: "test", alias: "t"}
  , where: {
    funcall: {id: ">"}
    , args: [{ref: {id: "t"}, key: {literal: "col2"}}, {id: "col1"}]
  }
}

###
