
{assert, isa, inherits} = require '../shared/assert'
parser = require '../src/chamelang'
Exception = require '../shared/exception'
logger = require '../shared/log'

parseTest = (str, expected) ->
  (done) ->
    try
      res = parser.parse(str)
      # logger.debug 'parseTest', str, res, expected, obj.deepEqual(res, expected)
      assert.deepEqual res, expected
      done null
    catch e
      done e

parseTestError = (str, expected) ->
  (done) ->
    try
      res = parser.parse(str)
      assert.ok obj.deepEqual(res, expected)
      done new Exception error: 'expecting_parse_error_but_passed', input: str, expected: expected
    catch e
      logger.debug 'expected_error', e
      done null


describe 'parse atomic expressions', () ->
  it 'should parse integer as literal', 
    parseTest '1', {literal: 1}
  it 'should parse float as literal',
    parseTest '1.2', {literal: 1.2}
  it 'should parse dqstring as literal',
    parseTest '"a string"', {literal: "a string"}
  it 'should parse sqstring as literal',
    parseTest "'a string'", {literal: 'a string'}
  it 'should parse true as literal',
    parseTest 'true', {literal: true}
  it 'should parse false as literal',
    parseTest 'false', {literal: false}
  it 'should parse null as literal',
    parseTest 'null', {literal: null}
  #it 'should parse undefined as literal',
  #  parseTest 'undefined', {literal: undefined}
  it 'should parse regex as regex',
    parseTest '/^abc$/', {regex: '^abc$'}
  it 'should parse object as object expression',
    parseTest '{foo: 1, bar: 2}', {object: [['foo', {literal: 1}], ['bar', {literal: 2}]]}
  it 'should parse object as object expression',
    parseTest '{foo: 1, bar: 2 + 3}', {object: [['foo', {literal: 1}], ['bar', {funcall: {id: '+'}, args: [{literal: 2}, {literal: 3}]}]]}
  it 'should parse array as array expression',
    parseTest '[1, 2, 3]', {array: [{literal: 1}, {literal: 2}, {literal: 3}]}
  it 'should parse identifier',
    parseTest 'abc', {id: 'abc'}
  it 'should parse reference',
    parseTest 'abc.df', {ref: {id: 'abc'}, key: {literal: 'df'}}
  it 'should parse reference',
    parseTestError 'abc.def', {ref: {id: 'abc'}, key: {literal: 'def'}}
  it 'should parse reference',
    parseTest 'abc["def"]', {ref: {id: 'abc'}, key: {literal: 'def'}}
  it 'should parse reference',
    parseTest 'abc[1]', {ref: {id: 'abc'}, key: {literal: 1}}

describe 'parse operation expressions', ->
  it 'should parse !false',
    parseTest '!false', {funcall: {id: '!'}, args: [{literal: false}]}
  it 'should parse 1 + 1',
    parseTest '1 + 1', {funcall: {id: '+'}, args: [{literal: 1}, {literal: 1}]}
  it 'should parse 1 * 1',
    parseTest '1 * 1', {funcall: {id: '*'}, args: [{literal: 1}, {literal: 1}]}
  it 'should parse 1 + 1 * 2',
    parseTest '1 + 1 * 2',
      funcall: {id: '+'},
      args: [
        {literal: 1}
        {funcall: {id: '*'}, args: [{literal: 1}, {literal: 2}]}
      ]
  it 'should parse abc + foo',
    parseTest 'abc + foo', {funcall: {id: '+'}, args: [{id: 'abc'}, {id: 'foo'}]}
  it 'should parse a.foo + a.bar',
    parseTest 'a.foo + a.bar',
      funcall: {id: '+'}
      args: [
        {ref: {id: 'a'}, key: {literal: 'foo'}}
        {ref: {id: 'a'}, key: {literal: 'bar'}}
      ]
  it 'should parse !a.foo || a.bar',
    parseTest '!a.foo || a.bar',
      funcall: {id: '||'}
      args: [
        {funcall: {id: '!'}, args: [
            {ref: {id: 'a'}, key: {literal: 'foo'}}
          ]
        }
        {ref: {id: 'a'}, key: {literal: 'bar'}}
      ]
  it 'should parse !(a + b.foo * 3)', (done) ->
    multExp =
      funcall: {id: '*'}
      args: [
        {ref: {id: 'b'}, key: {literal: 'foo'}}
        {literal: 3}
      ]
    addExp =
      funcall: {id: '+'}
      args: [
        {id: 'a'}
        multExp
      ]
    notExp = funcall: {id: '!'}, args: [ addExp ] 
    (parseTest '!(a + b.foo * 3)', notExp)(done)

describe 'parse funcall expression', () ->
  it 'should parse foo()', 
    parseTest 'foo()', {funcall: {id: 'foo'}, args: []}
  it 'should parse foo(1, 2, 3)', 
    parseTest 'foo(1, 2, 3)', {funcall: {id: 'foo'}, args: [{literal: 1}, {literal: 2}, {literal: 3}]}
  it 'should parse foo(1 + 1, 2 * bar)',
    parseTest 'foo(1 + 1, 2 * bar)',
      funcall: {id: 'foo'}
      args: [
        {funcall: {id: '+'}, args: [{literal: 1}, {literal: 1}]}
        {funcall: {id: '*'}, args: [{literal: 2}, {id: 'bar'}]}
      ]
  it 'should parse foo(foo(a.bar), !b)',
    parseTest 'foo(foo(a.bar), !b)',
      funcall: {id: 'foo'}
      args: [
        {funcall: {id: 'foo'}, args: [{ref: {id: 'a'}, key: {literal: 'bar'}}]}
        {funcall: {id: '!'}, args: [{id: 'b'}]}
      ]

describe 'parse select query', () ->
  it 'should parse select * from User',
    parseTest 'select * from User', {select: '*', from: {id: 'User'}, where: ''}

describe 'parse insert query', () ->
  it 'should parse insert into User values {login: "yc"}',
    parseTest 'insert into User values {login: "yc"}', {insert: {id: 'User'}, values: [{object: [['login', {literal: 'yc'}]]}]}

describe 'parse delete query', () ->
  it 'should parse delete from User where login == "yc"',
    parseTest 'delete from User where login == "yc"', {delete: 'User', where: {funcall: {id: '=='}, args: [{id: 'login'}, {literal: 'yc'}]}}

describe 'parse update query', () ->
  it 'should parse update User set login = "hello, " + "yc" where login == "yc"',
    parseTest 'update User set login = "hello, " + "yc" where login == "yc"', {update: 'User', set: [{setField: 'login', exp: {funcall: {id: '+'}, args: [{literal: 'hello, '}, {literal: 'yc'}]}}], where: {funcall: {id: '=='}, args: [{id: 'login'}, {literal: 'yc'}]}}


describe 'parse create table statement', () ->
  stmt = 'create table Test (id integer primary key not null, col1 string unique)'
  it "should parse #{stmt}",
    parseTest stmt,
      createTable:
        name: 'Test'
        columns: [
          {name: 'id', type: 'integer', primary: true, optional: false}
          {name: 'col1', type: 'string', unique: true}
        ]
        constraints: []
  
describe 'parse drop table statement', () ->
  stmt = 'drop table Test'
  it "should parse #{stmt}",
    parseTest stmt,
      dropTable:
        name: 'Test'

describe 'parse alter table statement', () ->
  stmt = "alter table Test add column col2 hello not null, drop column col1, add unique key ukey_test_col2 (col2)"
  it "should parse #{stmt}",
    parseTest stmt,
      alterTable:
        name: 'Test'
        alter: [
          {alter: 'addColumn', exp: {name: 'col2', type: 'hello', optional: false}}
          {alter: 'dropColumn', exp: 'col1'}
          {alter: 'addConstraint', exp: {name: 'ukey_test_col2', columns: ['col2'], unique: true}}
        ]

describe 'parse create index statement', () ->
  stmt = 'create unique index ukey_test_col1_col2 on Test (col1, col2)'
  it "should parse #{stmt}",
    parseTest stmt,
      createIndex:
        name: 'ukey_test_col1_col2'
        table: 'Test'
        columns: ['col1','col2']
        unique: true

describe 'parse drop index statement', () ->
  stmt = 'drop index ukey_test_col1_col2 on Test'
  it "should parse #{stmt}",
    parseTest stmt,
      dropIndex:
        name: 'ukey_test_col1_col2'
        table: 'Test'

# will have to parse other type of expression as well... will add here...
# define_expression
# inline_expression (things that can be "inlined" -> ie. returning values)
# block_expression
# query_expression
# command_expression
# atomic_expression
# funcall_exp
#
# funcall_exp <- ref_expression
# ref_expression <- funcall_expression
#
# funcall_expression =
# foo()
# foo()()
# foo.bar()
# foo.bar().foo()
# it cannot be regular ATOM...
# (abc)()
# 

# what about ref_expression
#
# abc.def
# (abc()).def()
# abc().def
# (abc + def).[] # this also makes no sense...
#
# it also cannot be regular ATOM... but it can be from a funcall...
#
# it means that these two are mutually referential... how do I break out of it?
# they will both refer to something in between...
# 
# identifier() or
# 