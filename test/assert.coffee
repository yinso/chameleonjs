# **********************************************************************
# shared code base
# **********************************************************************
{assert, isa, inherits} = require '../shared/assert'

describe 'assert isa test', () ->
  it 'isa should work with regular objects', (done) ->
    try
      assert.ok isa(null, null)
      assert.ok isa(undefined, undefined)
      assert.ok isa({}, Object)
      assert.ok isa('', String)
      assert.ok isa(3, Number)
      assert.ok isa([], Array)
      assert.ok !isa([], Object)
      done null
    catch e
      done e

  class Foo
    test: 1
  
  class Bar extends Foo
    test: 2

  it 'inherits should work with classes', (done) ->
    try
      assert.ok inherits(new Bar(), Foo)
      assert.ok inherits([], Object)
      assert.ok inherits(new Bar(), Object)
      assert.ok !inherits(new Foo(), Bar)
      done null
    catch e
      done e  
  
